! function () {
	var e = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof self ? self : "undefined" != typeof window ? window : "undefined" != typeof global ? global : {};
	var t = e.parcelRequired439,
		s = {},
		a = {};
	null == t && ((t = function (e) {
		if (e in a) {
			let t = a[e];
			delete a[e], t()
		}
		if (e in s) return s[e];
		if ("undefined" != typeof module && "function" == typeof module.require) return module.require(e);
		var t = new Error("Cannot find module '" + e + "'");
		throw t.code = "MODULE_NOT_FOUND", t
	}).register = function (e, t) {
		s[e] = t
	}, t.registerBundle = function (e, t) {
		a[e] = t, s[e] = {}
	}, e.parcelRequired439 = t);
	var n, o, i, r, l = t("4FqfW"),
		c = (n = l()) && n.__esModule ? n.default : n,
		u = t("2kp5t"),
		d = t("4pwCs"),
		h = t("6UyrO"),
		m = t("nIfN7"),
		p = t("7Mfds"),
		g = t("6DHKG"),
		f = t("XcRe2"),
		C = t("hQmIV"),
		w = t("6ExGy"),
		v = t("47HXs"),
		k = t("1QaVb"),
		S = t("5kIzC"),
		b = t("qnXVN"),
		y = t("uqDnl"),
		E = t("6oOtY"),
		T = t("6PPN4"),
		A = t("3YoTT"),
		P = t("6zDjW"),
		I = t("4PGYP"),
		F = t("Saatc"),
		x = t("2JplJ"),
		G = t("520kD"),
		D = t("3HMDR"),
		O = t("7wJlz"),
		B = t("2cWJK"),
		U = t("1g0wW"),
		L = t("50MFB"),
		R = t("7rAex"),
		M = t("7b7Bi"),
		N = t("1X6VZ"),
		z = t("45NE2"),
		V = t("7xzSo"),
		H = t("5gMj4"),
		$ = t("1rJgl"),
		W = t("6vp85"),
		q = t("1YcSR"),
		j = t("3OcNA"),
		Y = t("69bsG"),
		K = t("6Awlk"),
		Z = t("3wQfI"),
		J = t("2lIMO"),
		X = t("hUK5n"),
		Q = t("5NrOn"),
		ee = t("4ZA4S"),
		te = t("1aIyT"),
		se = t("1xehS"),
		ae = t("3cqbl"),
		ne = t("3lnaG"),
		oe = t("lPVHz"),
		ie = t("5SoTn"),
		re = t("4RYcH"),
		le = t("23fUW"),
		ce = t("2zvjG"),
		ue = t("5WsGf"),
		de = t("4NLO5"),
		he = t("23BIq"),
		me = t("5ZuLE"),
		pe = t("7dHHw"),
		ge = t("5LHf1"),
		fe = t("1GjA3"),
		Ce = t("6o3Qu"),
		we = t("5HXaP"),
		ve = t("dfuZm"),
		ke = t("lUxTC"),
		Se = t("367Kq"),
		be = t("6lSP7"),
		ye = t("5hlU5"),
		_e = !1;

	function Ee(e) {
		for (var t = Object.keys(e), s = 0; s < t.length; s++) i[t[s]] = e[t[s]]
	}

	function Te(e) {
		var t = i[e];
		if (null == t) throw new Error("Could not resolve bundle with id " + e);
		return t
	}

	function Ae() {
		return _e || (_e = !0, i = {}, (o = {}).register = Ee, r = Te, o.resolve = r), o
	}
	Ae().register(JSON.parse('{"5VRNk":"inssist.e95e514e.js","4csc5":"jszip.5358a684.js","4i8Pz":"background.fea1ee37.js"}')), l(), u(), d(), h(), m(), p(), g(), f(), C(), w(), v(), k(), S(), b(), v(), y(), E(), T();
	class Pe extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onAccountClick = () => {
				E().iframeBus.send("ig.ajax-go", "/inssistapp")
			}, this._onRateYesClick = e => {
				chrome.tabs.create({
					url: b().common.reviewsUrl
				}), v().gaController.sendEvent("user", "rate-us:yes-click"), this._acknowledgeAndHide(e)
			}, this._onRateNoClick = e => {
				v().gaController.sendEvent("user", "rate-us:no-click"), this._acknowledgeAndHide(e)
			}, this._onOkClick = e => {
				v().gaController.sendEvent("user", "rate-us:ok-click"), this._acknowledgeAndHide(e)
			}, this._onCloseClick = e => {
				v().gaController.sendEvent("user", "rate-us:close-click"), this._acknowledgeAndHide(e)
			}, t
		}
		render() {
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "rate-us-mediator",
				show: this.props.show
			}, Glamor.createElement(S().default.RateUsStars, {
				texts: {
					subcontent: Glamor.createElement("div", null, "Found a bug? You can DM us at ", Glamor.createElement("a", {
						onClick: this._onAccountClick
					}, "@inssistapp"), " or email at ", Glamor.createElement("a", {
						href: "mailto:inssist@slashed.io"
					}, "inssist@slashed.io"), " and we will try to help.")
				},
				onRateYesClick: this._onRateYesClick,
				onRateNoClick: this._onRateNoClick,
				onOkClick: this._onOkClick,
				onCloseClick: this._onCloseClick
			}))
		}
		_acknowledgeAndHide(e) {
			v().gaController.sendEvent("user", `rate-us:${e}-stars`), y().transaction((t => {
				t.rateUs.shown = !1, t.rateUs.rate = e, t.acknowledged.rateUs = Date.now()
			}))
		}
	}
	var Ie = y().influx((e => ({
		show: e.rateUs.shown && !T().stateProxy.isAcknowledged("rateUs")
	})))(Pe);
	E(), A(), P(), y(), T();
	var Fe = {
		init: function () {
			if (T().stateProxy.isAcknowledged("rateUs")) return;
			Ge(30), E().iframeBus.on("ig.media-open", xe), E().iframeBus.on("ig.media-download", xe), E().iframeBus.on("ig.submit-post", xe), E().iframeBus.on("ig.submit-story", xe)
		},
		showRateUs: function () {
			Ge(5)
		}
	};

	function xe() {
		Ge(5)
	}
	async function Ge(e) {
		if (T().stateProxy.isAcknowledged("followUs")) return;
		const t = y().model.state;
		(Date.now() - t.installedAt) / P().DAY < e || (E().iframeBus.off("ig.media-open", xe), E().iframeBus.off("ig.media-download", xe), E().iframeBus.off("ig.submit-post", xe), E().iframeBus.off("ig.submit-story", xe), await A().default(7 * P().SECOND), y().transaction((e => {
			e.rateUs.shown = !0
		})))
	}
	k(), S(), A(), b(), v(), y(), T(), I();
	const De = {
		followedRow: {
			height: 23,
			...k().default.text.bleak
		}
	};
	class Oe extends k().default.Component {
		constructor(e) {
			super(e), this._onFollowClick = async() => {
				v().gaController.sendEvent("user", "follow-us:follow-click"), this.setState({
					followClicked: !0
				}), await I().chromeBus.send("ig-api.follow-user", this._userId), await A().default(1300), this._acknowledgeAndHide()
			}, this._onNoClick = () => {
				v().gaController.sendEvent("user", "follow-us:no-click"), this._acknowledgeAndHide()
			}, this._prepare = async() => {
				const e = await I().chromeBus.send("ig-api.fetch-user-details", this._username),
					t = e.result;
				t ? t.isFollowedByViewer ? this._acknowledgeAndHide() : (this._userId = t.userId, this.setState({
					prepared: !0
				})) : console.error("follow us: failed to fetch user details", e.error)
			}, this._userId = null, this._username = "inssistapp", this.state = {
				followClicked: !1,
				prepared: !1
			}
		}
		componentDidUpdate(e) {
			this.props.show && !e.show && this._prepare()
		}
		render() {
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "follow-us-card-mediator",
				show: this.props.show && this.state.prepared
			}, Glamor.createElement(S().default.ActionCard, {
				image: "icon-128.png:40:40",
				content: Glamor.createElement("div", null, "Be the first to know when the next update is coming. Follow us on Instagram ", Glamor.createElement("a", {
					href: `https://instagram.com/${this._username}/`,
					target: "_blank"
				}, "@", this._username)),
				subcontent: Glamor.createElement("div", null, "Want to report a bug or request a feature? Please reach us at ", Glamor.createElement("a", {
					href: `mailto:${b().common.supportEmail}`
				}, b().common.supportEmail)),
				actions: this._getActions()
			}))
		}
		_getActions() {
			return this.state.followClicked ? [Glamor.createElement("div", {
				css: De.followedRow
			}, "Thank you 😘")] : [Glamor.createElement(k().default.ActionButton, {
				small: !0,
				label: "FOLLOW US",
				onClick: this._onFollowClick
			}), Glamor.createElement(k().default.LinkButton, {
				small: !0,
				label: "NO, GO AWAY",
				onClick: this._onNoClick
			})]
		}
		_acknowledgeAndHide() {
			y().transaction((e => {
				e.followUs.shown = !1, e.acknowledged.followUs = Date.now()
			}))
		}
	}
	var Be = y().influx((e => ({
		show: e.followUs.shown && !T().stateProxy.isAcknowledged("followUs")
	})))(Oe);
	E(), A(), P(), y(), T();
	var Ue = {
		init: function () {
			if (T().stateProxy.isAcknowledged("followUs")) return;
			Re(32), E().iframeBus.on("ig.media-open", Le), E().iframeBus.on("ig.media-download", Le), E().iframeBus.on("ig.submit-post", Le), E().iframeBus.on("ig.submit-story", Le)
		},
		showFollowUs: function () {
			Re(7)
		}
	};

	function Le() {
		Re(7)
	}
	async function Re(e) {
		if (T().stateProxy.isAcknowledged("followUs")) return;
		const t = y().model.state;
		(Date.now() - t.installedAt) / P().DAY < e || (E().iframeBus.off("ig.media-open", Le), E().iframeBus.off("ig.media-download", Le), E().iframeBus.off("ig.submit-post", Le), E().iframeBus.off("ig.submit-story", Le), await A().default(t.acknowledged.rateUs > 0 ? 15 * P().SECOND : 3 * P().MINUTE), y().transaction((e => {
			e.followUs.shown = !0
		})))
	}
	I(), E(), A(), E(), v(), y();
	var Me = {
		init: function () {
			E().iframeBus.on("ig.show-igtv-popup", Ne), E().iframeBus.on("ig.post-igtv", ze), E().iframeBus.on("ig.igtv-posted", Ve)
		},
		showPopup: Ne,
		closePopup: function () {
			v().gaController.sendEvent("user", "igtv-upload:click-close"), y().transaction((e => {
				e.igtvUpload.shown = !1
			}))
		}
	};

	function Ne() {
		v().gaController.sendEvent("user", "igtv-upload:shown"), y().transaction((e => {
			e.igtvUpload.shown = !0
		}))
	}

	function ze() {
		v().gaController.sendEvent("user", "igtv-upload:click-post")
	}
	async function Ve() {
		E().iframeBus.send("ig.go-to-igtv-tab"), await A().default(1e3), y().transaction((e => {
			e.igtvUpload.shown = !0
		}))
	}
	k(), S();
	var He = {
		createName: function (e, t) {
			return `${e}|${JSON.stringify(t)}`
		},
		getName: $e,
		getParams: function () {
			return function (e) {
				try {
					return JSON.parse(e)
				} catch (e) {
					return null
				}
			}(window.self.name.split("|")[1]) || {}
		},
		isIframe: function (e = null) {
			return window.self !== parent && (!e || $e() === e)
		}
	};

	function $e() {
		return window.self.name.split("|")[0] || null
	}
	F(), I(), x(), G();
	var We = {
		init: async function () {
			I().chromeBus.on("fusion.reload-popup", (() => {
				location.reload()
			})), I().chromeBus.send("fusion.check-new-version");
			const e = await x().default(chrome.tabs.getCurrent);
			I().chromeBus.send("fusion.popup-tab-id", e.id)
		},
		getConfig: G().default
	};
	k(), S(), I();
	class qe extends k().default.Component {
		constructor(e) {
			super(e), this._onUpdateClick = () => {
				this.setState({
					updating: !0
				}), I().chromeBus.send("fusion.update-now-click")
			}, this.state = {
				show: !1,
				updating: !1
			}
		}
		componentDidMount() {
			I().chromeBus.on("fusion.new-version-available", (() => {
				this.setState({
					show: !0
				})
			}))
		}
		render() {
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "fusion-new-version-card-mediator",
				show: this.state.show
			}, Glamor.createElement(S().default.ActionCard, {
				image: "new-version-icon.png:40:40",
				content: "A new product version is available. Click the button to update and reload INSSIST.",
				markerColor: k().default.color.error,
				actions: [Glamor.createElement(k().default.LinkButton, {
					small: !0,
					label: this.state.updating ? "UPDATING..." : "UPDATE NOW",
					disabled: this.state.updating,
					onClick: this._onUpdateClick
				})]
			}))
		}
	}
	const je = {
		iframe: {
			width: "100%",
			height: "100%",
			border: "none"
		}
	};
	class Ye extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onClose = () => {
				Me.closePopup()
			}, t
		}
		render() {
			const e = He.createName("inssist-igtv", {
					theme: this.state.theme,
					fusionConfig: We.getConfig(),
					isElectron: !!window.electron
				}),
				t = !1 !== F().env.features.iframes ? "https://www.instagram.com/tv/upload/" : null;
			return Glamor.createElement(S().default.IgtvPopup, {
				onClose: this._onClose
			}, Glamor.createElement("iframe", {
				css: je.iframe,
				name: e,
				src: t
			}))
		}
	}
	var Ke = k().default.theme.ThemeAware(Ye);
	P(), b(), v(), y(), T(), E(), I();
	var Ze = {
		followUser: async function (e) {
			return I().chromeBus.send("ig-task.follow-user", e)
		},
		unfollowUser: async function (e) {
			return I().chromeBus.send("ig-task.unfollow-user", e)
		},
		likeUser: async function (e) {
			return I().chromeBus.send("ig-task.like-user", e)
		}
	};
	k(), S(), y(), v(), D();
	class Je extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onActionBlockCloseClick = () => {
				v().gaController.sendEvent("user", "action-block:close-click"), y().transaction((e => {
					e.igTask.actionBlockCode = null
				}))
			}, t
		}
		render() {
			const {
				code: e
			} = this.props;
			let t = Boolean(e),
				s = null;
			return e === D().ec.missingPost || e === D().ec.notFound ? t = !1 : s = e === D().ec.suspended ? ["Instagram has blocked this action, probably because it might think you are mass liking / following people.", "Please re-login to Instagram.com to refresh the session and let your account rest for some time."] : e === D().ec.tooManyRequests ? ["Instagram has refused this action: too many like or follow actions were made recently.", "Please give it some time to rest before trying again."] : e === D().ec.noNetwork ? ["Can not reach Instagram server. Please check Internet connection."] : e === D().ec.timedOut ? ["Server connection has timed out or Instagram server is not responding.", "Please try again later or check or Internet connection."] : e === D().ec.redirectToLogin ? ["Instagram session expired. Please re-login to Instagram."] : e === D().ec.missingUser ? ["Liked or followed user account is no longer found on Instagram."] : e === D().ec.forbidden ? ["Instagram session expired. Please re-login to Instagram."] : e === D().ec.serverIsDown || e === D().ec.badGateway || e === D().ec.serviceUnavailable ? ["Instagram server is not responding. Please check Internet connection and try again later when Instagram is back online."] : ["Instagram refused to perform this action. Please try again later."], Glamor.createElement(S().default.SnackbarItem, {
				id: "action-block-card-mediator",
				show: t
			}, Glamor.createElement(k().default.InfoCard, {
				markerColor: k().default.color.error,
				title: "Oops… Action Blocked",
				content: s,
				onClose: this._onActionBlockCloseClick
			}))
		}
	}
	var Xe = y().influx((e => ({
		code: e.igTask.actionBlockCode
	})))(Je);
	O();
	var Qe = {
		init: function () {
			E().iframeBus.on("insights.check-credibility-click", st),
				function () {
					const e = () => {
						y().transaction((e => {
							const t = Date.now();
							e.insights.lastTaskTimestamps = e.insights.lastTaskTimestamps.filter((e => t - e < 24 * P().HOUR))
						}))
					};
					e(), setInterval(e, P().HOUR / 4)
				}()
		},
		isAutoscrollToNewTask: function () {
			return et
		},
		setAutoscrollToNewTask: tt,
		getUserErrorMessage: function (e) {
			const t = e.snapshot.usernameErrorCode;
			if (!t) return null;
			return {
				"missing-user": "user is not found on Instagram",
				"private-user": "user is private"
			}[t] || "check your connection to instagram.com"
		},
		canStartSpamFollowers: function (e) {
			if (!e.followersCount) return !1;
			return "running" === e.peerScrapeStatus || "completed" === e.peerScrapeStatus || "pending" === e.peerScrapeStatus && e.spamFollowersCount > 0
		},
		canStartSpamFollowings: function (e) {
			if (!e.followingsCount) return !1;
			return "running" === e.peerScrapeStatus || "completed" === e.peerScrapeStatus || "pending" === e.peerScrapeStatus && e.spamFollowingsCount > 0
		},
		renderTime: function (e) {
			const t = Math.max(1, Math.ceil(e / 1e3 / 60)),
				s = t % 60,
				a = Math.floor(t / 60) % 24,
				n = Math.floor(t / 60 / 24);
			let o;
			o = n > 100 ? "100d." : n > 0 ? a > 0 ? `${n}d. ${a}h.` : `${n}d.` : a > 0 ? s > 0 ? `${a}h. ${s}m.` : `${a}h.` : `${s} min.`;
			return `~${o}`
		},
		addTask: function (e) {
			if (!T().stateProxy.insights.canAddTask()) return;
			tt(!0), O().default.addTask(e)
		},
		onFaqClick: function () {
			chrome.tabs.create({
				url: b().common.faqUrl
			})
		},
		onFollowClick: async function (e) {
			return Ze.followUser(e)
		},
		onUnfollowClick: async function (e) {
			return Ze.unfollowUser(e)
		},
		onLikeClick: async function (e) {
			return Ze.likeUser(e)
		}
	};
	let et = !1;

	function tt(e) {
		et = e
	}

	function st() {
		v().gaController.sendEvent("user", "insights:check-if-bot-click")
	}
	k(), S(), P(), v(), y(), T(), O(), k(), S(), y(), T(), v(), I(), B();
	var at = Object.assign((function (e, t = !1) {
		0 === nt.length && (ot = new MutationObserver((e => {
			for (const t of nt) {
				if (ot.disconnect(), t(e), !ot) return;
				ot.observe(document.documentElement, {
					attributes: !0,
					childList: !0,
					subtree: !0
				})
			}
		})), ot.observe(document.documentElement, {
			attributes: !0,
			childList: !0,
			subtree: !0
		}));
		nt.push(e), t && e()
	}), {
		off: function (e) {
			const t = nt.indexOf(e);
			if (-1 === t) return;
			nt.splice(t, 1), 0 === nt.length && (ot.disconnect(), ot = null)
		}
	});
	const nt = [];
	let ot;
	I(), U();
	var it = {
		init: async function () {
			return void 0;
			ct = await U().idbController.get("image-proxy.cache") || {}, lt = new Worker("/image-proxy/index-web-worker.js"),
				function () {
					const e = Symbol("prevUrl");
					at((() => {
						B().$$('[style*="background-image:url"], [style*="background-image: url"]').forEach((async t => {
							const s = t.style.backgroundImage.split("(")[1].split(")")[0].replace(/['"]/g, "");
							if (!(s in ct)) return;
							if (t[e] === s) return;
							const a = await async function (e) {
								if (rt[e]) return rt[e];
								return new Promise((t => {
									lt.postMessage({
										url: e
									}), lt.addEventListener("message", (function s(a) {
										a.data.originalUrl === e && (lt.removeEventListener("message", s), rt[e] = a.data.objectUrl || e, t(rt[e]))
									}))
								}))
							}(s);
							t[e] = a, t.style.backgroundImage = `url('${a}')`, I().chromeBus.send("image-proxy.cache-item-used", s)
						}))
					}))
				}()
		},
		save: function (e) {
			return void 0;
			I().chromeBus.send("image-proxy.save", e)
		}
	};
	const rt = {};
	let lt, ct = null;
	L(), b();
	var ut = {
		getUsersProgress: function (e, t) {
			if ("planned" === e.peerScrapeStatus) return 0;
			if ("completed" === e.peerScrapeStatus) return 1;
			return function (e, t, s) {
				const a = ht(e, t, s),
					n = mt(e, t, s);
				return Math.min(a / n, .99)
			}("users", e, t)
		},
		getSpamFollowersProgress: function (e, t) {
			if (0 === e.spamFollowersCount) return 1;
			if ("completed" === e.spamFollowersStatus) return 1;
			return e.resultCounters.spamFollowersCount / e.spamFollowersCount
		},
		getSpamFollowingsProgress: function (e, t) {
			if (0 === e.spamFollowingsCount) return 1;
			if ("completed" === e.spamFollowingsStatus) return 1;
			return e.resultCounters.spamFollowingsCount / e.spamFollowingsCount
		},
		getUsersTimeLeft: function (e, t) {
			return dt("users", e, t)
		},
		getSpamFollowersTimeLeft: function (e, t) {
			return dt("spam-followers", e, t)
		},
		getSpamFollowingsTimeLeft: function (e, t) {
			return dt("spam-followings", e, t)
		},
		getTimeForSnapshot: function (e, t) {
			return pt({
				postsCount: Number(e.snapshot.postsCount) || b().insights.maxPostsCount,
				usersCount: Number(e.snapshot.followersCount) || 0,
				spamCount: 0,
				netStats: t
			}) + pt({
				postsCount: Number(e.snapshot.postsCount) || b().insights.maxPostsCount,
				usersCount: Number(e.snapshot.followingsCount) || 0,
				spamCount: 0,
				netStats: t
			})
		}
	};

	function dt(e, t, s) {
		const a = ht(e, t, s);
		return mt(e, t, s) - a
	}

	function ht(e, t, s) {
		return "users" === e ? pt({
			postsCount: t.resultCounters.postsCount,
			usersCount: t.resultCounters.followersCount,
			spamCount: 0,
			netStats: s
		}) + pt({
			postsCount: t.resultCounters.postsCount,
			usersCount: t.resultCounters.followingsCount,
			spamCount: 0,
			netStats: s
		}) : "spam-followers" === e ? pt({
			postsCount: 0,
			usersCount: t.resultCounters.followersCount,
			spamCount: t.resultCounters.spamFollowersCount,
			netStats: s
		}) : "spam-followings" === e ? pt({
			postsCount: 0,
			usersCount: t.resultCounters.followingsCount,
			spamCount: t.resultCounters.spamFollowingsCount,
			netStats: s
		}) : void 0
	}

	function mt(e, t, s) {
		return "users" === e ? pt({
			postsCount: t.postsCount || b().insights.maxPostsCount,
			usersCount: t.followersCount,
			spamCount: 0,
			netStats: s
		}) + pt({
			postsCount: t.postsCount || b().insights.maxPostsCount,
			usersCount: t.followingsCount,
			spamCount: 0,
			netStats: s
		}) : "spam-followers" === e ? pt({
			postsCount: 0,
			usersCount: t.followersCount,
			spamCount: t.spamFollowersCount,
			netStats: s
		}) : "spam-followings" === e ? pt({
			postsCount: 0,
			usersCount: t.followingsCount,
			spamCount: t.spamFollowingsCount,
			netStats: s
		}) : void 0
	}

	function pt({
		postsCount: e,
		usersCount: t,
		spamCount: s,
		netStats: a = {
			avgResponseTimePosts: null,
			avgResponseTimePeers: null,
			avgResponseTimeUser: null
		}
	}) {
		const n = a.avgResponseTimePosts || 1300,
			o = a.avgResponseTimePeers || 500,
			i = a.avgResponseTimeUser || 1300;
		let r = 0;
		r += e / 50 * (gt(b().sleepTimes.posts) + n); {
			const e = t / 50,
				a = gt(b().sleepTimes.peers) + o,
				n = (0 === e ? 0 : s / e) * (gt(b().sleepTimes.user) + i);
			r += e * Math.max(a, n)
		}
		return r
	}

	function gt(e) {
		let t = (e.max + e.min) / 2;
		return e.longBreak && (t += (e.longBreak.max + e.longBreak.min) / 2 / e.longBreak.every), t
	}
	O();
	const ft = {
		root: {
			marginBottom: k().default.space.g2,
			...k().default.relative()
		},
		autoscrollAnchor: { ...k().default.absolute(". . 0 0")
		}
	};
	class Ct extends k().default.Component {
		constructor(e) {
			super(e), this._onUsernameInput = e => {
				const t = this.props.task.id;
				y().transaction((s => {
					s.insights.tasks.find((e => e.id === t)).snapshot.usernameInput = e
				}))
			}, this._updateUser = () => {
				const e = this.props.task.id;
				y().transaction((t => {
					const s = t.insights.tasks.find((t => t.id === e));
					s.snapshot.user = null, s.snapshot.usernameErrorCode = null
				})), this.setState({
					isUserFound: !1
				}), this.setState({
					isFetchingUserDetails: !1
				}), clearTimeout(this._fetchUserInfoTimer);
				const t = this.props.task.snapshot.usernameInput,
					s = t.trim().replace(/^@+/, "");
				s && (this._fetchUserInfoTimer = setTimeout((async() => {
					this.setState({
						isFetchingUserDetails: !0
					});
					const e = this.props.task;
					if (s !== e.snapshot.usernameInput && t !== e.snapshot.usernameInput) return;
					const a = await I().chromeBus.send("ig-api.fetch-user-details", s, {
						ignoreCache: !0
					});
					this.setState({
						isFetchingUserDetails: !1
					});
					const n = a.result;
					a.error ? O().default.clearUserDetails.dispatch(e.id, a.error.code) : n && n.isPrivate && n.userId !== this.props.viewerId ? O().default.clearUserDetails.dispatch(e.id, "private-user") : (this.setState({
						isUserFound: !0
					}), O().default.applyUserDetails.dispatch(e.id, n))
				}), 600))
			}, this._onUsernameChange = () => {
				v().gaController.sendEvent("user", "insights:username-change"), O().default.blurUsername.dispatch(this.props.task.id)
			}, this._onFollowersInput = e => {
				O().default.inputFollowersCount.dispatch(this.props.task.id, e)
			}, this._onFollowersChange = () => {
				v().gaController.sendEvent("user", "insights:followers-change"), O().default.blurFollowersCount.dispatch(this.props.task.id)
			}, this._onFollowersMaxClick = () => {
				v().gaController.sendEvent("user", "insights:max-followers-click"), O().default.clickFollowersMax.dispatch(this.props.task.id)
			}, this._onFollowersMaxLongPress = () => {
				y().transaction((e => {
					e.insights.tasks.find((e => e.id === this.props.task.id)).snapshot.maxFollowersCount = 1e5
				}))
			}, this._onFollowingsInput = e => {
				O().default.inputFollowingsCount.dispatch(this.props.task.id, e)
			}, this._onFollowingsChange = () => {
				v().gaController.sendEvent("user", "insights:followings-change"), O().default.blurFollowingsCount.dispatch(this.props.task.id)
			}, this._onFollowingsMaxClick = () => {
				v().gaController.sendEvent("user", "insights:max-followings-click"), O().default.clickFollowingsMax.dispatch(this.props.task.id)
			}, this._onFollowingsMaxLongPress = () => {
				y().transaction((e => {
					e.insights.tasks.find((e => e.id === this.props.task.id)).snapshot.maxFollowingsCount = 1e5
				}))
			}, this._onDeleteClick = () => {
				v().gaController.sendEvent("user", "insights:delete-click"), this.setState({
					isDeleted: !0
				})
			}, this._onDeleteAnimationEnd = () => {
				O().default.clickDelete.dispatch(this.props.task.id)
			}, this._onDetailsClick = () => {
				const e = this.props.task;
				y().transaction((t => {
					t.analytics.selectedCard === e.id ? t.analytics.isReportOpen = !t.analytics.isReportOpen : (t.analytics.isReportOpen = !0, t.analytics.selectedCard = e.id, t.insights.filters.query = "", t.insights.filters.posts = "all", t.insights.filters.accounts = "all")
				}))
			}, this._onRetryClick = () => {
				v().gaController.sendEvent("user", "insights:retry-click"), O().default.clickRetry.dispatch(this.props.task.id)
			}, this._onAbortClick = () => {
				v().gaController.sendEvent("user", "insights:abort-click"), O().default.clickAbort.dispatch(this.props.task.id)
			}, this._onSwitchChange = () => {
				v().gaController.sendEvent("user", "insights:switch-change"), O().default.clickSwitch(this.props.task.id)
			}, this._onConfigClick = () => {
				v().gaController.sendEvent("user", "insights:config-click"), O().default.clickConfig.dispatch(this.props.task.id)
			}, this._onStartClick = () => {
				var e;
				v().gaController.sendEvent("user", "insights:start-click"), O().default.clickStart.dispatch(this.props.task.id), it.save(null === (e = this.props.task.user) || void 0 === e ? void 0 : e.avatarUrl)
			}, this._onCancelClick = () => {
				v().gaController.sendEvent("user", "insights:cancel-click"), O().default.clickCancel.dispatch(this.props.task.id)
			}, this._onCloneClick = () => {
				v().gaController.sendEvent("user", "insights:clone-click"), O().default.clickClone.dispatch(this.props.task.id)
			}, this._autoscrollAnchorRef = k().default.createRef(), this._fetchUserInfoTimer = null, this._initialNetStats = { ...e.netStats
			}, this.state = {
				isUserFound: !0,
				isFetchingUserDetails: !1,
				isDeleted: !1
			}
		}
		componentDidMount() {
			const e = this.props.task.snapshot;
			if (e.usernameInput && !e.user && this._updateUser(), Qe.isAutoscrollToNewTask()) {
				Qe.setAutoscrollToNewTask(!1);
				this._autoscrollAnchorRef.current.scrollIntoViewIfNeeded()
			}
		}
		componentDidUpdate(e) {
			e.task.snapshot.usernameInput !== this.props.task.snapshot.usernameInput && this._updateUser()
		}
		render() {
			return Glamor.createElement(S().default.InsightsTaskCard, {
				style: ft.root,
				selected: this.props.isReportOpen && !this.props.task.cardExpanded && this.props.selectedCard === this.props.task.id,
				deleted: this.state.isDeleted,
				onDeleteAnimationEnd: this._onDeleteAnimationEnd
			}, Glamor.createElement(k().default.Fragment, null, this._renderAutoscrollAnchor(), this.props.task.cardExpanded ? this._renderContentExpanded() : this._renderContentCollapsed()))
		}
		_renderAutoscrollAnchor() {
			return Glamor.createElement("div", {
				css: ft.autoscrollAnchor,
				ref: this._autoscrollAnchorRef
			})
		}
		_renderContentExpanded() {
			const e = this.props.task,
				t = e.snapshot,
				s = ut.getTimeForSnapshot(e, this._initialNetStats);
			return Glamor.createElement(S().default.InsightsTaskCardContentExpanded, {
				avatarUrl: t.user ? t.user.avatarUrl : null,
				username: {
					value: t.usernameInput,
					errorText: Qe.getUserErrorMessage(e)
				},
				followers: {
					value: t.followersCount,
					max: t.maxFollowersCount
				},
				followings: {
					value: t.followingsCount,
					max: t.maxFollowingsCount
				},
				timeText: Qe.renderTime(s),
				avatarShaking: this.state.isFetchingUserDetails,
				userNotFound: "missing-user" === t.usernameErrorCode,
				startDisabled: this._isStartDisabled(),
				withCancelButton: "planned" !== e.peerScrapeStatus,
				onUsernameInput: this._onUsernameInput,
				onUsernameChange: this._onUsernameChange,
				onFollowersInput: this._onFollowersInput,
				onFollowersChange: this._onFollowersChange,
				onFollowersMaxClick: this._onFollowersMaxClick,
				onFollowersMaxLongPress: this._onFollowersMaxLongPress,
				onFollowingsInput: this._onFollowingsInput,
				onFollowingsChange: this._onFollowingsChange,
				onFollowingsMaxClick: this._onFollowingsMaxClick,
				onFollowingsMaxLongPress: this._onFollowingsMaxLongPress,
				onStartClick: this._onStartClick,
				onCancelClick: this._onCancelClick,
				onCloneClick: this._onCloneClick,
				onDeleteClick: this._onDeleteClick
			})
		}
		_renderContentCollapsed() {
			const e = this.props,
				t = e.task,
				s = t.user || t.snapshot.user,
				a = ut.getUsersTimeLeft(t, e.netStats),
				n = ut.getUsersProgress(t, e.netStats),
				o = {
					running: "in progress",
					pending: "pending",
					paused: "stopped",
					error: "error",
					completed: "completed"
				}[t.peerScrapeStatus] || null;
			return Glamor.createElement(S().default.InsightsTaskCardContentCollapsed, {
				status: t.peerScrapeStatus,
				username: s ? s.username : null,
				avatarUrl: s ? s.avatarUrl : null,
				progress: n,
				timeLeftText: Qe.renderTime(a),
				errorText: t.peerScrapeError ? t.peerScrapeError.message : null,
				userNotFound: "missing-user" === t.snapshot.usernameErrorCode,
				stats: [!!o && {
					label: "credibility",
					value: this._renderCredibility()
				}, !!o && {
					label: "status",
					value: `${Math.round(100*n)}%, ${o}`
				}],
				onSwitchChange: this._onSwitchChange,
				onAbortClick: this._onAbortClick,
				onRetryClick: this._onRetryClick,
				onConfigClick: this._onConfigClick,
				onDetailsClick: this._onDetailsClick
			})
		}
		_renderCredibility() {
			const e = L().default(this.props.task.resultCounters.userCredibility);
			return Glamor.createElement(k().default.Fragment, null, Glamor.createElement("span", {
				style: {
					color: e.color
				}
			}, e.value), ", ", e.label)
		}
		_isStartDisabled() {
			if (this.state.isFetchingUserDetails) return !0;
			if (!this.state.isUserFound) return !0;
			if (!this.props.hasInsightsFeature) return !0;
			const e = this.props.task.snapshot;
			return !e.usernameInput || (!e.user || !!e.usernameErrorCode)
		}
	}
	var wt = y().influx((e => ({
		viewerId: e.authStatus.userId,
		isReportOpen: e.analytics.isReportOpen,
		selectedCard: e.analytics.selectedCard,
		netStats: e.insights.netStats,
		hasInsightsFeature: T().stateProxy.hasPro({
			feature: "insights"
		})
	})))(Ct);
	k(), S(), F(), R(), y(), D(), E(), v(), C(), M();
	const vt = {
		pricingRequested: !1,
		apiSender: new(R().Sender)({
			urlPrefix: F().env.options.apiUrl
		}),
		defaultCountry: "United States",
		statesOfAmerica: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"],
		fsamLink: null,
		fsamLinkRequestedAt: -1,
		init: function () {
			this.capturePageEvents(), this.sendGaEventsWhenPurchasing()
		},
		openBilling: function (e = null) {
			y().transaction((t => {
				t.sidebar.isOpen = !0, t.sidebar.selectedTabId = "tab-billing", t.billing.navigation.isBodyOpen = !0, t.tagAssist.shown = !1, t.coverAssist.shown = !1, e && (t.billing.recentFeature = e)
			}))
		},
		capturePageEvents: function () {
			E().iframeBus.on("fspring.close", this.closeFSpringPopup), E().iframeBus.on("fspring.subscription-success", (() => {
				chrome.runtime.sendMessage({
					name: "fspring.subscription-success"
				})
			})), E().iframeBus.on("fspring.subscription-failure", (() => {
				chrome.runtime.sendMessage({
					name: "fspring.subscription-failure"
				})
			}))
		},
		closeFSpringPopup: function () {
			y().transaction((e => {
				e.billing.purchasingPlan = null
			}))
		},
		sendGaEventsWhenPurchasing: function () {
			y().model.observe((e => e.billing.purchasingPlan), (e => {
				if (!e) return;
				const t = y().model.state.billing.recentFeature || "billing";
				v().gaController.sendEvent("user", `upgrade:from-${t}-${e.id}`)
			}))
		},
		updatePricing: async function () {
			var e, t, s;
			if (this.pricingRequested) return;
			const a = await D().igApi.fetchLoginActivity();
			let n = this.defaultCountry;
			if ((null === (e = a.result) || void 0 === e || null === (t = e.data) || void 0 === t || null === (s = t.sessions) || void 0 === s ? void 0 : s.length) > 0) {
				n = ((a.result.data.sessions.find((e => (e.location || "").split(", ").length > 1)) || a.result.data.sessions[0]).location || "").split(", "), n = n[n.length - 1], "" === !n ? n = null : this.statesOfAmerica.includes(n) && (n = this.defaultCountry)
			}
			const o = "/fspring/pricing",
				i = n !== this.defaultCountry ? `United States,${n}` : this.defaultCountry,
				r = Object.keys(F().env.options.billingPlans).join(","),
				l = await this.apiSender.send(o, {
					query: {
						countryNames: i,
						productIds: r
					}
				});
			if (l && "ok" === l.status) {
				this.pricingRequested = !0;
				const {
					countries: e,
					pricing: t
				} = l, s = n ? e[n] : null;
				y().transaction((e => {
					e.billing.countryIso = s, e.billing.pricing = t
				}))
			} else {
				const e = l.error;
				"TypeError" === e.name && "Failed to fetch" === e.message || C().sentryController.sendError(`Unexpected API error at ${o}`, "error", {
					error: e
				}, {
					actor: "auth"
				})
			}
		},
		onFsamClick: function () {
			let e = `https://${F().env.options.storefront}/account`;
			const t = Date.now();
			this.fsamLink = t - this.fsamLinkRequestedAt < 828e5 ? this.fsamLink : null;
			(this.fsamLink ? Promise.resolve() : M().billingConnector.accountManagementUrl(null, y().model.state.billing.account.token).then((e => {
				this.fsamLink = e.url, this.fsamLinkRequestedAt = t
			}))).then((() => {
				this.fsamLink && (e = `${this.fsamLink}#/subscriptions`), chrome.tabs.create({
					url: e,
					active: !0
				})
			})).catch((() => {
				chrome.tabs.create({
					url: e,
					active: !0
				})
			}))
		}
	};

	function kt() {
		return (kt = Object.assign || function (e) {
			for (var t = 1; t < arguments.length; t++) {
				var s = arguments[t];
				for (var a in s) Object.prototype.hasOwnProperty.call(s, a) && (e[a] = s[a])
			}
			return e
		}).apply(this, arguments)
	}
	k(), S(), b(), v(), y(), d(), k(), S(), y(), T(), M();
	let St = -1,
		bt = -1;
	const yt = e => null == e,
		_t = (e, t = null) => null != e ? e : t;
	k().default.Image.registerImages({
		"billing-form-mediator.inssist": "inssist-mini.png:24:24",
		"billing-form-mediator.everliker": "everliker-mini.png:24:24"
	});
	const Et = {
		selectedPlan: {
			maxWidth: 240,
			...k().default.absolute("0 0 . . 1"),
			...k().default.shadow.sh6,
			paddingTop: k().default.space.g5,
			paddingBottom: k().default.space.g3,
			backgroundColor: k().default.color.bgLight2,
			borderRadius: 8
		}
	};
	class Tt extends k().default.Component {
		static getDerivedStateFromProps(e, t) {
			const s = e.billing.snapshot || {},
				a = {
					panel: _t(s.panel),
					accountScreen: _t(s.accountScreen, null),
					accountForm: _t(s.accountForm, {
						email: "",
						password: "",
						firstName: "",
						lastName: "",
						errors: {
							email: !1,
							password: !1,
							firstName: !1,
							lastName: !1
						}
					}),
					statusText: _t(s.statusText),
					statusButtons: _t(s.statusButtons),
					confirmationCodeText: _t(s.confirmationCodeText),
					confirmationCodeError: _t(s.confirmationCodeError, !1),
					passwordResetValue: _t(s.passwordResetValue),
					passwordResetCodeText: _t(s.passwordResetCodeText),
					passwordResetCodeError: _t(s.passwordResetCodeError, !1),
					loading: _t(s.loading, !1)
				};
			if (!t.isAccountFormTouched) {
				if (e.authStatus.email && (a.accountForm.email || (a.accountForm.email = e.authStatus.email)), e.authStatus.fullName) {
					const t = e.authStatus.fullName.split(" "),
						s = t.shift(),
						n = t.join(" ");
					!a.accountForm.firstName && s && (a.accountForm.firstName = s), !a.accountForm.lastName && n && (a.accountForm.lastName = n)
				}
				a.accountForm.password = ""
			}
			return a
		}
		constructor(e) {
			super(e), At.call(this), this.state = {}
		}
		render() {
			return Glamor.createElement(S().default.BillingForm, {
				disabled: this.state.loading
			}, this.renderContent())
		}
		renderContent() {
			const {
				email: e
			} = this.props.billing.account, {
				accountScreen: t,
				accountForm: s,
				confirmationCodeText: a,
				passwordResetValue: n,
				passwordResetCodeText: o
			} = this.state;
			if ("auth" === t) return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(S().default.BillingFSpringAuthSection, kt({}, s, {
				onChange: this._onAccountFormChange,
				onCancelClick: this._onGoToStorefrontClick,
				onNextClick: this._onNextClick,
				disableInputs: !yt(a),
				hideActions: !yt(a)
			})), this._renderCodeSection(), this._renderStatusSection(), this._renderSelectedPlanSection());
			if ("login" === t) return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(S().default.BillingFSpringLoginSection, kt({}, s, {
				onChange: this._onAccountFormChange,
				onCancelClick: this._onGoToStorefrontClick,
				onIForgotPasswordClick: this._onIForgotPasswordClick,
				onLoginClick: this._onLoginClick,
				disableInputs: !yt(a),
				hideActions: !yt(a)
			})), this._renderCodeSection(), this._renderStatusSection(), this._renderSelectedPlanSection());
			if ("create-account" === t) return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(S().default.BillingFSpringCreateAccountSection, kt({}, s, {
				onChange: this._onAccountFormChange,
				onCancelClick: this._onGoToStorefrontClick,
				onCreateAccountClick: this._onCreateAccountClick,
				disableInputs: !yt(a),
				hideActions: !yt(a)
			})), this._renderCodeSection(), this._renderStatusSection(), this._renderSelectedPlanSection());
			if ("reset" === t) {
				const e = !yt(o);
				return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(S().default.BillingFSpringResetSection, kt({}, s, {
					newPassword: n,
					onNewPasswordChange: !e && this._onNewPasswordChange,
					onResetPasswordClick: !e && this._onResetPasswordClick
				})), this._renderResetCodeSection(), this._renderStatusSection())
			}
			return "reset-confirm" === t ? Glamor.createElement(S().default.BillingFSpringResetConfirmSection, {
				onOkClick: this._onResetPasswordAcknowledgeClick
			}) : "logout-confirm" === t ? Glamor.createElement(S().default.BillingFSpringLogoutConfirmSection, {
				onOkClick: this._onLogoutAcknowledgeClick
			}) : "delete-confirm" === t ? Glamor.createElement(S().default.BillingFSpringDeleteConfirmSection, {
				onOkClick: this._onDeleteAcknowledgeClick
			}) : "manage-account" === t ? Glamor.createElement(S().default.BillingFSpringManageAccountSection, kt({
				email: e
			}, this._subscriptions(), {
				onDeleteClick: this._onDeleteClick,
				onDeleteCancelClick: this._onDeleteCancelClick
			})) : "delete" === t ? Glamor.createElement(S().default.BillingFSpringManageAccountSection, kt({
				email: e
			}, this._subscriptions(), {
				onDeleteConfirmClick: this._onDeleteConfirmClick,
				onDeleteCancelClick: this._onDeleteCancelClick
			})) : null
		}
		_renderCodeSection() {
			const {
				accountScreen: e,
				accountForm: t,
				confirmationCodeText: s,
				confirmationCodeError: a
			} = this.state;
			if (yt(s)) return null;
			let n = null;
			const {
				firstName: o,
				lastName: i,
				email: r
			} = t;
			"create-account" === e && o && i && (n = k().default.emailTypoChecker.check({
				firstName: o,
				lastName: i,
				email: r
			}));
			const l = Glamor.createElement("p", null, "Please confirm your email address by entering a code we sent to you from ", Glamor.createElement("strong", null, "no-reply@inssist.com"), "."),
				c = n ? Glamor.createElement("div", {
					css: k().default.row
				}, Glamor.createElement("div", {
					style: {
						fontSize: 21
					}
				}, "🧐"), Glamor.createElement(k().default.Spacer, {
					width: "g0h"
				}), Glamor.createElement("div", {
					css: k().default.column
				}, Glamor.createElement("p", null, "The email address you entered appears to have a typo. Did you mean to enter ", Glamor.createElement("strong", null, n), " email address instead?"), Glamor.createElement(k().default.Spacer, {
					height: "g0h"
				}), Glamor.createElement("div", null, "If you made a typo, simply click Cancel."), Glamor.createElement(k().default.Spacer, {
					height: "g0h"
				}))) : Glamor.createElement("p", null, "If you have not received a code, please check the spam folder, email address or send a new code.");
			return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), Glamor.createElement(S().default.BillingCodeSection, {
				value: s,
				text: Glamor.createElement(k().default.Fragment, null, l, Glamor.createElement(k().default.Spacer, {
					height: "g1"
				}), c),
				hasError: a,
				onChange: this._onConfirmationCodeChange
			}), Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), Glamor.createElement("div", {
				css: k().default.row
			}, Glamor.createElement(k().default.ActionButton, {
				label: "CONFIRM EMAIL",
				shadow: k().default.color.linkShadow,
				onClick: this._onConfirmEmailClick
			}), Glamor.createElement(k().default.Spacer, {
				width: "g3"
			}), Glamor.createElement(k().default.LinkButton, {
				label: "CANCEL",
				cancel: !0,
				onClick: this._onCancelConfirmEmailClick
			})))
		}
		_renderResetCodeSection() {
			const {
				passwordResetCodeText: e,
				passwordResetCodeError: t
			} = this.state;
			return yt(e) ? null : Glamor.createElement(k().default.Fragment, null, Glamor.createElement(S().default.BillingCodeSection, {
				value: e,
				text: "We’ve sent a verification code to your email address. Please enter it below:",
				hasError: t,
				onChange: this._onPasswordResetCodeChange
			}), Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), Glamor.createElement("div", {
				css: k().default.row
			}, Glamor.createElement(k().default.ActionButton, {
				label: "CONFIRM RESET",
				shadow: k().default.color.linkShadow,
				onClick: this._onConfirmPasswordResetClick
			}), Glamor.createElement(k().default.Spacer, {
				width: "g3"
			}), Glamor.createElement(k().default.LinkButton, {
				label: "CANCEL",
				cancel: !0,
				onClick: this._onCancelPasswordResetClick
			})), Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}))
		}
		_renderSelectedPlanSection() {
			const {
				selectedPlan: e
			} = this.props.billing;
			if (!e) return null;
			const t = this.props.authStatus.username,
				s = "inssist-pro-monthly" === e.id ? ["Best for Casual use", "Unlimited accounts", "Paid monthly, cancel any time"] : "inssist-pro-lifetime" === e.id ? ["Best for Creatives", Glamor.createElement("div", null, "Activates for this account, ", Glamor.createElement("strong", null, "@", t)), "Paid once"] : "inssist-pro-infinite" === e.id ? ["Best for Businesses", "Unlimited accounts", "Paid once"] : null;
			return Glamor.createElement("div", {
				css: [k().default.column, Et.selectedPlan]
			}, Glamor.createElement(S().default.BillingPlanRenderer, kt({}, e, {
				benefits: s,
				padding: k().default.padding("0 20 0 50"),
				onChangePlanClick: this._onChangePlanClick
			})))
		}
		_renderStatusSection() {
			const {
				statusText: e
			} = this.state;
			if (!yt(e)) return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), Glamor.createElement(S().default.BillingStatusSection, {
				text: e,
				buttons: this._enrichStatusButtons()
			}))
		}
		_subscriptions() {
			const {
				subscriptions: e
			} = this.props.billing;
			if (!e) return null;
			const t = [];
			return e["inssist-pro-monthly"] && "active" === e["inssist-pro-monthly"].state && t.push({
				label: "INSSIST PRO",
				icon: "billing-form-mediator.inssist"
			}), e["everliker-pro-monthly"] && "active" === e["everliker-pro-monthly"].state && t.push({
				label: "EVERLIKER PRO",
				icon: "billing-form-mediator.everliker"
			}), 0 === t.length ? null : {
				subscriptions: t,
				onFsamClick: this._onFsamClick
			}
		}
		_showVerificationCodeSnackbar(e) {
			setTimeout((() => y().transaction((t => {
				t.billing.verificationCodeEmail = e
			}))), 1500)
		}
		_hideVerificationCodeSnackbar() {
			setTimeout((() => y().transaction((e => {
				e.billing.verificationCodeEmail = null
			}))), 1500)
		}
		_enrichStatusButtons() {
			return this.state.statusButtons && 0 !== this.state.statusButtons.length ? this.state.statusButtons.map((e => {
				const t = { ...e
				};
				return "CREATE NEW ACCOUNT" === e.label && (t.onClick = this._onGoToCreateAccountScreen), "SEND A NEW CODE" === e.label && (t.onClick = this._onResendCodeClick), "I FORGOT MY PASSWORD" === e.label && (t.onClick = this._onIForgotPasswordClick), "LOGIN INSTEAD" === e.label && (t.onClick = this._onGoToLoginScreen), t
			})) : null
		}
		_reset(e, t) {
			t = t || {
				form: !0,
				errors: !0,
				status: !0,
				code: !0,
				password: !0,
				loading: !0
			};
			const {
				form: s,
				errors: a,
				status: n,
				code: o,
				password: i,
				loading: r
			} = t;
			s && (this.setState({
				isAccountFormTouched: !1
			}), e.accountForm.email = "", e.accountForm.password = "", e.accountForm.firstName = "", e.accountForm.lastName = ""), a && (e.accountForm.errors.email = !1, e.accountForm.errors.password = !1, e.accountForm.errors.firstName = !1, e.accountForm.errors.lastName = !1), n && (e.statusText = null, e.statusButtons = null), o && (e.confirmationCodeText = null, e.confirmationCodeError = !1), i && (e.passwordResetValue = null, e.passwordResetCodeText = null, e.passwordResetCodeError = !1), r && (e.loading = !1)
		}
		getSnapshot() {
			return d().default.cloneDeep(this.state)
		}
		applySnapshot(e) {
			this.setState(e), y().transaction((t => {
				t.billing.snapshot = e
			}))
		}
		updateSnapshot(e) {
			const t = this.getSnapshot(),
				s = e(t);
			return this.applySnapshot(t), s
		}
	}
	var At = function () {
			this._onFsamClick = () => {
				vt.onFsamClick()
			}, this._onChangePlanClick = () => {
				y().transaction((e => {
					e.billing.selectedPlan = null, e.billing.snapshot.panel = "storefront", e.billing.snapshot.accountScreen = null
				}))
			}, this._onLogoutAcknowledgeClick = () => {
				this.updateSnapshot((e => {
					e.panel = "storefront", e.accountScreen = null
				}))
			}, this._onDeleteClick = () => {
				this.updateSnapshot((e => {
					e.accountScreen = "delete"
				}))
			}, this._onCancelClick = () => {
				this.updateSnapshot((e => {
					e.panel = "storefront", e.accountScreen = null
				}))
			}, this._onDeleteConfirmClick = () => {
				M().billingConnector.deleteAccount(this, this.props.billing.account.token).then((() => {
					this.updateSnapshot((e => {
						this._reset(e, {
							status: !0,
							form: !0,
							code: !0,
							password: !0
						}), e.accountScreen = "delete-confirm"
					})), y().transaction((e => {
						e.billing.account = {
							email: null,
							token: null
						}
					}))
				})).catch((e => {
					this.updateSnapshot((t => {
						"unauthorized" === e.status && (this._reset(t, {
							form: !0,
							status: !0,
							code: !0,
							password: !0,
							loading: !0
						}), t.accountScreen = null, t.statusButtons = null, t.statusText = "Oops... Your session has expired, please log back in to delete your account.", y().transaction((e => {
							e.billing.account = {
								email: null,
								token: null
							}, e.billing.snapshot = t
						})))
					}))
				}))
			}, this._onDeleteAcknowledgeClick = () => {
				this.updateSnapshot((e => {
					e.panel = "storefront", e.accountScreen = null
				}))
			}, this._onDeleteCancelClick = () => {
				this.updateSnapshot((e => {
					e.panel = "storefront", e.accountScreen = null
				}))
			}, this._onAccountFormChange = e => {
				this.setState({
					isAccountFormTouched: !0
				}), this.updateSnapshot((t => {
					this._reset(t, {
						status: !0,
						code: !0,
						loading: !0
					}), e.email !== t.accountForm.email && (t.accountForm.email = e.email, t.accountForm.errors.email = !1), e.firstName !== t.accountForm.firstName && (t.accountForm.firstName = e.firstName, t.accountForm.errors.firstName = !1), e.lastName !== t.accountForm.lastName && (t.accountForm.lastName = e.lastName, t.accountForm.errors.lastName = !1), e.password !== t.accountForm.password && (t.accountForm.password = e.password, t.accountForm.errors.password = !1)
				}))
			}, this._onGoToStorefrontClick = () => {
				this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						form: !0,
						errors: !0,
						code: !0,
						loading: !0
					}), e.panel = "storefront"
				}))
			}, this._onIForgotPasswordClick = () => {
				this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						errors: !0,
						code: !0,
						loading: !0
					}), e.accountScreen = "reset", e.passwordResetValue = "", e.passwordResetCodeText = null
				}))
			}, this._onGoToCreateAccountScreen = () => {
				this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						errors: !0,
						code: !0,
						loading: !0
					}), e.accountScreen = "create-account"
				}))
			}, this._onGoToLoginScreen = () => {
				this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						errors: !0,
						code: !0,
						loading: !0
					}), e.accountScreen = "login"
				}))
			}, this._onNextClick = () => {
				const {
					email: e
				} = this.state.accountForm;
				this.updateSnapshot((t => (t.accountForm.errors = {
					email: e.length < 1,
					password: !1
				}, t.accountForm.errors.email ? (t.statusText = "Email is required to store your purchases and reuse them across multiple devices.", !0) : (t.statusText = null, !1)))) || M().billingConnector.isTaken(this, e).then((e => {
					e.taken ? y().transaction((e => {
						e.billing.snapshot.accountScreen = "login"
					})) : y().transaction((e => {
						e.billing.snapshot.accountScreen = "create-account"
					}))
				})).catch((e => {
					console.log(e), this.updateSnapshot((t => {
						"bad-query" === e.status && (t.accountForm.errors.email = !0)
					}))
				}))
			}, this._onLoginClick = () => {
				const {
					email: e,
					password: t
				} = this.state.accountForm;
				this.updateSnapshot((s => (s.accountForm.errors = {
					email: e.length < 1,
					password: t.length < 4
				}, s.accountForm.errors.email || s.accountForm.errors.password ? (s.statusText = "Email and password required to sign in to Inssist account. If you do not have an Inssist account, create one instead.", !0) : (s.statusText = null, !1)))) || M().billingConnector.login(this, e, t).then((t => {
					this.updateSnapshot((e => {
						this._reset(e, {
							status: !0,
							form: !0,
							code: !0,
							password: !0
						}), "reset" === e.accountScreen ? e.accountScreen = "reset-confirm" : e.panel = "storefront"
					})), y().transaction((s => {
						s.billing.account = {
							email: e,
							token: t.token
						}, s.billing.selectedPlan && (s.billing.purchasingPlan = s.billing.selectedPlan, s.billing.selectedPlan = null)
					}))
				})).catch((e => {
					this.updateSnapshot((t => {
						"bad-query" === e.status ? t.accountForm.errors.email = !0 : "email-is-not-verified" === e.status && (t.confirmationCodeText = "")
					}))
				}))
			}, this._onCreateAccountClick = () => {
				const {
					email: e,
					password: t,
					firstName: s,
					lastName: a
				} = this.state.accountForm;
				if (this.updateSnapshot((n => (n.accountForm.errors = {}, e.length < 1 && (n.accountForm.errors.email = !0), t.length < 4 && (n.accountForm.errors.password = !0), s.length < 1 && (n.accountForm.errors.firstName = !0), a.length < 1 && (n.accountForm.errors.lastName = !0), n.confirmationCodeText = null, n.statusText = n.accountForm.errors.email || n.accountForm.errors.password || n.accountForm.errors.firstName || n.accountForm.errors.lastName ? "Please fill in remaining fields to create an Inssist account. First Name and Last Name are required by FastSpring." : null, Boolean(n.statusText))))) return;
				const n = T().stateProxy.allUsernames();
				M().billingConnector.register(this, e, t, s, a, n).then((() => {
					this._showVerificationCodeSnackbar(e), y().transaction((e => {
						e.billing.recordedUsernames = (e => Array.from(new Set(e)))([...e.billing.recordedUsernames, ...n])
					})), this.updateSnapshot((e => {
						this._reset(e, {
							status: !0,
							code: !0
						}), e.confirmationCodeText = ""
					})), y().transaction((t => {
						t.billing.account = {
							email: e
						}
					}))
				})).catch((e => {
					this.updateSnapshot((t => {
						("bad-query" === e.status || "email-already-in-use" === e.status) && (t.accountForm.errors.email = !0)
					}))
				}))
			}, this._onConfirmEmailClick = () => {
				if (this.updateSnapshot((e => {
						const {
							confirmationCodeText: t
						} = e;
						return 0 === t.length || 5 !== t.length ? (e.confirmationCodeError = !0, e.statusText = "Verification code does not match our records. Please check your inbox for the code we sent.", e.statusButtons = [{
							label: "SEND A NEW CODE"
						}], !0) : (this._reset(e, {
							status: !0
						}), !1)
					}))) return;
				const e = this.state.accountForm.email,
					t = this.state.confirmationCodeText;
				M().billingConnector.verifyEmail(this, e, t).then((() => {
					this._hideVerificationCodeSnackbar(), this._onLoginClick()
				})).catch((e => {
					this.updateSnapshot((t => {
						"bad-query" === e.status ? t.accountForm.errors.email = !0 : "account-not-found" === e.status ? (t.statusButtons = null, t.statusText = "No account found for the given email address. Please correct email address or create a new account instead.") : "email-already-verified" === e.status ? (t.confirmationCodeText = null, t.statusText = "This email address has been verified. Please login instead.") : "otp-does-not-match" === e.status && (t.confirmationCodeError = !0, t.statusButtons = [{
							label: "SEND A NEW CODE"
						}])
					}))
				}))
			}, this._onResendCodeClick = () => {
				const {
					email: e,
					password: t
				} = this.state.accountForm;
				this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						code: !0,
						loading: !0
					}), e.confirmationCodeText = "";
					const t = Date.now();
					return Math.round((St + 3e4 - t) / 1e3) > 0 && (e.statusText = "The last code was sent less than a minute ago. Please wait some time before requesting a new one.", e.statusButtons = [{
						label: "SEND A NEW CODE"
					}], !0)
				})) || (St = Date.now(), M().billingConnector.resendOtp(this, e, t).then((() => {
					this._showVerificationCodeSnackbar(e), this.updateSnapshot((e => {
						e.statusText = "We sent you a new email verification code. Please check your inbox for messages from no-reply@inssist.com.", e.statusButtons = [{
							label: "SEND A NEW CODE"
						}]
					}))
				})).catch((e => {
					this.updateSnapshot((t => {
						"account-not-found" === e.status ? t.statusText = "No account found for the given email address. Please update email address or create a new account instead." : "account-is-not-active" === e.status ? t.statusText = "This account has been disabled. Please create a new account instead." : "email-already-verified" === e.status && (t.confirmationCodeText = null, t.statusText = "This email address has already been verified. Please login instead.")
					}))
				})))
			}, this._onResetPasswordClick = e => {
				if (!e) return void this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						code: !0,
						loading: !0,
						password: !0
					}), e.panel = "storefront", e.accountScreen = null
				}));
				if (this.updateSnapshot((e => {
						this._reset(e, {
							status: !0,
							code: !0
						});
						const t = Date.now();
						return Math.round((bt + 3e4 - t) / 1e3) > 0 && (e.statusText = "The last code was sent less than a minute ago. Please wait some time before requesting a new one.", this.applySnapshot(e), !0)
					}))) return;
				const t = this.state.accountForm.email;
				bt = Date.now(), M().billingConnector.recoverPassword(this, t).then((() => {
					this._showVerificationCodeSnackbar(t), this.updateSnapshot((e => {
						e.passwordResetCodeText = ""
					}))
				})).catch((e => {
					this.updateSnapshot((t => {
						"account-not-found" === e.status && (t.statusButtons = null, t.statusText = "No account found for the given email address. Please correct email address or create a new account instead.")
					}))
				}))
			}, this._onConfirmPasswordResetClick = () => {
				if (this.updateSnapshot((e => {
						const {
							passwordResetCodeText: t
						} = e;
						return 0 === t.length || 5 !== t.length ? (e.passwordResetCodeError = !0, e.statusText = "Verification code does not match our records. Please check your inbox for the code we sent.", !0) : (this._reset(e, {
							status: !0
						}), !1)
					}))) return;
				const e = this.state.accountForm.email,
					t = this.state.passwordResetCodeText,
					s = this.state.passwordResetValue;
				M().billingConnector.changePassword(this, e, t, s).then((() => {
					this._hideVerificationCodeSnackbar(), this.setState({
						isAccountFormTouched: !0
					}), this.updateSnapshot((e => {
						e.accountForm.password = s
					})), this._onLoginClick()
				})).catch((e => {
					this.updateSnapshot((t => {
						"bad-query" === e.status ? t.accountForm.errors.email = !0 : "account-not-found" === e.status ? (t.statusButtons = null, t.statusText = "No account found for the given email address. Please correct email address or create a new account instead.") : "account-is-not-active" === e.status && (t.statusText = "This account has been disabled. Please create a new account instead.")
					}))
				}))
			}, this._onConfirmationCodeChange = e => {
				this.updateSnapshot((t => {
					this._reset(t, {
						status: !0,
						loading: !0
					}), t.confirmationCodeText = e, t.confirmationCodeError = !1
				}))
			}, this._onCancelConfirmEmailClick = () => {
				this._hideVerificationCodeSnackbar(), this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						code: !0,
						loading: !0
					})
				}))
			}, this._onPasswordResetCodeChange = e => {
				this.updateSnapshot((t => {
					this._reset(t, {
						status: !0,
						loading: !0
					}), t.passwordResetCodeText = e, t.passwordResetCodeError = !1
				}))
			}, this._onNewPasswordChange = e => {
				this.updateSnapshot((t => {
					this._reset(t, {
						status: !0,
						code: !0,
						loading: !0
					}), t.passwordResetValue = e, t.passwordResetCodeText = null
				}))
			}, this._onCancelPasswordResetClick = () => {
				this._hideVerificationCodeSnackbar(), this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						code: !0,
						password: !0,
						loading: !0
					}), e.panel = "storefront", e.accountScreen = null
				}))
			}, this._onResetPasswordAcknowledgeClick = () => {
				this.updateSnapshot((e => {
					this._reset(e, {
						status: !0,
						code: !0,
						password: !0,
						loading: !0
					}), e.panel = "storefront", e.accountScreen = null
				}))
			}
		},
		Pt = y().influx((e => ({
			billing: e.billing,
			authStatus: e.authStatus,
			hasPro: T().stateProxy.hasPro(),
			hasProPaid: T().stateProxy.hasProPaid(),
			hasProPromocode: T().stateProxy.hasProPromocode()
		})))(Tt);
	k(), T(), y(), k(), S(), y();
	class It extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onLikeClick = () => {
				this.props.onLikeClick && this.props.onLikeClick(this.props.user)
			}, t
		}
		render() {
			return this.props.hidden ? null : Glamor.createElement(S().default.LikesColumnRenderer, {
				likes: this.props.likes,
				onLikeClick: this._onLikeClick
			})
		}
	}
	var Ft = y().influx(((e, t) => {
		const s = (e.igTask.likeStatus || {})[t.user.userId];
		return {
			likes: s && s > 0 ? s : 0,
			hidden: t.user.userId === e.authStatus.userId || t.user.isPrivate
		}
	}))(It);
	N(), k(), S(), I(), b(), y(), T(), z(), y();
	var xt = {
		fetchAnalytics: async function (e) {
			let t = null;
			Gt[e] ? t = Gt[e] : (t = await z().default.getAnalytics(), Gt = {
				[e]: t
			});
			return t
		},
		onLikeClick: async function (e) {
			return Ze.likeUser(e)
		},
		isFollower: function e(t, s) {
			const a = e;
			a.analytics !== s && (a.analytics = s, a.followerIds = new Set([...s.nFollowers.map((e => e.userId)), ...s.followers.map((e => e.userId))]));
			if (a.followerIds.has(t.userId)) return !0;
			return t.isFollowingViewer || !1
		},
		isFollowing: function e(t, s) {
			const a = e;
			a.analytics !== s && (a.analytics = s, a.followingIds = new Set([...s.nFollowings.map((e => e.userId)), ...s.followings.map((e => e.userId))]));
			const n = y().model.state.igTask.followStatus;
			if (t.userId in n) return n[t.userId];
			if (a.followingIds.has(t.userId)) return !0;
			return t.isFollowedByViewer || !1
		}
	};
	let Gt = {};
	const Dt = {
		followersGraph: {
			columnWidth: 7,
			columnGapWidth: 2,
			columnMaxHeight: 40
		},
		unfollowersGraph: {
			columnWidth: 7,
			columnGapWidth: 2,
			columnMaxHeight: 40
		}
	};
	class Ot extends k().default.Component {
		constructor(e) {
			super(e), this._onCheckNowClick = () => {
				I().chromeBus.send("analytics.check-now")
			}, this._onStopClick = () => {
				I().chromeBus.send("analytics.stop")
			}, this._onErrorClick = () => {
				y().transaction((e => {
					e.analytics.error = null
				}))
			}, this._onRunAutomaticallyChange = () => {
				y().transaction((e => {
					e.analytics.runAutomatically = !e.analytics.runAutomatically
				}))
			}, this._onCloseClick = () => {
				y().transaction((e => {
					e.sidebar.isOpen = !1
				}))
			}, this._onCardSelect = (e, t) => {
				y().transaction((e => {
					0 !== t && 1 !== t || (e.analytics.filters.query = "", e.analytics.filters.accounts = "all"), 0 === t ? (e.analytics.isReportOpen = !e.analytics.isReportOpen || "nFollowers" !== e.analytics.selectedCard, e.analytics.selectedCard = "nFollowers") : 1 === t ? (e.analytics.isReportOpen = !e.analytics.isReportOpen || "uFollowers" !== e.analytics.selectedCard, e.analytics.selectedCard = "uFollowers") : e.analytics.isReportOpen = !1
				}))
			}, this._onRunInsightClick = () => {
				Qe.addTask(T().stateProxy.username())
			}, this.mounted = !1, this.state = {
				fetched: !1,
				nFollowersCard: null,
				uFollowersCard: null,
				timer: 0
			}
		}
		componentDidMount() {
			this.mounted = !0, this.timerChangeInterval = setInterval((() => {
				const e = (this.state.timer || 0) + 1;
				this.setState({
					timer: e
				})
			}), 6e4), this._updateAnalytics()
		}
		componentWillUnmount() {
			this.mounted = !1, clearTimeout(this.timerChangeInterval)
		}
		componentDidUpdate(e) {
			e.activeUserId !== this.props.activeUserId ? (this.setState({
				fetched: !1,
				nFollowersCard: null,
				uFollowersCard: null
			}), this._updateAnalytics()) : this.props.lastScanOn !== e.lastScanOn && this._updateAnalytics()
		}
		render() {
			const {
				runAutomatically: e,
				runningJobId: t,
				scanProgress: s,
				lastScanOn: a,
				nextScanOn: n
			} = this.props, o = {
				title: "Analytics",
				text: "Scan your account periodically to check who has followed and unfollowed you over time. Engage with your audience."
			}, i = {
				runAutomatically: e,
				lastScanOn: a,
				nextScanOn: null !== t ? `running, ${Math.round(s)}% finished...` : n,
				isRunning: null !== t,
				error: {
					hasError: Boolean(this.props.error),
					message: this.props.error
				},
				onCheckNowClick: this._onCheckNowClick,
				onStopClick: this._onStopClick,
				onErrorClick: this._onErrorClick,
				onRunAutomaticallyChange: this._onRunAutomaticallyChange
			};
			return this.props.lastScanOn ? this._renderData({
				header: o,
				status: i
			}) : this._renderNoData({
				header: o,
				status: i
			})
		}
		_renderNoData({
			header: e,
			status: t
		}) {
			const s = [{
				type: "graph",
				title: "New followers",
				content: Glamor.createElement("div", null, "No scan run yet."),
				marker: k().default.color.link,
				graph: null
			}, {
				type: "graph",
				title: "Unfollowers",
				content: Glamor.createElement("div", null, "No scan run yet."),
				marker: k().default.color.error,
				graph: null
			}, this._generatePostsCard()];
			return Glamor.createElement(S().default.AnalyticsSidePanel, {
				loading: !this.state.fetched,
				header: e,
				status: t,
				cards: s,
				insights: Glamor.createElement(Ms, null),
				onCloseClick: this._onCloseClick,
				onCardSelect: this._onCardSelect
			})
		}
		_renderData({
			header: e,
			status: t
		}) {
			const s = [];
			return this.state.nFollowersCard && s.push({ ...this.state.nFollowersCard,
				selected: this.props.isReportOpen && "nFollowers" === this.props.selectedCard
			}), this.state.uFollowersCard && s.push({ ...this.state.uFollowersCard,
				selected: this.props.isReportOpen && "uFollowers" === this.props.selectedCard
			}), (this.state.nFollowersCard || this.state.uFollowersCard) && s.push(this._generatePostsCard()), Glamor.createElement(S().default.AnalyticsSidePanel, {
				loading: !this.state.fetched,
				header: e,
				status: t,
				cards: s,
				insights: Glamor.createElement(Ms, null),
				onCloseClick: this._onCloseClick,
				onCardSelect: this._onCardSelect
			})
		}
		async _updateAnalytics() {
			const {
				lastScanOn: e
			} = this.props;
			if (!this.mounted) return;
			const t = await xt.fetchAnalytics(e);
			if (!this.mounted) return;
			if (this.state.fetched && e !== this.props.lastScanOn) return;
			const s = new Array(40).fill(0),
				a = new Array(40).fill(0);
			let n = N().default(new Date).getTime(),
				o = this.props.firstScanOn || n,
				i = 0,
				r = 0,
				l = 0,
				c = 0;
			for (let e = 0; e < 40; e++) {
				for (; i < t.nFollowers.length && t.nFollowers[i].on > n;) o > n && (o = n), s[e]++, l++, i++;
				for (; r < t.uFollowers.length && t.uFollowers[r].on > n;) o > n && (o = n), a[e]++, c++, r++;
				n -= 864e5
			}
			n = N().default(new Date).getTime();
			let u = 0;
			for (let e = 0; e < 40; e++) n < o ? (s[e] = null, a[e] = null) : u++, n -= 864e5;
			u = u || 1;
			const d = 1 === u ? "day." : `${u} days.`;
			let h = (t.followers || []).length;
			(0 === h || h > b().analytics.maxFollowersCount) && (h = b().analytics.maxFollowersCount);
			const m = {
					type: "graph",
					title: "New followers",
					content: 0 !== l ? Glamor.createElement("div", null, Glamor.createElement("b", {
						key: "b"
					}, l), " new followers in the past ", d) : Glamor.createElement("div", null, "No new followers in the past ", d),
					marker: k().default.color.link,
					graph: {
						items: s.map(((e, t) => ({
							value: e,
							color: k().default.color.link,
							labels: this._toLabels(e, t)
						}))).reverse(),
						graphStyle: Dt.followersGraph
					}
				},
				p = {
					type: "graph",
					title: "Unfollowers",
					content: 0 !== c ? Glamor.createElement("div", null, Glamor.createElement("b", {
						key: "b"
					}, c), " followers out of ", h + c, " were lost in the past ", d) : Glamor.createElement("div", null, "No followers lost in the past ", d),
					marker: k().default.color.error,
					graph: {
						items: a.map(((e, t) => ({
							value: e,
							color: k().default.color.error,
							labels: this._toLabels(e, t)
						}))).reverse(),
						graphStyle: Dt.unfollowersGraph
					}
				};
			this.setState({
				fetched: !0,
				nFollowersCard: m,
				uFollowersCard: p
			})
		}
		_toLabels(e, t) {
			return [0 === e ? "0" : null === e ? "NOT AVAILABLE" : e, 0 === t ? "TODAY" : 1 === t ? "YESTERDAY" : `${t} DAYS AGO`]
		}
		_generatePostsCard() {
			const {
				insightsTasks: e,
				activeUserId: t
			} = this.props;
			return e.some((e => e.user && e.user.userId === t)) ? null : {
				type: "graph",
				title: "Posts, tags and posting hours",
				content: Glamor.createElement("div", null, "Run Insights to collect account posts, posting times, hashtags performance and post engagement statistics."),
				button: {
					label: "RUN INSIGHT",
					onClick: this._onRunInsightClick
				},
				selectable: !1
			}
		}
	}
	var Bt = y().influx((e => ({
		activeUserId: e.authStatus.userId,
		runAutomatically: e.analytics.runAutomatically,
		runningJobId: e.analytics.runningJobId,
		firstScanOn: e.analytics.firstScanOn,
		lastScanOn: e.analytics.lastScanOn,
		nextScanOn: e.analytics.nextScanOn,
		scanProgress: e.analytics.scanProgress,
		error: e.analytics.error,
		isReportOpen: e.analytics.isReportOpen,
		selectedCard: e.analytics.selectedCard,
		insightsTasks: e.insights.tasks
	})))(Ot);
	k(), S(), y(), T(), v(), I(), E();
	var Ut, Lt, Rt, Mt, Nt = !1;

	function zt(e) {
		switch (e) {
			case "preload":
				return Rt;
			case "prefetch":
				return Mt;
			default:
				return Lt
		}
	}

	function Vt() {
		return Nt || (Nt = !0, Ut = {}, Lt = {}, Rt = {}, Mt = {}, Ut = function (e, t) {
			return function (s) {
				let a = zt(t);
				return a[s] ? a[s] : a[s] = e.apply(null, arguments).catch((function (e) {
					throw delete a[s], e
				}))
			}
		}), Ut
	}
	var Ht, $t, Wt = !1;

	function qt() {
		return Wt || (Wt = !0, Ht = {}, $t = Vt(), Ht = $t((function (e) {
			return new Promise((function (t, s) {
				if ([...document.getElementsByTagName("script")].some((t => t.src === e))) t();
				else {
					var a = document.createElement("script");
					a.async = !0, a.type = "text/javascript", a.charset = "utf-8", a.src = e, a.onerror = function (e) {
						a.onerror = a.onload = null, a.remove(), s(e)
					}, a.onload = function () {
						a.onerror = a.onload = null, t()
					}, document.getElementsByTagName("head")[0].appendChild(a)
				}
			}))
		}))), Ht
	}
	var jt, Yt, Kt = !1;

	function Zt() {
		return Yt || (Yt = function () {
			try {
				throw new Error
			} catch (t) {
				var e = ("" + t.stack).match(/(https?|file|ftp):\/\/[^)\n]+/g);
				if (e) return ("" + e[0]).replace(/^((?:https?|file|ftp):\/\/.+)\/[^/]+$/, "$1") + "/"
			}
			return "/"
		}()), Yt
	}

	function Jt() {
		return Kt || (Kt = !0, Yt = null, (jt = {}).getBundleURL = Zt), jt
	}
	var Xt, Qt, es, ts, ss = !1;

	function as(e) {
		if ("" === e) return ".";
		var t = "/" === e[e.length - 1] ? e.slice(0, e.length - 1) : e,
			s = t.lastIndexOf("/");
		return -1 === s ? "." : t.slice(0, s)
	}

	function ns(e, t) {
		if (e === t) return "";
		var s = e.split("/");
		"." === s[0] && s.shift();
		var a, n, o = t.split("/");
		for ("." === o[0] && o.shift(), a = 0;
			(a < o.length || a < s.length) && null == n; a++) s[a] !== o[a] && (n = a);
		var i = [];
		for (a = 0; a < s.length - n; a++) i.push("..");
		return o.length > n && i.push.apply(i, o.slice(n)), i.join("/")
	}

	function os() {
		return ss || (ss = !0, Xt = {}, Qt = Ae().resolve, Xt = function (e, t) {
			return ns(as(Qt(e)), Qt(t))
		}, es = as, Xt._dirname = es, ts = ns, Xt._relative = ts), Xt
	}
	var is, rs = !1;

	function ls() {
		return rs || (rs = !0, is = {}, is = qt()(Jt().getBundleURL() + os()("5VRNk", "4csc5")).then((() => t("2YvfX")()))), is
	}

	function cs(e, t) {
		return [t.map((e => e[0])), ...e.map((e => t.map((t => function (e) {
			(e = (e = (e = String(e)).replace(/\n/g, " ")).replace(/"/g, '""')).includes(",") && (e = `"${e}"`);
			return e
		}(t[1](e))))))].map((e => e.join(","))).join("\n")
	}
	V(), y();
	var us = {
		downloadResultsAsZip: async function (e) {
			const t = await ls(),
				s = await xt.fetchAnalytics(y().model.state.analytics.lastScanOn),
				a = s.nFollowers || [],
				n = s.uFollowers || [],
				o = JSON.stringify(a, null, 2),
				i = a.map((e => e.username)).join(","),
				r = cs(a, [
					["username", e => e.username],
					["user id", e => e.userId],
					["name", e => e.fullName],
					["is private", e => e.isPrivate ? "PRIVATE" : ""],
					["is verified", e => e.isVerified ? "VERIFIED" : ""],
					["follows viewer", e => xt.isFollower(e, s) ? "YES" : ""],
					["is followed by viewer", e => xt.isFollowing(e, s) ? "YES" : ""],
					["profile url", e => `https://instagram.com/${e.username}`],
					["avatar url", e => e.avatarUrl]
				]),
				l = JSON.stringify(n, null, 2),
				c = n.map((e => e.username)).join(","),
				u = cs(n, [
					["username", e => e.username],
					["user id", e => e.userId],
					["name", e => e.fullName],
					["is private", e => e.isPrivate ? "PRIVATE" : ""],
					["is verified", e => e.isVerified ? "VERIFIED" : ""],
					["follows viewer", e => xt.isFollower(e, s) ? "YES" : ""],
					["is followed by viewer", e => xt.isFollowing(e, s) ? "YES" : ""],
					["profile url", e => `https://instagram.com/${e.username}`],
					["avatar url", e => e.avatarUrl]
				]),
				d = new t;
			d.file("newfollowers.csv", r), d.file("newfollowers.json", o), d.file("newfollowers.txt", i), d.file("unfollowers.csv", u), d.file("unfollowers.json", l), d.file("unfollowers.txt", c);
			const h = await d.generateAsync({
				type: "blob"
			});
			V().utils.saveFile("inssist-analytics.zip", h), Fe.showRateUs(), Ue.showFollowUs()
		}
	};
	d(), k(), S(), v(), y();
	class ds extends k().default.Component {
		constructor(e) {
			super(e), this._onInput = e => {
				v().gaController.sendEvent("user", "analytics:filter-query"), this._updateFilters({
					key: "query",
					value: e,
					delay: 300
				})
			}, this._onClearClick = () => {
				this._updateFilters({
					key: "query",
					value: ""
				})
			}, this._onItemClick = (e, t) => {
				v().gaController.sendEvent("user", `insights:filter-${e}-${t}`), this._updateFilters({
					key: e,
					value: t
				})
			}, this._applyFiltersTimeout = null, this.state = { ...e.filters
			}
		}
		componentDidUpdate(e) {
			const t = e,
				s = this.props;
			(!t.isReportOpen && s.isReportOpen || t.selectedCard !== s.selectedCard && s.isReportOpen) && clearTimeout(this._applyQueryTimeout), d().default.isEqual(t.filters, s.filters) || this.setState({ ...s.filters
			})
		}
		render() {
			return Glamor.createElement(S().default.Filter, {
				query: this.state.query,
				placeholder: "Filter users",
				highlightTriangle: "all" !== this.state.accounts,
				sections: [{
					id: "accounts",
					title: "filter accounts",
					details: this.props.accountsTotalCount ? `${this.props.accountsFilteredCount} of ${this.props.accountsTotalCount}` : "",
					value: this.state.accounts,
					items: [{
						label: "All accounts",
						value: "all"
					}, {
						label: "My followers",
						value: "followers"
					}, {
						label: "My followings",
						value: "followings"
					}, {
						label: "Unilateral followers",
						value: "unilateral-followers"
					}, {
						label: "Unilateral followings",
						value: "unilateral-followings"
					}, {
						label: "Mutual followers",
						value: "mutual"
					}]
				}],
				onInput: this._onInput,
				onClearClick: this._onClearClick,
				onItemClick: this._onItemClick
			})
		}
		_updateFilters({
			key: e,
			value: t,
			delay: s = 0
		}) {
			this.setState({
				[e]: t
			}), y().transaction((e => {
				e.analytics.loading = !0
			})), clearTimeout(this._applyFiltersTimeout), this._applyFiltersTimeout = setTimeout((() => {
				y().transaction((e => {
					e.analytics.filters = { ...this.state
					}, e.analytics.loading = !1
				}))
			}), s)
		}
	}
	var hs = y().influx((e => ({
		isReportOpen: e.analytics.isReportOpen,
		selectedCard: e.analytics.selectedCard,
		filters: e.analytics.filters
	})))(ds);
	k(), S(), y();
	class ms extends k().default.Component {
		render() {
			return Glamor.createElement(S().default.LoadingOverlay, {
				show: this.props.show
			})
		}
	}
	var ps = y().influx((e => ({
		show: e.analytics.loading
	})))(ms);
	k(), y();
	class gs extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onResetFiltersClick = () => {
				y().transaction((e => {
					e.analytics.loading = !0
				})), setTimeout((() => {
					y().transaction((e => {
						e.analytics.filters.query = "", e.analytics.filters.accounts = "all", e.analytics.loading = !1
					}))
				}), 50)
			}, t
		}
		render() {
			return Glamor.createElement(k().default.Fragment, null, "No data found", Glamor.createElement(k().default.Spacer, {
				width: "g2"
			}), Glamor.createElement(k().default.LinkButton, {
				label: "RESET FILTERS",
				onClick: this._onResetFiltersClick
			}))
		}
	}
	const fs = {
		root: {
			height: "100%"
		}
	};
	class Cs extends k().default.Component {
		constructor(e) {
			super(e), this._renderLikesCell = e => Glamor.createElement(Ft, {
				user: e,
				onLikeClick: this._onLikeClick
			}), this._onClose = () => {
				y().transaction((e => {
					e.analytics.isReportOpen = !1
				}))
			}, this._onDownloadClick = () => {
				const {
					selectedCard: e
				} = this.props, t = "nFollowers" === e ? "followers" : "uFollowers" === e ? "unfollowers" : "other";
				v().gaController.sendEvent("user", `analytics:download-${t}-click`), us.downloadResultsAsZip()
			}, this._onStartDmClick = e => {
				E().iframeBus.send("dm.start-conversation", e.userId), y().transaction((e => {
					e.sidebar.isOpen = !0, e.sidebar.selectedTabId = "tab-dm"
				}))
			}, this._onLikeClick = e => {
				v().gaController.sendEvent("user", "analytics:table-like-click"), xt.onLikeClick(e)
			}, this._onActionClick = (e, t) => {
				"unfollow" === e ? this._onUnfollowClick(t) : "follow" === e && this._onFollowClick(t)
			}, this._onUnfollowClick = e => {
				(async() => {
					v().gaController.sendEvent("user", "analytics:table-unfollow-click"), this._disableAction(e.userId), await I().chromeBus.send("ig-task.unfollow-user", e), this._enableAction(e.userId)
				})()
			}, this._onFollowClick = e => {
				(async() => {
					v().gaController.sendEvent("user", "analytics:table-follow-click"), this._disableAction(e.userId), await I().chromeBus.send("ig-task.follow-user", e), this._enableAction(e.userId)
				})()
			}, this._onScanClick = () => {
				I().chromeBus.send("analytics.check-now")
			}, this.mounted = !1, this.state = {
				fetched: !1,
				nFollowers: [],
				uFollowers: [],
				analytics: {},
				isActionDisabled: {}
			}
		}
		componentDidMount() {
			this.mounted = !0, this._updateAnalytics()
		}
		componentDidUpdate(e) {
			const t = e,
				s = this.props;
			t.activeUserId !== s.activeUserId ? (this.setState({
				fetched: !1,
				nFollowers: [],
				uFollowers: [],
				analytics: {}
			}), this._updateAnalytics()) : s.lastScanOn !== t.lastScanOn && this._updateAnalytics()
		}
		componentWillUnmount() {
			this.mounted = !1
		}
		render() {
			const {
				selectedCard: e
			} = this.props;
			if ("nFollowers" !== e && "uFollowers" !== e) return Glamor.createElement(Aa, null);
			const t = "nFollowers" === e ? "New Followers" : "uFollowers" === e ? "Unfollowers" : "Analytics",
				s = "nFollowers" === e ? this.state.analytics.nFollowers : "uFollowers" === e ? this.state.analytics.uFollowers : [];
			return this._users = this._enrichUsers(s || []), Glamor.createElement("div", {
				css: fs.root,
				key: e
			}, Glamor.createElement(S().default.AnalyticsBodyPanel, {
				title: t,
				loadingOverlay: Glamor.createElement(ps, null),
				filter: this._constructFilter(),
				onDownloadClick: this._onDownloadClick,
				upsellOverlay: this._constructUpsellOverlay(),
				onClose: this._onClose
			}, this._constructContent()))
		}
		_constructFilter() {
			const e = this._users.filter((e => e.allFiltersOk));
			return Glamor.createElement(hs, {
				accountsFilteredCount: e.length,
				accountsTotalCount: this._users.length
			})
		}
		_constructUpsellOverlay() {
			const e = !this.props.hasAnalyticsFeature && this.props.lastScanOn && this.state.fetched && this._users.length > 0;
			return Glamor.createElement(Bs, {
				show: e,
				feature: "analytics"
			})
		}
		_constructContent() {
			const e = null !== this.props.runningJobId;
			return this.props.lastScanOn ? this.state.fetched ? 0 === this._users.length ? Glamor.createElement(S().default.AnalyticsStatusSection, {
				title: "nFollowers" === this.props.selectedCard ? "No New Followers Found" : "No Unfollowers Found",
				text: "nFollowers" === this.props.selectedCard ? "Inssist scans Instagram account periodically to detect new followers. It has not found new followers yet." : "Inssist scans Instagram account periodically to detect unfollowers. It has not found unfollowers yet.",
				onScanClick: !e && this._onScanClick
			}) : (this.props.isReportOpen && this._users.length > 0 && setTimeout((() => {
				this._updateTrialUsage()
			})), Glamor.createElement(S().default.AnalyticsTimelineSection, {
				users: this._users.filter((e => e.allFiltersOk)),
				onActionClick: this._onActionClick,
				onStartDmClick: this._onStartDmClick,
				precision: "days",
				userCount: "nFollowers" === this.props.selectedCard ? "positive" : "uFollowers" === this.props.selectedCard ? "negative" : null,
				noDataMessage: Glamor.createElement(gs, null),
				tablesSkipResetKey: JSON.stringify(this.props.filters),
				renderLikesCell: this._renderLikesCell
			})) : null : Glamor.createElement(S().default.AnalyticsStatusSection, {
				title: "Not Scanned",
				text: "No scan has been performed yet. Inssist has to scan followers at least twice to calculate new followers and followings. Please check back later or run the scan now.",
				onScanClick: !e && this._onScanClick
			})
		}
		_updateTrialUsage() {
			const e = this.props.selectedCard,
				t = this._users[0].userId;
			y().transaction((s => {
				"nFollowers" === e && s.analytics.trialLastViewedFollowerId !== t ? (s.analytics.trialLastViewedFollowerId = t, T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:analytics", "followers"), s.billing.trial.analytics += 1) : "uFollowers" === e && s.analytics.trialLastViewedUnfollowerId !== t && (s.analytics.trialLastViewedUnfollowerId = t, T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:analytics", "unfollowers"), s.billing.trial.analytics += 1)
			}))
		}
		async _updateAnalytics() {
			const {
				lastScanOn: e
			} = this.props;
			if (!this.mounted) return;
			const t = await xt.fetchAnalytics(e);
			this.mounted && (this.state.fetched && e !== this.props.lastScanOn || this.setState({
				fetched: !0,
				analytics: t
			}))
		}
		_disableAction(e) {
			const t = { ...this.state.isActionDisabled
			};
			t[e] = !0, this.setState({
				isActionDisabled: t
			})
		}
		_enableAction(e) {
			const t = { ...this.state.isActionDisabled
			};
			delete t[e], this.setState({
				isActionDisabled: t
			})
		}
		_enrichUsers(e) {
			const t = this.props.filters.query.trim().toLowerCase(),
				s = this.props.filters.accounts,
				a = this.state.analytics;
			return e.map((e => {
				const n = e.userId === this.props.activeUserId,
					o = xt.isFollower(e, a),
					i = xt.isFollowing(e, a);
				let r = !0;
				t && (r = e.fullName.toLowerCase().includes(t) || e.username.toLowerCase().includes(t) || e.userId.includes(t));
				const l = "all" === s || ("followers" === s ? o : "followings" === s ? i : "unilateral-followers" === s ? o && !i : "unilateral-followings" === s ? i && !o : "mutual" === s && (o && i));
				return { ...e,
					isFollower: o,
					isFollowing: i,
					allFiltersOk: r && l,
					...!n && {
						action: i ? "unfollow" : "follow",
						actionDisabled: this.state.isActionDisabled[e.userId]
					}
				}
			}))
		}
	}
	var ws = y().influx(((e, t) => ({
		filters: e.analytics.filters,
		activeUserId: e.authStatus.userId,
		lastScanOn: e.analytics.lastScanOn,
		runningJobId: e.analytics.runningJobId,
		selectedCard: e.analytics.selectedCard,
		hasAnalyticsFeature: T().stateProxy.hasPro({
			feature: "analytics"
		}),
		isReportOpen: e.analytics.isReportOpen,
		followStatus: e.igTask.followStatus
	})))(Cs);
	const vs = {
		root: { ...k().default.padding("g1h 0 0 0"),
			borderTop: k().default.border.dark
		},
		title: { ...k().default.text.bleakSemiBold,
			maxWidth: 120,
			minWidth: 120,
			opacity: .6,
			textAlign: "right",
			marginRight: k().default.space.g5
		},
		stat: {
			minWidth: 100,
			paddingRight: k().default.space.g3
		},
		statValue: {
			fontFamily: "var(--font-primary)",
			fontSize: "24px",
			fontWeight: "500",
			lineHeight: "28px",
			color: "var(--color-primary)"
		},
		statLabel: { ...k().default.text.bleak,
			fontSize: 10
		}
	};
	class ks extends k().default.Component {
		constructor(e) {
			super(e), this.mounted = !1, this.state = {
				analytics: null
			}
		}
		componentDidMount() {
			this.mounted = !0, this._updateAnalytics()
		}
		componentDidUpdate(e) {
			e.userId === this.props.userId && e.lastScanOn === this.props.lastScanOn || this._updateAnalytics()
		}
		componentWillUnmount() {
			this.mounted = !1
		}
		_updateAnalytics() {
			this.mounted && xt.fetchAnalytics(this.props.lastScanOn).then((e => {
				var t, s;
				this.mounted && this.setState({
					nFollowers: (null == e || null === (t = e.nFollowers) || void 0 === t ? void 0 : t.length) || 0,
					uFollowers: (null == e || null === (s = e.uFollowers) || void 0 === s ? void 0 : s.length) || 0
				})
			}))
		}
		render() {
			const {
				dmsSent: e,
				postsPublished: t,
				postsScheduled: s,
				insightsCreated: a
			} = this.props, {
				nFollowers: n,
				uFollowers: o
			} = this.state;
			return e || t || s || a || n || o ? Glamor.createElement("div", {
				css: [k().default.row, vs.root]
			}, Glamor.createElement("div", {
				css: vs.title
			}, "Some of your stats with Inssist:"), this._renderStat("DMs Sent", e), this._renderStat("Published Posts", t), this._renderStat("Scheduled Posts", s), this._renderStat("Insights", a), this._renderStat("Unfollowers", o), this._renderStat("New Followers", n, "+")) : null
		}
		_renderStat(e, t, s) {
			return t ? Glamor.createElement("div", {
				css: [k().default.column, vs.stat]
			}, Glamor.createElement("div", {
				css: vs.statValue
			}, s, t || 0), Glamor.createElement("div", {
				css: vs.statLabel
			}, e)) : null
		}
	}
	var Ss = y().influx((e => ({
		hasPro: T().stateProxy.hasPro(),
		userId: e.authStatus.userId,
		lastScanOn: e.analytics.lastScanOn,
		dmsSent: e.billing.trial.dmsSent,
		postsPublished: e.billing.trial.postsPublished + e.billing.trial.storiesPublished,
		insightsCreated: e.billing.trial.insights || 0,
		postsScheduled: e.billing.trial.schedule || 0
	})))(ks);
	k(), S(), V(), y(), F(), T(), v();
	const bs = {
		fsamLink: k().default.clickable
	};
	class ys extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onLoginClick = () => {
				y().transaction((e => {
					e.billing.snapshot.panel = "form", e.billing.snapshot.accountScreen = "login"
				}))
			}, this._onLogoutClick = () => {
				y().transaction((e => {
					e.billing.snapshot.panel = "form", e.billing.snapshot.accountScreen = "logout-confirm", e.billing.account.email = null, e.billing.account.token = null
				}))
			}, this._onManageAccountClick = () => {
				y().transaction((e => {
					e.billing.snapshot.panel = "form", e.billing.snapshot.accountScreen = "manage-account"
				}))
			}, this._onFsamClick = () => {
				vt.onFsamClick()
			}, this._onActionClick = e => {
				v().gaController.sendEvent("user", `billing:storefront-plan-click-${e.id}`), chrome.runtime.sendMessage({
					name: "update-fspring-status"
				}, (() => {
					setTimeout((() => {
						var t, s;
						const {
							billing: a
						} = this.props;
						"active" === (null === (t = a.subscriptions) || void 0 === t || null === (s = t["inssist-pro-monthly"]) || void 0 === s ? void 0 : s.state) || T().stateProxy.hasProPaid("inssist-pro-infinite") || "inssist-pro-lifetime" === e.id && T().stateProxy.hasProPaid("inssist-pro-lifetime") || this._kickOffPurchaseFlow(e)
					}), 250)
				}))
			}, this._kickOffPurchaseFlow = e => {
				var t;
				(null === (t = this.props.billing.purchasingPlan) || void 0 === t ? void 0 : t.id) === e.id ? y().transaction((e => {
					e.billing.purchasingPlan = null
				})) : T().stateProxy.isLoggedIn() ? y().transaction((t => {
					t.billing.purchasingPlan = {
						id: e.id
					}
				})) : y().transaction((t => {
					t.billing.selectedPlan = {
						id: e.id,
						type: e.type,
						icon: e.icon,
						label: e.label,
						period: e.period,
						price: e.price
					}, t.billing.snapshot.panel = "form", t.billing.snapshot.accountScreen = "auth"
				}))
			}, t
		}
		componentDidMount() {
			this._updatePricing()
		}
		async _updatePricing() {
			vt.updatePricing()
		}
		render() {
			const e = this._constructPlans(),
				t = this._constructStatus(),
				s = this._constructCurrencies(),
				a = this._constructActions();
			return Glamor.createElement(S().default.BillingStorefront, {
				title: this._renderTitle(),
				plans: e,
				status: t,
				currencies: s,
				onCurrencyChange: this._onCurrencyChange,
				actions: a
			})
		}
		_renderTitle() {
			return Glamor.createElement("div", null, "Unlock all features with a ", Glamor.createElement("strong", null, "PRO Subscription"), " or a ", Glamor.createElement("strong", null, "Life-Time Deal"), ":")
		}
		_constructStatus() {
			const {
				hasProPromocode: e,
				hasProPaid: t,
				authStatus: s
			} = this.props;
			if (e) {
				return {
					title: "PROMOCODE ACTIVATED",
					text: `A promocode has been activated for your Instagram @${(null==s?void 0:s.username)||"account"} 🤗. Enjoy all features!`,
					ok: !0
				}
			}
			return t ? {
				title: "PRO ACTIVATED 🏅",
				text: Glamor.createElement("div", null, "All advanced features unlocked! You can view your orders history at ", Glamor.createElement("a", {
					style: bs.fsamLink,
					onClick: this._onFsamClick
				}, "FastSpring Account Management"), " site."),
				ok: !0
			} : null
		}
		_constructCurrencies() {
			const {
				billing: e
			} = this.props;
			if (!e.countryIso || "US" === e.countryIso) return null;
			const t = "default" === e.snapshot.checkoutCurrency ? "US" : this.props.billing.countryIso;
			return "US" !== t && "USD" === this._getPriceCurrency("inssist-pro-monthly", t) && this._getPriceValue("inssist-pro-monthly", t) > this._getPriceValue("inssist-pro-monthly", "US") && "USD" === this._getPriceCurrency("inssist-pro-lifetime", t) && this._getPriceValue("inssist-pro-lifetime", t) > this._getPriceValue("inssist-pro-lifetime", "US") && "USD" === this._getPriceCurrency("inssist-pro-infinite", t) && this._getPriceValue("inssist-pro-infinite", t) > this._getPriceValue("inssist-pro-infinite", "US") ? null : [{
				label: "Pay in: Local Currency",
				selected: "US" !== t,
				checkoutCurrency: "local",
				countryIso: t
			}, {
				label: "Pay in: USD",
				selected: "US" === t,
				checkoutCurrency: "default",
				countryIso: t
			}]
		}
		_onCurrencyChange(e) {
			y().transaction((t => {
				t.billing.snapshot.checkoutCurrency = e.checkoutCurrency
			}))
		}
		_constructActions() {
			const {
				billing: e
			} = this.props;
			return e.account.token ? {
				content: e.account.email ? Glamor.createElement("div", null, "You are signed in to ", Glamor.createElement("strong", null, e.account.email)) : Glamor.createElement("div", null, "You are signed in to Inssist"),
				buttons: [{
					label: "MANAGE ACCOUNT",
					onClick: this._onManageAccountClick
				}, {
					label: "LOGOUT",
					onClick: this._onLogoutClick
				}]
			} : {
				content: Glamor.createElement("div", null, "Already have an account?"),
				buttons: [{
					label: "RECOVER MY PURCHASES",
					onClick: this._onLoginClick
				}]
			}
		}
		_getPrice(e, t) {
			const {
				billing: s
			} = this.props;
			if (!e) return null;
			t || (t = "US");
			const a = (e, t, s) => e && e[t] ? e[t][s] ? { ...e[t][s],
				countryIso: s
			} : e[t].pricing && e[t].pricing[s] ? { ...e[t].pricing[s],
				countryIso: s
			} : null : null;
			return (e => {
				if (!e) return null;
				const t = V().utils.getIntegralNumberPart(e.price),
					s = 100 * V().utils.getFractalNumberPart(e.price);
				return {
					currency: e.currency,
					integral: t,
					fractal: 0 === s ? null : s
				}
			})(a(s.pricing, e, t) || a(s.pricing, e, "US") || a(F().env.options.billingPlans, e, t) || a(F().env.options.billingPlans, e, "US"))
		}
		_getPriceValue(e, t) {
			const s = this._getPrice(e, t);
			return s ? s.integral + s.fractal / 100 : null
		}
		_getPriceCurrency(e, t) {
			const s = this._getPrice(e, t);
			return s ? s.currency : null
		}
		_constructPlans() {
			var e, t, s, a, n;
			const {
				billing: o,
				authStatus: i
			} = this.props, r = "default" === o.snapshot.checkoutCurrency ? "US" : o.countryIso, l = T().stateProxy.hasProPaid("inssist-pro-monthly"), c = T().stateProxy.hasProPaid("inssist-pro-lifetime"), u = T().stateProxy.hasProPaid("inssist-pro-infinite"), d = "active" === (null === (e = o.subscriptions) || void 0 === e || null === (t = e["inssist-pro-monthly"]) || void 0 === t ? void 0 : t.state), h = i.username;
			return [{
				id: "inssist-pro-monthly",
				type: "subscription",
				price: this._getPrice("inssist-pro-monthly", r),
				period: "/ month",
				isSelected: l,
				icon: {
					name: "billing-storefront.sub"
				},
				label: "MONTHLY",
				benefits: ["Best for Casual use", "Unlimited accounts", Glamor.createElement("span", null, "Paid monthly,", Glamor.createElement("br", null), "cancel any time")],
				action: {
					label: l ? d ? "SUBSCRIBED" : "CANCELED" : "SUBSCRIBE",
					disabled: l || u,
					progress: "inssist-pro-monthly" === (null === (s = o.purchasingPlan) || void 0 === s ? void 0 : s.id),
					tooltip: l && !d ? "You have canceled PRO subscription and it will deactivate automatically at the end of the billing period." : null,
					onClick: this._onActionClick
				},
				disabled: !1
			}, {
				id: "inssist-pro-lifetime",
				type: "product-username",
				price: this._getPrice("inssist-pro-lifetime", r),
				period: "/ account",
				badge: "POPULAR",
				isSelected: c,
				icon: {
					name: "billing-storefront.person"
				},
				label: "LIFETIME",
				benefits: ["Best for Creatives", Glamor.createElement("div", {
					css: k().default.relative()
				}, Glamor.createElement("div", {
					css: k().default.absolute("1 . . -20")
				}, Glamor.createElement(k().default.InfoCircle, {
					mini: !0,
					tooltip: {
						text: ["Lifetime plan is purchased once per Instagram account and will remain active for that Instagram account. It can not be transferred or reused with a different Instagram account.", "If you rename your Instagram account, the Lifetime plan will switch to the new name automatically.", "If you'd like to activate Lifetime plan for a different Instagram account, please switch over to that account in Inssist or Instagram before making the purchase."]
					}
				})), "Activates for this account, ", Glamor.createElement("strong", null, "@", h)), "Paid once"],
				action: {
					label: c ? "PURCHASED" : "PURCHASE",
					disabled: c || u || d,
					progress: "inssist-pro-lifetime" === (null === (a = o.purchasingPlan) || void 0 === a ? void 0 : a.id),
					tooltip: d ? "Please cancel MONTHLY subscription before purchasing LIFETIME plan." : null,
					onClick: this._onActionClick
				},
				disabled: !h
			}, {
				id: "inssist-pro-infinite",
				type: "product",
				price: this._getPrice("inssist-pro-infinite", r),
				isUnlimited: !0,
				isSelected: u,
				icon: {
					name: "billing-storefront.ouroboros"
				},
				label: "INFINITE",
				benefits: ["Best for Businesses", "Unlimited accounts", "Paid once"],
				action: {
					label: u ? "PURCHASED" : "PURCHASE",
					disabled: u || d,
					progress: "inssist-pro-infinite" === (null === (n = o.purchasingPlan) || void 0 === n ? void 0 : n.id),
					tooltip: d ? "Please cancel MONTHLY subscription before purchasing INFINITE plan." : null,
					onClick: this._onActionClick
				},
				disabled: !1
			}]
		}
	}
	var _s = y().influx((e => ({
		billing: e.billing,
		authStatus: e.authStatus,
		hasProPromocode: T().stateProxy.hasProPromocode(),
		hasProPaid: T().stateProxy.hasProPaid()
	})))(ys);
	k(), S(), F();
	class Es extends k().default.Component {
		render() {
			const e = F().env.options.billingProFeaturesTable;
			return Glamor.createElement(S().default.BillingFeaturesTable, {
				features: e
			})
		}
	}
	k(), y(), F(), T();
	const Ts = {
		permissionPrompt: { ...k().default.absolute("0 0 0 0 1"),
			...k().default.column,
			...k().default.justifyContent.center,
			...k().default.alignItems.center,
			...k().default.noDataGradient
		},
		permissionText: { ...k().default.column,
			...k().default.alignItems.center,
			maxWidth: 300,
			backgroundColor: k().default.color.bgLight1,
			border: k().default.border.light,
			textAlign: "center",
			...k().default.borderRadius.r4,
			...k().default.padding("g3 g3 g3 g3")
		},
		fsForm: e => ({ ...k().default.absolute("0 0 0 0 1"),
			...k().default.transition.slow,
			opacity: e.loaded ? 1 : 0
		})
	};
	k().default.SvgIcon.registerSvgIcons(['<symbol id="billing-checkout-mediator.unlock" viewBox="0 0 50 49.999"><g transform="translate(0 -0.003)"><g transform="translate(41.21 11.82)"><path d="M430.624,121.817a1.468,1.468,0,0,0-1.966-.655l-5.859,2.93a1.465,1.465,0,1,0,1.31,2.621l5.859-2.93A1.465,1.465,0,0,0,430.624,121.817Z" transform="translate(-421.989 -121.008)" fill="#74be86"/></g><g transform="translate(41.21 26.467)"><path d="M429.969,274.082l-5.859-2.93a1.465,1.465,0,0,0-1.31,2.621l5.859,2.93a1.465,1.465,0,0,0,1.31-2.621Z" transform="translate(-421.989 -270.997)" fill="#74be86"/></g><g transform="translate(0 0.003)"><path d="M33.691,23.636H11.719V16.116a7.324,7.324,0,0,1,14.648,0v2.93a1.464,1.464,0,0,0,1.465,1.465h5.859a1.464,1.464,0,0,0,1.465-1.465v-2.93a16.113,16.113,0,1,0-32.226,0v7.789A4.388,4.388,0,0,0,0,28.03V45.608A4.4,4.4,0,0,0,4.394,50h29.3a4.4,4.4,0,0,0,4.394-4.394V28.03A4.4,4.4,0,0,0,33.691,23.636ZM20.508,38.014v4.664a1.465,1.465,0,1,1-2.93,0V38.014a4.394,4.394,0,1,1,2.93,0Z" transform="translate(0 -0.003)" fill="#74be86"/></g><g transform="translate(41.21 20.608)"><path d="M429.319,211H423.46a1.465,1.465,0,0,0,0,2.93h5.859a1.465,1.465,0,0,0,0-2.93Z" transform="translate(-421.995 -211)" fill="#74be86"/></g></g></symbol>']);
	class As extends k().default.Component {
		constructor(e) {
			super(e), this._onLoad = () => {
				this.setState({
					loaded: !0
				})
			}, this._onCloseClick = () => {
				y().transaction((e => {
					e.billing.purchasingPlan = null
				}))
			}, this.state = {
				granted: null
			}
		}
		render() {
			const {
				purchasingPlan: e
			} = this.props.billing;
			if (!e) return null;
			if (null === this.state.granted) return chrome.permissions.contains({
				origins: ["*://*.onfastspring.com/*"]
			}, (e => {
				this.setState({
					granted: e
				})
			})), null;
			if (!this.state.granted) return chrome.permissions.request({
				origins: ["*://*.onfastspring.com/*"]
			}, (e => {
				e ? this.setState({
					granted: !0
				}) : y().transaction((e => {
					e.billing.purchasingPlan = null
				}))
			})), Glamor.createElement("div", {
				css: Ts.permissionPrompt
			}, Glamor.createElement("div", {
				css: Ts.permissionText
			}, Glamor.createElement(k().default.SvgIcon, {
				name: "billing-checkout-mediator.unlock"
			}), Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), Glamor.createElement("div", null, "INSSIST needs your permission to launch a secure billing form powered by FastSpring.")));
			const t = F().env.options.checkoutContainer,
				s = this._fetchFSpringConfig(),
				a = `${t}/fspring/${this.state.theme}?config=${s}`;
			Glamor.createElement("iframe", {
				src: a,
				style: {
					border: "none",
					width: "100%",
					height: "100%"
				},
				className: "dnd-immune",
				onLoad: this._onLoad
			});
			return Glamor.createElement("div", {
				css: Ts.fsForm(this.state)
			}, Glamor.createElement("iframe", {
				src: a,
				style: {
					border: "none",
					width: "100%",
					height: "100%"
				},
				className: "dnd-immune",
				onLoad: this._onLoad
			}), Glamor.createElement(k().default.CloseButton, {
				style: k().default.absolute("8 8 . . 2"),
				onClick: this._onCloseClick
			}))
		}
		_fetchFSpringConfig() {
			const e = this.props.billing,
				t = {
					plan: e.purchasingPlan.id,
					email: e.account.email,
					token: e.account.token,
					hasPro: T().stateProxy.hasPro(),
					countryIso: "default" === e.snapshot.checkoutCurrency ? "US" : e.countryIso,
					storefront: F().env.options.storefront,
					apiUrl: F().env.options.apiUrl
				};
			return "inssist-pro-lifetime" === t.plan && (t.tags = {
				accounts: [{
					id: y().model.state.authStatus.userId,
					name: y().model.state.authStatus.username
				}]
			}), btoa(JSON.stringify(t))
		}
	}
	var Ps = y().influx((e => ({
		billing: e.billing
	})))(k().default.theme.ThemeAware(As));
	class Is extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCloseClick = () => {
				y().transaction((e => {
					e.sidebar.isOpen = !1
				}))
			}, this._onBillingFaqClick = () => {
				v().gaController.sendEvent("user", "billing:faq-click"), chrome.tabs.create({
					url: b().common.faqBillingUrl
				})
			}, t
		}
		componentDidMount() {
			Date.now() - Is.lastUpdateTs < 5e3 || (Is.lastUpdateTs = Date.now(), setTimeout((() => chrome.runtime.sendMessage({
				name: "update-pro"
			})), 250))
		}
		render() {
			const {
				billing: e
			} = this.props;
			return Glamor.createElement(S().default.BillingPanel, {
				form: this._getFormSection(),
				storefront: this._getStorefrontSection(),
				checkout: this._getCheckoutSection(),
				clients: this._getClientsSection(),
				stats: this._getStatsSection(),
				features: this._getFeaturesSection(),
				onCloseClick: this._onCloseClick,
				onBillingFaqClick: this._onBillingFaqClick,
				loading: e.snapshot.loading
			})
		}
		_getFormSection() {
			const {
				panel: e,
				accountScreen: t
			} = this.props.billing.snapshot;
			return "form" === e && t ? Glamor.createElement(Pt, null) : null
		}
		_getStorefrontSection() {
			const {
				panel: e,
				accountScreen: t
			} = this.props.billing.snapshot;
			return "form" === e && t ? null : Glamor.createElement(_s, null)
		}
		_getCheckoutSection() {
			const {
				purchasingPlan: e
			} = this.props.billing;
			return e ? Glamor.createElement(Ps, null) : null
		}
		_getClientsSection() {
			return Glamor.createElement(S().default.BillingClients, null)
		}
		_getStatsSection() {
			return Glamor.createElement(Ss, null)
		}
		_getFeaturesSection() {
			return Glamor.createElement(Es, null)
		}
	}
	var Fs = y().influx((e => ({
		billing: e.billing
	})))(Is);
	k(), S(), y();
	class xs extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onActionBlockCloseClick = () => {
				y().transaction((e => {
					e.billing.verificationCodeEmail = null
				}))
			}, t
		}
		render() {
			const {
				email: e
			} = this.props, t = Boolean(e), s = [Glamor.createElement("div", null, "We sent a verification code to ", Glamor.createElement("b", null, e), " from ", Glamor.createElement("b", null, "Inssist, no-reply@slashed.io")), Glamor.createElement("div", null, "Sometimes it may get into a spam folder. If a code is not delivered in 5 minutes, please double check your email address or reach out to us at ", "inssist@slashed.io")];
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "billing-code-mediator",
				show: t
			}, Glamor.createElement(k().default.InfoCard, {
				markerColor: k().default.color.positive,
				title: "Your Code is Emailed",
				content: s,
				onClose: this._onActionBlockCloseClick
			}))
		}
	}
	var Gs = y().influx((e => ({
		email: e.billing.verificationCodeEmail
	})))(xs);
	k(), S(), k(), S(), F(), b(), v(), T(), y();
	class Ds extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onFaqClick = () => {
				v().gaController.sendEvent("user", "billing:faq-click"), chrome.tabs.create({
					url: b().common.faqBillingUrl
				})
			}, t
		}
		render() {
			return Glamor.createElement(S().default.BillingPurchase, {
				expanded: this.props.expanded,
				purchased: this.props.hasProPaid,
				withShadow: this.props.withShadow,
				hidePrice: this.props.hidePrice,
				hideDisclaimer: this.props.hideDisclaimer,
				hideFaq: this.props.hideFaq,
				header: this._getHeader(),
				features: this._getFeatures(),
				footer: this._getFooter(),
				onFaqClick: this._onFaqClick
			})
		}
		_getHeader() {
			return {
				title: "Upgrade to PRO",
				description: Glamor.createElement(k().default.Fragment, null, "This feature is available in Inssist PRO Version. Upgrade to PRO to support app development 🚀")
			}
		}
		_getFeatures() {
			return F().env.options.billingProFeaturesList.map((e => ({
				id: e.id,
				icon: e.icon,
				title: e.title,
				description: e.description
			})))
		}
		_getFooter() {
			return {
				button: {
					label: "ACTIVATE PRO",
					onClick: this.props.onActivateProClick
				}
			}
		}
	}
	Ds.defaultProps = {
		expanded: !1,
		widthShadow: !0,
		hidePrice: !0,
		hideDisclaimer: !0,
		hideFaq: !0
	};
	var Os = y().influx((e => ({
		hasPro: T().stateProxy.hasPro(),
		hasProPaid: T().stateProxy.hasProPaid()
	})))(Ds);
	class Bs extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onActivateProClick = () => {
				const e = this.props.feature || null;
				vt.openBilling(e)
			}, t
		}
		render() {
			return Glamor.createElement(S().default.UpsellOverlay, {
				show: this.props.show,
				onOverlayClick: this.props.onOverlayClick,
				content: this._renderContent()
			})
		}
		_renderContent() {
			return Glamor.createElement("div", {
				css: {
					width: 400
				}
			}, Glamor.createElement(Os, {
				onActivateProClick: this._onActivateProClick
			}))
		}
	}
	class Us extends k().default.Component {
		constructor(e) {
			super(e), this._onOkClick = () => {
				vt.openBilling("insights")
			}, this.state = {
				isPublishing: !1
			}
		}
		render() {
			return Glamor.createElement(S().default.InsightsLockedCard, {
				onOkClick: this._onOkClick
			})
		}
	}
	const Ls = {
		root: { ...k().default.padding("g2 g2 100 23")
		}
	};
	class Rs extends k().default.Component {
		constructor(e) {
			super(e), this._onWarningOkClick = () => {
				v().gaController.sendEvent("user", "insights:warning-ok-click"), this.setState({
					showWarning: !1
				})
			}, this._onWarningCreateAnywayClick = () => {
				v().gaController.sendEvent("user", "insights:warning-create-anyway-click"), this.setState({
					showWarning: !1
				}), Qe.setAutoscrollToNewTask(!0), O().default.addTask()
			}, this._onAddTaskClick = () => {
				v().gaController.sendEvent("user", "insights:add-task-click");
				const e = y().model.state;
				e.insights.lastTaskTimestamps.length >= 3 && Date.now() - e.insights.lastTaskTimestamps[0] < 24 * P().HOUR ? this.setState({
					showWarning: !0
				}) : (Qe.setAutoscrollToNewTask(!0), O().default.addTask())
			}, this.state = {
				showWarning: !1
			}
		}
		render() {
			return Glamor.createElement("div", {
				css: Ls.root
			}, Glamor.createElement("div", {
				css: k().default.text.contentTitle
			}, "Account Insights"), Glamor.createElement(k().default.Spacer, {
				height: "g1"
			}), Glamor.createElement("div", {
				css: k().default.text.bleak
			}, "Collect info on any public Instagram account: posting hours, hashtags, post performance, followers and followings lists."), Glamor.createElement(k().default.Spacer, {
				height: "g2"
			}), Glamor.createElement("div", {
				css: Ls.content
			}, this._renderAddTaskButton(), this._renderLockedCard(), this._renderWarningCard(), Glamor.createElement(k().default.Spacer, {
				height: "g2"
			}), this._renderTasks()))
		}
		_renderTasks() {
			return Glamor.createElement(k().default.Fragment, null, this.props.tasks.map((e => Glamor.createElement(wt, {
				key: e.id,
				task: e
			}))))
		}
		_renderAddTaskButton() {
			return this.props.canAddTask ? this.state.showWarning ? null : Glamor.createElement(S().default.InsightsAddTaskButton, {
				onClick: this._onAddTaskClick
			}) : null
		}
		_renderLockedCard() {
			return this.props.canAddTask ? null : Glamor.createElement(Us, null)
		}
		_renderWarningCard() {
			return this.props.canAddTask && this.state.showWarning ? Glamor.createElement(S().default.InsightsWarningCard, {
				title: "Warning: Too Many Insights",
				description: ["It is a friendly recommendation to run 3 or less Insights with Inssist per day.", "Running more than 3 insights abuses Instagram APIs and might make Instagram action block your account or IP address from running more Insights in the future."],
				buttons: [{
					id: "cancel",
					label: "OK, CANCEL",
					onClick: this._onWarningOkClick
				}, {
					id: "create-insights",
					label: "CREATE INSIGHT ANYWAY",
					cancel: !0,
					onClick: this._onWarningCreateAnywayClick
				}]
			}) : null
		}
	}
	var Ms = y().influx((e => ({
		tasks: [...e.insights.tasks].reverse(),
		canAddTask: T().stateProxy.insights.canAddTask()
	})))(Rs);

	function Ns(e) {
		const t = W().default();
		return (e.match(t) || []).map((e => e.toLowerCase())).filter($().default)
	}
	k(), S(), y(), I(), v(), P(), H(), V(), y(), I(), L(), $(), W();
	var zs = {
		downloadResultsAsZip: async function (e) {
			const t = await ls(),
				s = await I().chromeBus.send("insights.get-task-results.users", e.id),
				a = await I().chromeBus.send("insights.get-task-results.posts", e.id),
				n = await xt.fetchAnalytics(y().model.state.analytics.lastScanOn),
				o = L().default(e.resultCounters.userCredibility),
				i = e.user.lastPosts[0],
				r = JSON.stringify({
					userId: e.user.userId,
					username: e.user.username,
					fullName: e.user.fullName,
					bio: e.user.bio,
					isFresh: e.user.isFresh,
					isPrivate: e.user.isPrivate,
					isVerified: e.user.isVerified,
					isBusiness: e.user.isBusiness,
					isFollowingViewer: xt.isFollower(e.user, n),
					isFollowedByViewer: xt.isFollowing(e.user, n),
					postsCount: e.user.postsCount,
					followersCount: e.user.followersCount,
					followingsCount: e.user.followingsCount,
					lastPostTimestamp: i ? i.ts : null,
					avatarUrl: e.user.avatarUrl
				}, null, 2),
				l = cs([e.user], [
					["user id", e => e.userId],
					["username", e => e.username],
					["name", e => e.fullName],
					["bio", e => e.bio],
					["is fresh", e => e.isFresh ? "YES" : "—"],
					["is private", e => e.isPrivate ? "YES" : "—"],
					["is verified", e => e.isVerified ? "YES" : "—"],
					["is business", e => e.isBusiness ? "YES" : "—"],
					["follows viewer", e => xt.isFollower(e, n) ? "YES" : "—"],
					["is followed by viewer", e => xt.isFollowing(e, n) ? "YES" : "—"],
					["posts count", e => e.postsCount],
					["followers count", e => e.followersCount],
					["followings count", e => e.followingsCount],
					["last posted", () => {
						if (!i) return "—";
						return new Date(i.ts).toString().split(" ").slice(1, 4).join(" ")
					}],
					["avatar url", e => e.avatarUrl],
					["spam followers", () => null === e.resultCounters.spamFollowersPercent ? "—" : `${Math.round(100*e.resultCounters.spamFollowersPercent)}%`],
					["spam followings", () => null === e.resultCounters.spamFollowingsPercent ? "—" : `${Math.round(100*e.resultCounters.spamFollowingsPercent)}%`],
					["credibility", () => `${o.value}, ${o.label}`]
				]),
				c = JSON.stringify(s.followers, null, 2),
				u = s.followers.map((e => e.username)).join(","),
				d = cs(s.followers, [
					["username", e => e.username],
					["user id", e => e.userId],
					["name", e => e.fullName],
					["is private", e => e.isPrivate ? "PRIVATE" : ""],
					["is verified", e => e.isVerified ? "VERIFIED" : ""],
					["follows viewer", e => xt.isFollower(e, n) ? "YES" : ""],
					["is followed by viewer", e => xt.isFollowing(e, n) ? "YES" : ""],
					["profile url", e => `https://instagram.com/${e.username}`],
					["avatar url", e => e.avatarUrl]
				]),
				h = JSON.stringify(s.followings, null, 2),
				m = s.followings.map((e => e.username)).join(","),
				p = cs(s.followings, [
					["username", e => e.username],
					["user id", e => e.userId],
					["name", e => e.fullName],
					["is private", e => e.isPrivate ? "PRIVATE" : ""],
					["is verified", e => e.isVerified ? "VERIFIED" : ""],
					["follows viewer", e => xt.isFollower(e, n) ? "YES" : ""],
					["is followed by viewer", e => xt.isFollowing(e, n) ? "YES" : ""],
					["profile url", e => `https://instagram.com/${e.username}`],
					["avatar url", e => e.avatarUrl]
				]),
				g = a.posts.map((e => ({
					on: e.on,
					code: e.code,
					type: e.type,
					likes: e.stats.likes,
					comments: e.stats.comments,
					engagement: H().default(e.stats.likes, e.stats.comments),
					caption: e.caption || "",
					preview: e.img,
					image: e.imgx
				}))),
				f = JSON.stringify(g, null, 2),
				C = cs(g, [
					["url", e => `https://www.instagram.com/p/${e.code}`],
					["posted on", e => new Date(e.on).toString().split(" ").slice(1, 5).join(" ").slice(0, -3)],
					["type", e => e.type],
					["likes", e => e.likes],
					["comments", e => e.comments],
					["engagement (likes + 10 * comments)", e => e.engagement],
					["caption", e => e.caption],
					["preview", e => e.preview],
					["image", e => e.image]
				]),
				w = [];
			g.forEach((e => {
				Ns(e.caption).forEach((t => {
					let s = w.find((e => e.hashtag === t));
					s || (s = {
						hashtag: t,
						likes: 0,
						comments: 0,
						engagement: 0,
						posts: 0
					}, w.push(s)), s.likes += e.likes, s.comments += e.comments, s.posts += 1
				}))
			})), w.forEach((e => {
				e.engagement = Math.round((e.likes + 10 * e.comments) / e.posts)
			})), w.sort(((e, t) => t.posts - e.posts));
			const v = JSON.stringify(w, null, 2),
				k = cs(w, [
					["hashtag", e => e.hashtag],
					["posts", e => e.posts],
					["likes", e => e.likes],
					["comments", e => e.comments],
					["engagement (likes + 10 * comments) / posts", e => e.engagement]
				]),
				S = e.user.username,
				b = new t;
			b.file(`${S}-overview.csv`, l), b.file(`${S}-overview.json`, r), s.followers.length > 0 && (b.file(`${S}-followers.csv`, d), b.file(`${S}-followers.json`, c), b.file(`${S}-followers.txt`, u));
			s.followings.length > 0 && (b.file(`${S}-followings.csv`, p), b.file(`${S}-followings.json`, h), b.file(`${S}-followings.txt`, m));
			g.length > 0 && (b.file(`${S}-posts.csv`, C), b.file(`${S}-posts.json`, f), b.file(`${S}-hashtags.csv`, k), b.file(`${S}-hashtags.json`, v));
			const _ = `${S}-insights.zip`,
				E = await b.generateAsync({
					type: "blob"
				});
			V().utils.saveFile(_, E), Fe.showRateUs(), Ue.showFollowUs()
		}
	};
	d(), k(), S(), v(), y();
	class Vs extends k().default.Component {
		constructor(e) {
			super(e), this._onInput = e => {
				v().gaController.sendEvent("user", "insights:filter-query"), this._updateFilters({
					key: "query",
					value: e,
					delay: 300
				})
			}, this._onClearClick = () => {
				this._updateFilters({
					key: "query",
					value: ""
				})
			}, this._onItemClick = (e, t) => {
				v().gaController.sendEvent("user", `insights:filter-${e}-${t}`), this._updateFilters({
					key: e,
					value: t
				})
			}, this._applyFiltersTimeout = null, this.state = { ...e.filters
			}
		}
		componentDidUpdate(e) {
			const t = e,
				s = this.props;
			(!t.isReportOpen && s.isReportOpen || t.selectedCard !== s.selectedCard && s.isReportOpen) && clearTimeout(this._applyQueryTimeout), d().default.isEqual(t.filters, s.filters) || this.setState({ ...s.filters
			})
		}
		render() {
			return Glamor.createElement(S().default.Filter, {
				query: this.state.query,
				placeholder: "Filter report",
				highlightTriangle: "all" !== this.state.accounts || "all" !== this.state.posts,
				sections: [{
					id: "accounts",
					title: "filter accounts",
					details: this.props.accountsTotalCount ? `${this.props.accountsFilteredCount} of ${this.props.accountsTotalCount}` : "",
					value: this.state.accounts,
					items: [{
						label: "All accounts",
						value: "all"
					}, {
						label: "My followers",
						value: "followers"
					}, {
						label: "My followings",
						value: "followings"
					}, {
						label: "Unilateral followers",
						value: "unilateral-followers"
					}, {
						label: "Unilateral followings",
						value: "unilateral-followings"
					}, {
						label: "Mutual followers",
						value: "mutual"
					}]
				}, {
					id: "posts",
					title: "filter posts",
					details: `${this.props.postsFilteredCount} of ${this.props.postsTotalCount}`,
					value: this.state.posts,
					items: [{
						label: "All posts",
						value: "all"
					}, {
						label: "Last 24 hours",
						value: "24h"
					}, {
						label: "Last 7 days",
						value: "7d"
					}, {
						label: "Last month",
						value: "1m"
					}, {
						label: "Last 3 months",
						value: "3m"
					}, {
						label: "Last 6 months",
						value: "6m"
					}, {
						label: "Last 12 months",
						value: "12m"
					}]
				}],
				onInput: this._onInput,
				onClearClick: this._onClearClick,
				onItemClick: this._onItemClick
			})
		}
		_updateFilters({
			key: e,
			value: t,
			delay: s = 0
		}) {
			this.setState({
				[e]: t
			}), y().transaction((e => {
				e.insights.loading = !0
			})), clearTimeout(this._applyFiltersTimeout), this._applyFiltersTimeout = setTimeout((() => {
				y().transaction((e => {
					e.insights.filters = { ...this.state
					}, e.insights.loading = !1
				}))
			}), s)
		}
	}
	var Hs = y().influx((e => ({
		isReportOpen: e.analytics.isReportOpen,
		selectedCard: e.analytics.selectedCard,
		filters: e.insights.filters
	})))(Vs);
	k(), S(), y();
	class $s extends k().default.Component {
		render() {
			return Glamor.createElement(S().default.LoadingOverlay, {
				show: this.props.show
			})
		}
	}
	var Ws = y().influx((e => ({
		show: e.insights.loading
	})))($s);
	k(), S(), y();
	const qs = {
		root: { ...k().default.padding("g3 g3 g2 g3")
		}
	};
	class js extends k().default.Component {
		render() {
			const e = this.props.task,
				t = e.user,
				s = this.props.netStats;
			let a;
			a = "completed" === e.peerScrapeStatus ? "completed" : "error" === e.peerScrapeStatus ? "error" : null;
			let n = [{
				label: "BUSINESS ACCOUNT",
				value: t.isBusiness
			}, {
				label: "FRESH",
				value: t.isFresh
			}, {
				label: "PRIVATE",
				value: t.isPrivate
			}, {
				label: "FOLLOWS ME",
				value: t.isFollowingViewer
			}, {
				label: "FOLLOWED BY ME",
				value: t.isFollowedByViewer
			}];
			return n.every((e => !e.value)) && (n = null), Glamor.createElement("div", {
				css: qs.root
			}, Glamor.createElement(S().default.InsightsUserInfo, {
				avatarUrl: t.avatarUrl,
				avatarBadge: a,
				progress: ut.getUsersProgress(e, s),
				progressSpinning: "running" === e.peerScrapeStatus,
				username: t.username,
				fullName: t.fullName,
				userId: t.userId,
				bio: t.bio,
				businessCategory: t.businessCategory,
				isVerified: t.isVerified,
				pills: n
			}))
		}
	}
	var Ys = y().influx((e => ({
		netStats: e.insights.netStats
	})))(js);

	function Ks(e, t) {
		j().default(2, arguments);
		var s = q().default(e),
			a = q().default(t),
			n = s.getTime() - a.getTime();
		return n < 0 ? -1 : n > 0 ? 1 : n
	}

	function Zs(e, t) {
		j().default(2, arguments);
		var s = q().default(e),
			a = q().default(t),
			n = s.getFullYear() - a.getFullYear(),
			o = s.getMonth() - a.getMonth();
		return 12 * n + o
	}

	function Js(e, t) {
		j().default(2, arguments);
		var s = q().default(e),
			a = q().default(t),
			n = Ks(s, a),
			o = Math.abs(Zs(s, a));
		s.setMonth(s.getMonth() - n * o);
		var i = Ks(s, a) === -n,
			r = n * (o - i);
		return 0 === r ? 0 : r
	}

	function Xs(e, t) {
		j().default(2, arguments);
		var s = q().default(e),
			a = q().default(t);
		return s.getTime() - a.getTime()
	}

	function Qs(e, t) {
		j().default(2, arguments);
		var s = Xs(e, t) / 1e3;
		return s > 0 ? Math.floor(s) : Math.ceil(s)
	}

	function ea(e) {
		return function (e, t) {
			if (null == e) throw new TypeError("assign requires that input parameter not be null or undefined");
			for (var s in t = t || {}) t.hasOwnProperty(s) && (e[s] = t[s]);
			return e
		}({}, e)
	}
	q(), j(), q(), q(), j(), j(), q(), j(), j(), Y(), q(), K(), j();
	var ta = 1440,
		sa = 43200;

	function aa(e, t, s) {
		j().default(2, arguments);
		var a = s || {},
			n = a.locale || Y().default;
		if (!n.formatDistance) throw new RangeError("locale must contain formatDistance property");
		var o = Ks(e, t);
		if (isNaN(o)) throw new RangeError("Invalid time value");
		var i, r, l = ea(a);
		l.addSuffix = Boolean(a.addSuffix), l.comparison = o, o > 0 ? (i = q().default(t), r = q().default(e)) : (i = q().default(e), r = q().default(t));
		var c, u = Qs(r, i),
			d = (K().default(r) - K().default(i)) / 1e3,
			h = Math.round((u - d) / 60);
		if (h < 2) return a.includeSeconds ? u < 5 ? n.formatDistance("lessThanXSeconds", 5, l) : u < 10 ? n.formatDistance("lessThanXSeconds", 10, l) : u < 20 ? n.formatDistance("lessThanXSeconds", 20, l) : u < 40 ? n.formatDistance("halfAMinute", null, l) : u < 60 ? n.formatDistance("lessThanXMinutes", 1, l) : n.formatDistance("xMinutes", 1, l) : 0 === h ? n.formatDistance("lessThanXMinutes", 1, l) : n.formatDistance("xMinutes", h, l);
		if (h < 45) return n.formatDistance("xMinutes", h, l);
		if (h < 90) return n.formatDistance("aboutXHours", 1, l);
		if (h < ta) {
			var m = Math.round(h / 60);
			return n.formatDistance("aboutXHours", m, l)
		}
		if (h < 2520) return n.formatDistance("xDays", 1, l);
		if (h < sa) {
			var p = Math.round(h / ta);
			return n.formatDistance("xDays", p, l)
		}
		if (h < 86400) return c = Math.round(h / sa), n.formatDistance("aboutXMonths", c, l);
		if ((c = Js(r, i)) < 12) {
			var g = Math.round(h / sa);
			return n.formatDistance("xMonths", g, l)
		}
		var f = c % 12,
			C = Math.floor(c / 12);
		return f < 3 ? n.formatDistance("aboutXYears", C, l) : f < 9 ? n.formatDistance("overXYears", C, l) : n.formatDistance("almostXYears", C + 1, l)
	}

	function na(e) {
		return oa.find((t => t.condition(e))).color
	}
	j(), k(), S(), y(), v(), b(), O();
	const oa = [{
		condition: e => e > .35,
		color: "#E34E21"
	}, {
		condition: e => "number" == typeof e,
		color: "#74BE86"
	}, {
		condition: () => !0,
		color: "#D8DADD"
	}];
	L();
	const ia = {
			spamFollowers: {
				text: ["Inssist scans account followers to estimate a percentage of spam and bots.", "High spam count may indicate purchased followers."]
			},
			spamFollowings: {
				text: ["Inssist scans account followings to estimate a percentage of spam and bots.", "High spam count may indicate a bot account."]
			},
			credibility: {
				text: ["High Credibility means account is likely to be a real person or business. Low Credibility may indicate a spam account.", "Inssist uses a set of rules such as followers / followings ratio, posting times, avatar and biography etc. to calculate this score."]
			}
		},
		ra = {
			root: {
				paddingLeft: 34
			}
		};
	class la extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCheckFollowersClick = () => {
				v().gaController.sendEvent("user", "insights:check-spam-followers-click"), O().default.clickCheckFollowersForSpam.dispatch(this.props.task.id)
			}, this._onCheckFollowingsClick = () => {
				v().gaController.sendEvent("user", "insights:check-spam-followings-click"), O().default.clickCheckFollowingsForSpam.dispatch(this.props.task.id)
			}, this._onCancelFollowersClick = () => {
				v().gaController.sendEvent("user", "insights:cancel-spam-followers-click"), O().default.clickCancelFollowersForSpam.dispatch(this.props.task.id)
			}, this._onCancelFollowingsClick = () => {
				v().gaController.sendEvent("user", "insights:cancel-spam-followings-click"), O().default.clickCancelFollowingsForSpam.dispatch(this.props.task.id)
			}, this._onLimitReachedOkClick = () => {
				v().gaController.sendEvent("user", "insights:spam-limit-reached-ok-click"), O().default.clickLimitReachedOk.dispatch()
			}, this._onFaqClick = () => {
				v().gaController.sendEvent("user", "insights:spam-limit-reached-faq-click"), Qe.onFaqClick()
			}, t
		}
		render() {
			const e = this.props.task.user;
			return Glamor.createElement(S().default.InsightsStatsBar, {
				style: ra.root,
				postsCount: e.postsCount,
				followersCount: e.followersCount,
				followingsCount: e.followingsCount,
				lastPostedText: this._getLastPostedText(),
				secondRow: this._getSecondRowData()
			})
		}
		_getLastPostedText() {
			const e = this.props.task.user.lastPosts[0];
			return e ? function (e, t) {
				return j().default(1, arguments), aa(e, Date.now(), t)
			}(e.ts, {
				addSuffix: !0
			}) : null
		}
		_getSecondRowData() {
			return {
				spamFollowers: this._getSpamFollowersData(),
				spamFollowings: this._getSpamFollowingsData(),
				credibility: this._getCredibilityData(),
				statusPanel: this._getLimitReachedData()
			}
		}
		_getSpamFollowersData() {
			const e = this.props.task;
			if (!Qe.canStartSpamFollowers(e)) return;
			const t = e.spamFollowersStatus,
				s = this.props.netStats;
			return {
				status: t,
				tooltip: ia.spamFollowers,
				..."planned" === t && {
					onCheckClick: this._onCheckFollowersClick
				},
				..."pending" === t && {
					onCancelClick: this._onCancelFollowersClick
				},
				..."running" === t && {
					progress: ut.getSpamFollowersProgress(e, s),
					timeLeftText: this._renderTimeLeft("followers"),
					onCancelClick: this._onCancelFollowersClick
				},
				..."completed" === t && {
					basedOn: Math.floor(e.resultCounters.spamFollowersCount / e.spamFollowersCount * e.followersCount),
					result: e.resultCounters.spamFollowersPercent,
					color: na(e.resultCounters.spamFollowersPercent)
				}
			}
		}
		_getSpamFollowingsData() {
			const e = this.props.task;
			if (!Qe.canStartSpamFollowings(e)) return;
			const t = e.spamFollowingsStatus,
				s = this.props.netStats;
			return {
				status: t,
				tooltip: ia.spamFollowings,
				..."planned" === t && {
					onCheckClick: this._onCheckFollowingsClick
				},
				..."pending" === t && {
					onCancelClick: this._onCancelFollowingsClick
				},
				..."running" === t && {
					progress: ut.getSpamFollowingsProgress(e, s),
					timeLeftText: this._renderTimeLeft("followings"),
					onCancelClick: this._onCancelFollowingsClick
				},
				..."completed" === t && {
					basedOn: Math.floor(e.resultCounters.spamFollowingsCount / e.spamFollowingsCount * e.followingsCount),
					result: e.resultCounters.spamFollowingsPercent,
					color: na(e.resultCounters.spamFollowingsPercent)
				}
			}
		}
		_getCredibilityData() {
			const e = this.props.task.resultCounters.userCredibility,
				t = L().default(e);
			return {
				value: t.value,
				color: t.color,
				tooltip: ia.credibility
			}
		}
		_getLimitReachedData() {
			const e = this.props.spamcheck;
			if ("ok" === e.status) return null;
			if (e.disabledStateAcknowledged) return null;
			let t;
			return "disabled-lock" === e.status ? t = Math.round(b().insights.spamLockDuration / 36e5) : "disabled-ban" === e.status && (t = Math.round(b().insights.spamBanDuration / 36e5)), {
				title: "LIMIT REACHED",
				text: Glamor.createElement(k().default.Fragment, null, "Reached an Instagram request rate limit,", Glamor.createElement("br", null), "please allow for a ", t, " hours cool-down."),
				buttons: [{
					label: "OK, GOT IT",
					onClick: this._onLimitReachedOkClick
				}, {
					label: "OPEN FAQ PAGE",
					onClick: this._onFaqClick
				}]
			}
		}
		_renderTimeLeft(e) {
			const t = this.props.task,
				s = this.props.netStats;
			let a;
			return "followers" === e ? a = ut.getSpamFollowersTimeLeft(t, s) : "followings" === e && (a = ut.getSpamFollowingsTimeLeft(t, s)), `in progress, ${Qe.renderTime(a)} left`
		}
	}
	var ca = y().influx((e => ({
		netStats: e.insights.netStats,
		spamcheck: e.insights.spamcheck
	})))(la);
	k(), S(), y(), E(), v(), k(), y();
	class ua extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onResetFiltersClick = () => {
				y().transaction((e => {
					e.insights.loading = !0
				})), setTimeout((() => {
					y().transaction((e => {
						e.insights.filters.query = "", e.insights.filters.posts = "all", e.insights.filters.accounts = "all", e.insights.loading = !1
					}))
				}), 50)
			}, t
		}
		render() {
			return Glamor.createElement(k().default.Fragment, null, "No data found", Glamor.createElement(k().default.Spacer, {
				width: "g2"
			}), Glamor.createElement(k().default.LinkButton, {
				label: "RESET FILTERS",
				onClick: this._onResetFiltersClick
			}))
		}
	}
	const da = {
			root: {},
			table: { ...k().default.padding("g3")
			},
			title: { ...k().default.text.contentTitle,
				lineHeight: "24px",
				color: k().default.color.textNormal
			}
		},
		ha = "insights-users-tables.times",
		ma = "insights-users-tables.navigate";
	k().default.SvgIcon.registerSvgIcons([`<symbol id="${ha}" viewBox="0 0 8.5 8.5"><path d="M4.229 4.983L.752 8.46l-.753-.754L3.475 4.23-.001.754.752 0l3.477 3.476L7.706 0l.753.754L4.983 4.23l3.476 3.476-.753.754z" fill="#a5aaaf"/></symbol>`, `<symbol id="${ma}" viewBox="0 0 32 32"><path fill="none" d="M0 0h32v32H0z"/><path d="M10.493 22V12h6l-2 2h-2v6h6v-2l2-2v6zm4.149-5.847L19.793 11h-3.3V9.5h6.508v6.453h-1.508V12.7l-5.151 5.152z" fill="currentColor"/></symbol>`]);
	class pa extends k().default.Component {
		constructor(e) {
			super(e), this._renderLikesCell = e => Glamor.createElement(Ft, {
				user: e,
				onLikeClick: this._onLikeClick
			}), this._onLikeClick = e => {
				v().gaController.sendEvent("user", "insights:table-like-click"), Qe.onLikeClick(e)
			}, this._onActionClick = (e, t) => {
				"unfollow" === e ? this._onUnfollowClick(t) : "follow" === e && this._onFollowClick(t)
			}, this._onStartDmClick = e => {
				E().iframeBus.send("dm.start-conversation", e.userId), y().transaction((e => {
					e.sidebar.isOpen = !0, e.sidebar.selectedTabId = "tab-dm"
				}))
			}, this._onUnfollowClick = e => {
				(async() => {
					v().gaController.sendEvent("user", "insights:table-unfollow-click"), this._disableAction(e.userId), await Qe.onUnfollowClick(e), this._enableAction(e.userId)
				})()
			}, this._onFollowClick = e => {
				(async() => {
					v().gaController.sendEvent("user", "insights:table-follow-click"), this._disableAction(e.userId), await Qe.onFollowClick(e), this._enableAction(e.userId)
				})()
			}, this.state = {
				followersSkip: 0,
				followingsSkip: 0,
				isActionDisabled: {}
			}
		}
		componentDidUpdate(e) {
			const t = e.filters,
				s = this.props.filters;
			t.query === s.query && t.accounts === s.accounts || (this.setState({
				followersSkip: 0
			}), this.setState({
				followingsSkip: 0
			}))
		}
		render() {
			return Glamor.createElement("div", {
				css: da.root
			}, this._renderUsersTable("followers"), this._renderUsersTable("followings"))
		}
		_renderUsersTable(e) {
			const t = "followers" === e ? this.props.followers : this.props.followings;
			if (0 === t.length) return null;
			const s = t.filter((e => e.allFiltersOk)),
				a = "followers" === e ? {
					title: "Followers",
					skip: this.state.followersSkip,
					onSkipChange: e => {
						this.setState({
							followersSkip: e
						})
					}
				} : {
					title: "Followings",
					skip: this.state.followingsSkip,
					onSkipChange: e => {
						this.setState({
							followingsSkip: e
						})
					}
				};
			return Glamor.createElement("div", {
				css: da.table
			}, Glamor.createElement("div", {
				css: da.title
			}, a.title), Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), Glamor.createElement(S().default.UserTable, {
				skip: a.skip,
				onSkipChange: a.onSkipChange,
				pageSize: 10,
				minRowCount: Math.min(10, t.length),
				noDataMessage: Glamor.createElement(ua, null),
				data: s.map((e => ((e = { ...e
				}).userId === this.props.activeUserId || (e.action = e.isFollowing ? "unfollow" : "follow", e.actionDisabled = this.state.isActionDisabled[e.userId]), e))),
				renderLikesCell: this._renderLikesCell,
				onActionClick: this._onActionClick,
				onStartDmClick: this._onStartDmClick
			}))
		}
		_disableAction(e) {
			const t = { ...this.state.isActionDisabled
			};
			t[e] = !0, this.setState({
				isActionDisabled: t
			})
		}
		_enableAction(e) {
			const t = { ...this.state.isActionDisabled
			};
			delete t[e], this.setState({
				isActionDisabled: t
			})
		}
	}
	var ga = y().influx((e => ({
		filters: e.insights.filters,
		followStatus: e.igTask.followStatus,
		activeUserId: e.authStatus.userId
	})))(pa);
	k(), S();
	const fa = {
		root: { ...k().default.padding("g3")
		}
	};
	class Ca extends k().default.Component {
		render() {
			const e = this.props.posts;
			return 0 === e.length ? null : Glamor.createElement("div", {
				css: fa.root
			}, Glamor.createElement(S().default.PostingTimesSection, {
				posts: e.filter((e => e.dateOk)),
				noDataMessage: Glamor.createElement(ua, null)
			}))
		}
	}
	k(), S(), H(), b(), y();
	const wa = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		va = (new Date).getFullYear(),
		ka = {
			root: { ...k().default.padding("g3")
			}
		};
	class Sa extends k().default.Component {
		constructor(e) {
			super(e), this._onSkipChange = e => {
				this.setState({
					skip: e
				})
			}, this.state = {
				skip: 0
			}
		}
		componentDidUpdate(e) {
			const t = e.filters,
				s = this.props.filters;
			t.query === s.query && t.posts === s.posts || this.setState({
				skip: 0
			})
		}
		render() {
			const e = this._getPosts();
			return 0 === e.length ? null : Glamor.createElement("div", {
				css: ka.root
			}, Glamor.createElement(S().default.PostTableSection, {
				skip: this.state.skip,
				posts: e.filter((e => e.allFiltersOk)),
				minRowCount: Math.min(e.length, 5),
				noDataMessage: Glamor.createElement(ua, null),
				onSkipChange: this._onSkipChange,
				onPostImageClick: this._onPostImageClick
			}))
		}
		_getPosts() {
			return this.props.posts.map((e => ({ ...e,
				title: this._formatDate(e.on),
				image: e.img || e.imgx,
				stats: { ...e.stats,
					engagement: H().default(e.stats.likes, e.stats.comments)
				}
			})))
		}
		_formatDate(e) {
			const t = new Date(e),
				s = t.getDate(),
				a = t.getMonth(),
				n = va !== t.getFullYear() ? ` ${t.getFullYear()}` : "";
			let o = t.getHours();
			o < 10 && (o = "0" + o);
			let i = t.getMinutes();
			return i < 10 && (i = "0" + i), `${s} ${wa[a]}${n}, ${o}:${i}`
		}
		_onPostImageClick(e) {
			if (e.code) {
				const t = b().ig.post.url(e.code);
				window.electron ? window.open(t, `post-details.${e.code}`) : chrome.tabs.create({
					url: t
				})
			}
		}
	}
	var ba = y().influx((e => ({
		filters: e.insights.filters
	})))(Sa);
	k(), S(), b(), y();
	const ya = {
		root: { ...k().default.padding("g3")
		}
	};
	class _a extends k().default.Component {
		constructor(e) {
			super(e), this._onSkipChange = e => {
				this.setState({
					skip: e
				})
			}, this._onHashtagClick = e => {
				if (e.name) {
					const t = e.name.startsWith("#") ? e.name.substr(1) : e.name,
						s = b().ig.hashtag.url(t);
					window.electron ? window.open(s, `hashtag-details.${t}`) : chrome.tabs.create({
						url: s,
						active: !0
					})
				}
			}, this.state = {
				skip: 0
			}
		}
		componentDidUpdate(e) {
			const t = e.filters,
				s = this.props.filters;
			t.query === s.query && t.posts === s.posts || this.setState({
				skip: 0
			})
		}
		render() {
			const e = this.props.posts || [];
			if (0 === e.length) return null;
			const t = this._getHashtags(e),
				s = e.filter((e => e.dateOk)),
				a = this._getHashtags(s, !0);
			return Glamor.createElement("div", {
				css: ya.root
			}, Glamor.createElement(S().default.HashtagTableSection, {
				skip: this.state.skip,
				hashtags: a,
				minRowCount: Math.min(t.length, 10),
				noDataMessage: Glamor.createElement(ua, null),
				onSkipChange: this._onSkipChange,
				onHashtagClick: this._onHashtagClick
			}))
		}
		_getHashtags(e, t = !1) {
			const s = {},
				a = this.props.filters.query.trim().toLowerCase();
			e.forEach((e => {
				if (!e.caption) return;
				const n = Ns(e.caption);
				for (const o of n) t && !o.toLowerCase().includes(a) || (s[o] ? (s[o].stats.posts++, s[o].stats.likes += e.stats.likes, s[o].stats.comments += e.stats.comments) : s[o] = {
					name: o,
					stats: {
						posts: 1,
						likes: e.stats.likes || 0,
						comments: e.stats.comments || 0
					}
				})
			}));
			let n = [];
			for (const e in s) {
				const t = s[e];
				t.stats.engagement = 0 === t.stats.posts ? 0 : Math.round((t.stats.likes + 10 * t.stats.comments) / t.stats.posts), n.push(t)
			}
			return n = n.sort(((e, t) => t.stats.posts - e.stats.posts)), n
		}
	}
	var Ea = y().influx((e => ({
		filters: e.insights.filters
	})))(_a);
	class Ta extends k().default.Component {
		constructor(e) {
			super(e), this._getDefaultState = () => ({
				posts: [],
				followers: [],
				followings: [],
				analytics: {}
			}), this._onCloseClick = () => {
				y().transaction((e => {
					e.analytics.isReportOpen = !1
				}))
			}, this._onDownloadClick = () => {
				v().gaController.sendEvent("user", "insights:download-all-click"), zs.downloadResultsAsZip(this.props.task)
			}, this._updateData = async() => {
				const e = this.props.task;
				if (!e) return;
				const {
					followers: t,
					followings: s
				} = await I().chromeBus.send("insights.get-task-results.users", e.id), {
					posts: a
				} = await I().chromeBus.send("insights.get-task-results.posts", e.id), n = await xt.fetchAnalytics(this.props.lastScanOn);
				this.setState({
					posts: a,
					followers: t,
					followings: s,
					analytics: n
				})
			}, this._enrichPosts = e => {
				const t = this._getQuery(),
					s = Date.now(),
					a = {
						all: null,
						"24h": 24 * P().HOUR,
						"7d": 7 * P().DAY,
						"1m": 1 * P().MONTH,
						"3m": 3 * P().MONTH,
						"6m": 6 * P().MONTH,
						"12m": 12 * P().MONTH
					}[this.props.filters.posts];
				return e.map((e => {
					let n = !0;
					t && (n = e.caption && e.caption.toLowerCase().includes(t));
					let o = !0;
					return a && (o = s - e.on < a), { ...e,
						dateOk: o,
						queryOk: n,
						allFiltersOk: n && o
					}
				}))
			}, this._enrichPeers = e => {
				const t = this._getQuery(),
					s = this.props.filters.accounts,
					a = this.state.analytics;
				return e.map((e => {
					const n = xt.isFollower(e, a),
						o = xt.isFollowing(e, a);
					let i = !0;
					t && (i = e.fullName.toLowerCase().includes(t) || e.username.toLowerCase().includes(t) || e.userId.includes(t));
					const r = "all" === s || ("followers" === s ? n : "followings" === s ? o : "unilateral-followers" === s ? n && !o : "unilateral-followings" === s ? o && !n : "mutual" === s && (n && o));
					return { ...e,
						isFollower: n,
						isFollowing: o,
						allFiltersOk: i && r
					}
				}))
			}, this._posts = [], this._followers = [], this._followings = [], this.state = this._getDefaultState(), this._updateData()
		}
		componentDidUpdate(e) {
			var t, s;
			const a = e,
				n = this.props,
				o = null === (t = a.task) || void 0 === t ? void 0 : t.id,
				i = null === (s = n.task) || void 0 === s ? void 0 : s.id;
			a.activeUserId !== n.activeUserId && (this.setState(this._getDefaultState()), this._updateData()), o !== i && (this.setState(this._getDefaultState()), this._updateData()), a.lastScanOn !== n.lastScanOn && this._updateData(), o && o === i && !_.isEqual(a.task.resultCounters, n.task.resultCounters) && this._updateData()
		}
		render() {
			return this.props.task ? (this._posts = this._enrichPosts(this.state.posts), this._followers = this._enrichPeers(this.state.followers), this._followings = this._enrichPeers(this.state.followings), Glamor.createElement(S().default.InsightsBodyPanel, {
				avatarUrl: this.props.task.user.avatarUrl,
				name: `@${this.props.task.user.username}`,
				filter: this._constructFilter(),
				loadingOverlay: Glamor.createElement(Ws, null),
				infoSection: Glamor.createElement(Ys, {
					task: this.props.task
				}),
				statsSection: Glamor.createElement(ca, {
					task: this.props.task
				}),
				postingTimesSection: Glamor.createElement(Ca, {
					posts: this._posts
				}),
				postsSection: Glamor.createElement(ba, {
					posts: this._posts
				}),
				hashtagsSection: Glamor.createElement(Ea, {
					posts: this._posts
				}),
				usersSection: this._constractUsersSection(),
				onCloseClick: this._onCloseClick,
				onDownloadClick: this._onDownloadClick
			})) : null
		}
		_constructFilter() {
			const e = new Set,
				t = new Set;
			for (const s of this._followers) e.add(s.userId), s.allFiltersOk && t.add(s.userId);
			for (const s of this._followings) e.add(s.userId), s.allFiltersOk && t.add(s.userId);
			const s = this._posts.filter((e => e.allFiltersOk)).length;
			return Glamor.createElement(Hs, {
				accountsFilteredCount: t.size,
				accountsTotalCount: e.size,
				postsFilteredCount: s,
				postsTotalCount: this._posts.length
			})
		}
		_constractUsersSection() {
			return Glamor.createElement(ga, {
				task: this.props.task,
				followers: this._followers,
				followings: this._followings
			})
		}
		_getQuery() {
			return this.props.filters.query.toLowerCase().trim()
		}
	}
	var Aa = y().influx((e => ({
		activeUserId: e.authStatus.userId,
		lastScanOn: e.analytics.lastScanOn,
		filters: e.insights.filters,
		task: e.insights.tasks.find((t => t.id === e.analytics.selectedCard)),
		followStatus: e.igTask.followStatus
	})))(Ta);
	Z(), $(), J(), X(), Q(), P(), F(), ee(), E(), T(), v(), y(), te(), se(), S(), ae(), A(), J(), ne(), oe(), P(), F(), y(), ee(), I(), v(), te(), ie();
	const {
		hasEngagementData: Pa,
		calcEngagement: Ia,
		calcRate: Fa
	} = S().default.tagUtils;
	var xa = {
		fetchTagFromIg: Da,
		fetchTagsFromRedis: Oa,
		fetchLadder: async function e(t) {
			const s = e;
			s.cache || (s.cache = {});
			const a = y().model.state,
				n = a.authStatus.userId;
			if (!n) return {};
			const o = ie().default.getAccountStats(),
				i = Ia(o),
				r = J().default(`${Ga.apiUrl}/ladder/calculate`, {
					id: n,
					engagement: i,
					tags: t.join(",")
				});
			if (s.cache[r]) return s.cache[r];
			const l = await Oa(t),
				c = [];
			for (const e of t) {
				if (l[e]) continue;
				const t = await Da(e);
				t && c.push(t)
			}
			c.length && await I().chromeBus.send("tag-assist.send-tags-to-redis", c);
			F().env.is.production && (async() => {
				for (const e in l) await Da(e)
			})();
			const u = {
				low: [],
				medium: [],
				high: [],
				vhigh: [],
				nodata: [],
				relevant: []
			};
			try {
				const e = await ee().fetcher.fetchJson(r, {
					method: "POST"
				});
				u.low.push(...e.low || []), u.medium.push(...e.medium || []), u.high.push(...e.high || []), u.vhigh.push(...e.vhigh || []), u.nodata.push(...e.nodata || [])
			} catch (e) {
				return console.error("failed to fetch ladder", e), u
			} {
				const e = [];
				for (const t of Object.values(l)) e.push(t.relevantTags);
				for (const t of c) e.push(t.relevantTags);
				u.relevant = [].concat(...u.low).concat(...u.medium).concat(...u.high).concat(...u.vhigh)
			}
			for (const e in u) u[e] = u[e].map(te().decompressTag);
			const d = a.tagAssist.ladderConfig.tiers,
				h = [...Object.values(l), ...c];
			for (const e of h) {
				let t;
				if (Pa(e)) {
					const s = Ia(e),
						a = Fa(s, i);
					t = a < d.low ? u.low : a < d.medium ? u.medium : a < d.high ? u.high : u.vhigh
				} else t = u.nodata;
				t.find((t => t.name === e.name)) || t.push(e)
			}
			const m = new Set(t);
			for (const e in u) {
				const t = u[e];
				if ("nodata" === e) {
					u[e] = t.sort(((e, t) => m.has(e.name) ? -1 : m.has(t.name) ? 1 : null));
					continue
				}
				const s = {};
				for (const e of t) {
					const t = e.relevantTags.slice(0, 50);
					for (const e of t) s[e] = (s[e] || 0) + 1
				}
				u[e] = t.sort(((e, t) => {
					if (m.has(e.name)) return -1;
					if (m.has(t.name)) return 1;
					const a = s[e.name],
						n = s[t.name];
					return a || n ? a ? n ? n - a : -1 : 1 : null
				}))
			}
			for (const e in u) "nodata" !== e && (u[e] = u[e].filter(Pa));
			return u.relevant = u.relevant.slice(0, 120).map((e => ({
				name: e.name
			}))), s.cache[r] = u, oe().default({
				delay: 20 * P().MINUTE
			}, (() => {
				delete s.cache[r]
			})), u
		}
	};
	const Ga = F().env.options.tagAssist;
	async function Da(e) {
		const t = Da;
		t.initialized && (t.initialized = !0, t.incognito = !0, t.lastRunOn = null);
		const s = Date.now();
		if (t.lastRunOn && !t.incognito) {
			const e = s - t.lastRunOn,
				a = 2 * P().SECOND;
			e < a && await A().default(a - e)
		}
		t.lastRunOn = s, e = e.toLowerCase();
		const a = await I().chromeBus.send("ig-api.fetch-tag", e, {
			incognito: t.incognito
		});
		if (a.result) {
			const e = { ...a.result,
				name: a.result.name.toLowerCase(),
				lastScanOn: Date.now(),
				relevantTags: a.result.relevantTags.map((e => e.toLowerCase())).slice(0, Ga.maxRelevantTagsToKeep)
			};
			return I().chromeBus.send("tag-assist.tag-fetched-from-ig", e), e
		}
		if (t.incognito) return t.incognito = !1, Da(e);
		const n = ae().default((() => JSON.stringify(a.error)), "unknown");
		return v().gaController.sendEvent("user", "tag-assist:fetch-tag-from-ig-error", n), console.error("failed to fetch tags from ig", a.error), null
	}
	async function Oa(e) {
		if (0 === (e = ne().default(e).map((e => e.toLowerCase()))).length) return {};
		const t = y().model.state.authStatus.userId;
		if (!t) return {};
		let s;
		try {
			const a = J().default(Ga.apiUrl, {
				id: t,
				tags: e.join(",")
			});
			s = await ee().fetcher.fetchJson(a, {
				credentials: "omit"
			})
		} catch (e) {
			return console.error("failed to fetch tags from redis", e), {}
		}
		const a = {};
		for (const e in s.tags) {
			const t = te().decompressTag(`${e}:${s.tags[e]}`);
			t.name = t.name.toLowerCase(), t.relevantTags = t.relevantTags.map((e => e.toLowerCase())).slice(0, Ga.maxRelevantTagsToKeep), a[e] = t
		}
		return Object.keys(a).forEach((e => {
			Pa(a[e]) || delete a[e]
		})), a
	}
	const Ba = X().default();
	var Ua = {
		init: async function () {
			(function () {
				const e = y().model.state.tagAssist.collectionsTagData;
				for (const t in e) {
					const s = te().decompressTag(e[t]);
					se().default.tagData[t] = s
				}
			})(), async function () {
				const e = y().model.state,
					t = Date.now(),
					s = e.tagAssist.ladderConfig.lastUpdateOn;
				if (t - s < 1 * P().HOUR) return;
				const a = y().model.state.authStatus.userId;
				if (!a) return;
				let n;
				try {
					const e = J().default(`${La.apiUrl}/ladder/get-config`, {
							id: a
						}),
						t = await ee().fetcher.fetchJson(e);
					n = t.ladderConfig || null
				} catch (e) {
					console.error("failed to fetch ladder config", e)
				}
				if (!n) return;
				y().transaction((e => {
					e.tagAssist.ladderConfig = {
						lastUpdateOn: Date.now(),
						...n
					}
				}))
			}(), E().iframeBus.on("tag-assist.ig-creation-session-start", (() => {
				y().transaction((e => {
					e.tagAssist.igSelectedTags = []
				}))
			})), Ra({
				_captionChange_: "tag-assist.ig-caption-change",
				_sessionStart_: "tag-assist.ig-creation-session-start",
				_sessionEnd_: "tag-assist.ig-creation-session-end",
				_setCaption_: "tag-assist.ig-set-caption",
				_selectedTags_: "igSelectedTags"
			}), Ra({
				_captionChange_: "tag-assist.fcs-caption-change",
				_sessionStart_: "tag-assist.fcs-composer-opened",
				_sessionEnd_: "tag-assist.fcs-composer-closed",
				_setCaption_: "tag-assist.fcs-set-caption",
				_selectedTags_: "fcsSelectedTags"
			}), Ba((() => {
				y().transaction((e => {
					for (const t in e.tagAssist.collectionsTagData) {
						const s = se().default.tagData[t];
						s && (e.tagAssist.collectionsTagData[t] = te().compressTag(s))
					}
				}))
			})), y().model.observe((e => e.schedule.navigation.isOpen), (e => {
				e || y().transaction((e => {
					e.schedule.showTagAssist = !1
				}))
			})), await async function () {
				await new Promise((e => {
					y().model.state.tagAssist.collections.length > 0 ? e() : E().iframeBus.on("ig.ready", (async() => {
						const t = await E().iframeBus.send("tag-assist.read-collections-from-ls");
						y().transaction((e => {
							e.tagAssist.collections = t
						})), e()
					}))
				}))
			}(), Ma()
		},
		search: async function e() {
			const t = e,
				s = Z().generate();
			t.searchId = s;
			const a = y().model.state.tagAssist.query.trim().toLowerCase();
			if (y().transaction((e => {
					e.tagAssist.ladderLoadingTags = []
				})), !a) return void y().transaction((e => {
				e.tagAssist.searching = !1, e.tagAssist.ladder = null
			}));
			v().gaController.sendEvent("user", "tag-assist:search-perform");
			const n = te().extractTags(a).slice(0, La.maxTagsToQuery),
				o = await xa.fetchLadder(n);
			if (t.searchId !== s) return;
			const i = {};
			for (const e in o) {
				const t = o[e];
				for (const e of t) {
					!!e.lastScanOn && (i[e.name] = e)
				}
			}
			Object.assign(se().default.tagData, i), za(), y().transaction((e => {
				e.tagAssist.searching = !1, e.tagAssist.ladderLoadingTags = [], e.tagAssist.ladder = {};
				for (const t in o) {
					const s = o[t];
					e.tagAssist.ladder[t] = s.map((e => e.name))
				}
				0 === o[e.tagAssist.selectedGroupId].length && (e.tagAssist.selectedGroupId = "medium")
			}))
		},
		getTagData: function () {
			return se().default.tagData
		},
		onTagDataUpdate: Ba,
		registerUsage: function () {
			if (T().stateProxy.hasProPaid()) return void v().gaController.sendEvent("user", "pro-paid-usage:tag-assist");
			y().transaction((e => {
				const t = Math.floor(Date.now() / P().DAY);
				e.tagAssist.lastDayUsedOn ? e.tagAssist.lastDayUsedOn !== t && (e.billing.trial.tagAssist += 1, e.tagAssist.lastDayUsedOn = t) : e.tagAssist.lastDayUsedOn = t
			}))
		},
		selectTags: function (e, t = null) {
			const s = "sidebar" === t ? "sidebarSelectedTags" : "schedule" === t ? "fcsSelectedTags" : "igSelectedTags";
			y().transaction((t => {
				t.tagAssist[s] = [...t.tagAssist[s], ...e].filter($().default)
			}))
		},
		unselectTags: function (e, t = null) {
			const s = "sidebar" === t ? "sidebarSelectedTags" : "schedule" === t ? "fcsSelectedTags" : "igSelectedTags";
			y().transaction((t => {
				t.tagAssist[s] = t.tagAssist[s].filter((t => !e.includes(t)))
			}))
		},
		toggleTag: function (e, t = null) {
			const s = "sidebar" === t ? "sidebarSelectedTags" : "schedule" === t ? "fcsSelectedTags" : "igSelectedTags";
			y().transaction((t => {
				t.tagAssist[s].includes(e) ? t.tagAssist[s] = t.tagAssist[s].filter((t => t !== e)) : t.tagAssist[s].push(e)
			}))
		},
		copyTags: function (e) {
			const t = e.map((e => `#${e}`)).join(" ");
			navigator.clipboard.writeText(t)
		},
		checkTags: async function e(t) {
			const s = e,
				a = Z().generate();
			s.execId = a, y().transaction((e => {
				e.tagAssist.ladderLoadingTags = e.tagAssist.ladderLoadingTags.concat(t).filter((e => !se().default.tagData[e])).filter($().default)
			}));
			const n = await xa.fetchTagsFromRedis(t);
			if (Object.assign(se().default.tagData, n), za(), s.execId !== a) return;
			y().transaction((e => {
				e.tagAssist.ladderLoadingTags = e.tagAssist.ladderLoadingTags.filter((e => !se().default.tagData[e]))
			}));
			const o = async() => {
				if (s.execId !== a) return;
				const e = y().model.state.tagAssist.ladderLoadingTags[0];
				if (!e) return;
				const t = await xa.fetchTagFromIg(e);
				t && (se().default.tagData[e] = t, za()), y().transaction((t => {
					t.tagAssist.ladderLoadingTags = t.tagAssist.ladderLoadingTags.filter((t => t !== e))
				})), o()
			};
			o()
		},
		refreshTag: async function (e) {
			y().transaction((t => {
				t.tagAssist.ladderLoadingTags.push(e)
			})), delete se().default.tagData[e], za();
			const t = await xa.fetchTagFromIg(e);
			se().default.tagData[t.name] = t, za(), y().transaction((t => {
				Q().default(t.tagAssist.ladderLoadingTags, e)
			}))
		},
		stopTags: function (e) {
			y().transaction((t => {
				t.tagAssist.ladderLoadingTags = t.tagAssist.ladderLoadingTags.filter((t => !e.includes(t)))
			}))
		},
		stopAllTags: function () {
			y().transaction((e => {
				e.tagAssist.ladderLoadingTags = []
			}))
		},
		saveCollectionsToLs: function () {
			const e = y().model.state.tagAssist.collections;
			E().iframeBus.send("tag-assist.save-collections-to-ls", e)
		},
		checkCollectionTags: Na,
		updateCollectionsTagData: Ma
	};
	const La = F().env.options.tagAssist;

	function Ra({
		_captionChange_: e,
		_sessionStart_: t,
		_sessionEnd_: s,
		_setCaption_: a,
		_selectedTags_: n
	}) {
		let o = "",
			i = !0,
			r = !1;
		E().iframeBus.on(e, (e => {
			i && (o = e, y().transaction((e => {
				e.tagAssist[n] = te().extractTags(o, !0), r = !0
			})))
		})), E().iframeBus.on(t, (() => {
			let e;
			const t = y().model.observe((e => e.tagAssist[n].join("-")), (async() => {
				if (r) return void(r = !1);
				const t = y().model.state,
					s = te().extractTags(o, !0),
					l = t.tagAssist[n],
					c = l.filter((e => !s.includes(e))),
					u = s.filter((e => !l.includes(e)));
				for (const e of u) o = o.replace(new RegExp(`\\s#${e}([^\\p{L}\\d_])`, "gui"), "$1").replace(new RegExp(`^#${e}([^\\p{L}\\d_])`, "gui"), "$1").replace(new RegExp(`#${e}\\s`, "gui"), "").replace(new RegExp(`\\s#${e}$`, "gui"), "").replace(new RegExp(`#${e}$`, "gui"), "");
				for (const e of c) o ? o += ` #${e}` : o = `#${e}`;
				clearTimeout(e), i = !1, await E().iframeBus.send(a, o), e = setTimeout((() => {
					i = !0
				}))
			}), !1);
			E().iframeBus.on(s, (() => {
				t && t()
			}))
		}))
	}
	async function Ma() {
		const e = y().model.state.tagAssist.collections.map((e => e.tags)).flat();
		y().transaction((t => {
			const s = Date.now(),
				a = t.tagAssist.collectionsTagData,
				n = Object.keys(a).filter((t => {
					if (!e.includes(t)) return !0;
					const n = te().decompressTag(a[t]);
					return s - n.lastScanOn > La.collectionsTagDataTtl
				}));
			for (const e of n) delete a[e]
		}));
		Na(e.filter((e => !y().model.state.tagAssist.collectionsTagData[e])).slice(0, 80))
	}
	async function Na(e, {
		useRedis: t = !0
	} = {}) {
		y().transaction((t => {
			t.tagAssist.collectionsLoadingTags = t.tagAssist.collectionsLoadingTags.concat(e).filter($().default)
		}));
		let s = {};
		t && (s = await xa.fetchTagsFromRedis(e), y().transaction((e => {
			for (const t in s) {
				const a = s[t];
				e.tagAssist.collectionsTagData[t] = te().compressTag(a)
			}
			e.tagAssist.collectionsLoadingTags = e.tagAssist.collectionsLoadingTags.filter((e => !s[e]))
		}))), e = e.filter((e => !s[e])).slice(0, 30), y().transaction((t => {
			t.tagAssist.collectionsLoadingTags = e
		}));
		for (const t of e) {
			const e = await xa.fetchTagFromIg(t);
			y().transaction((s => {
				e && (s.tagAssist.collectionsTagData[t] = te().compressTag(e)), s.tagAssist.collectionsLoadingTags = s.tagAssist.collectionsLoadingTags.filter((e => e !== t))
			}))
		}
	}

	function za() {
		Ba(se().default.tagData)
	}
	k(), S(), T(), v(), y(), k(), S(), y(), E(), v(), T();
	const Va = {
		tip: {
			"&:not(:last-child)": {
				marginBottom: k().default.space.g3
			}
		}
	};
	class Ha extends k().default.Component {
		constructor(e) {
			super(e), this._onScheduleTipOkClick = () => {
				T().actions.acknowledge.dispatch("scheduleTip"), v().gaController.sendEvent("user", "tips:schedule-tip-ok-click")
			}, this._onScheduleTipShowClick = () => {
				T().actions.acknowledge.dispatch("scheduleTip"), v().gaController.sendEvent("user", "tips:schedule-tip-show-click"), y().transaction((e => {
					e.sidebar.selectedTabId = "tab-scheduling", e.sidebar.isOpen = !0
				}))
			}, this._onStoryLimitationsOkClick = () => {
				T().actions.acknowledge.dispatch("storyLimitations"), v().gaController.sendEvent("user", "tips:story-limitations-ok-click")
			}, this._onIframePathChange = e => {
				let t;
				t = e.startsWith("/create/story") ? "create-story" : e.startsWith("/create/") ? "create-post" : null, this.setState({
					pageType: t
				})
			}, this.state = {
				pageType: null
			}
		}
		componentDidMount() {
			E().iframeBus.on("ig.path-change", this._onIframePathChange)
		}
		componentWillUnmount() {
			E().iframeBus.off("ig.path-change", this._onIframePathChange)
		}
		render() {
			const e = [],
				t = this.state.pageType,
				s = this.props.acknowledged;
			return "create-post" === t ? (e.push({
				key: "crop-tip",
				content: this._renderCropTip()
			}), s.scheduleTip || e.push({
				key: "schedule-tip",
				content: this._renderScheduleTip()
			})) : "create-story" === t && (e.push({
				key: "crop-tip",
				content: this._renderCropTip()
			}), s.story || e.push({
				key: "story-limitations",
				content: this._renderStoryLimitations()
			})), e.length ? Glamor.createElement("div", null, e.map((e => Glamor.createElement("div", {
				css: Va.tip,
				key: e.key
			}, e.content)))) : null
		}
		_renderStoryLimitations() {
			return Glamor.createElement(S().default.Tip, {
				content: Glamor.createElement(k().default.Fragment, null, Glamor.createElement("b", null, "Limitations:"), " When posting stories a few features such as tagging, gifs and mentions on videos are not implemented in the browser version of Instagram website and thus not available on the app."),
				buttons: [{
					label: "OK, GOT IT",
					onClick: this._onStoryLimitationsOkClick
				}]
			})
		}
		_renderCropTip() {
			return Glamor.createElement(S().default.Tip, {
				icon: "bulb",
				content: [Glamor.createElement(k().default.Fragment, null, Glamor.createElement("b", null, "Tips:"), " Recommended Instagram photo / video sizes and formats can be found in ", Glamor.createElement("a", {
					href: "https://inssist.com/knowledge-base/instagram-image-size-cheat-sheet",
					target: "_blank"
				}, "our guide"), "."), Glamor.createElement(k().default.Fragment, null, "You can pre-edit, crop or scale your photo with a free ", Glamor.createElement("a", {
					href: "https://www.iloveimg.com/crop-image",
					target: "_blank"
				}, "iloveimg"), " tool, and your video with ", Glamor.createElement("a", {
					href: "https://ezgif.com/resize-video",
					target: "_blank"
				}, "ezgif"), " tool.")]
			})
		}
		_renderScheduleTip() {
			return Glamor.createElement(S().default.Tip, {
				icon: "bulb",
				content: [Glamor.createElement(k().default.Fragment, null, Glamor.createElement("b", null, "Tips:"), " A few features such as posting carousels and editing published posts are not available on Instagram Web client."), Glamor.createElement(k().default.Fragment, null, "You can post carousels, edit posts and schedule posts for later with Post Assistant module on the left.")],
				buttons: [{
					label: "OK, GOT IT",
					onClick: this._onScheduleTipOkClick
				}, {
					label: "SHOW ME, GURU",
					onClick: this._onScheduleTipShowClick
				}]
			})
		}
	}
	var $a = y().influx((e => ({
		acknowledged: {
			story: -1 !== e.acknowledged.storyLimitations,
			scheduleTip: -1 !== e.acknowledged.scheduleTip
		}
	})))(Ha);
	ie(), k(), S(), y(), ie(), k(), S(), y(), T(), k(), S(), y(), v(), ie(), k(), S(), b(), y(), T(), v(), ie();
	class Wa extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onGoToTagClick = () => {
				const e = b().ig.hashtag.url(this.props.tag.name);
				window.electron ? window.open(e, `hashtag-details.${this.props.tag.name}`) : chrome.tabs.create({
					url: e,
					active: !0
				})
			}, this._onRefreshTagClick = () => {
				v().gaController.sendEvent("user", "tag-assist:refresh-tag-click"), Ua.refreshTag(this.props.tag.name)
			}, this._onUpgradeClick = () => {
				vt.openBilling("tag-assist")
			}, this._onUpgradeCancelClick = () => {
				y().transaction((e => {
					e.tagAssist.tagMetricsUpsellDismissed = !0
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.TagStatsPopup, {
				tag: this.props.tag,
				accountStats: this.props.accountStats,
				tiers: this.props.tiers,
				markerDy: this.props.markerDy,
				markerAtLeft: this.props.markerAtLeft,
				showRefreshButton: this.props.showRefreshButton,
				showUpgradeToPro: !this.props.hasPro,
				onGoToTagClick: this._onGoToTagClick,
				onRefreshTagClick: this._onRefreshTagClick,
				onUpgradeClick: this._onUpgradeClick,
				onUpgradeCancelClick: this._onUpgradeCancelClick
			})
		}
	}
	var qa = y().influx((e => ({
		tiers: e.tagAssist.ladderConfig.tiers,
		hasPro: T().stateProxy.hasPro({
			feature: "tagAssist"
		}),
		accountStats: ie().default.getAccountStats(),
		showRefreshButton: e.experiments.enabled
	})))(Wa);
	const {
		hasEngagementData: ja,
		engagementToBgColor: Ya
	} = S().default.tagUtils;
	class Ka extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._constructStatsPopup = ({
				markerDy: e,
				markerAtLeft: t
			}) => Glamor.createElement(qa, {
				tag: this.props.tag,
				markerDy: e,
				markerAtLeft: t
			}), this._onClick = () => {
				if ("sidebar" !== this.props.placement || "collections" !== this.props.selectedTabId) Ua.registerUsage(), Ua.toggleTag(this.props.tag.name, this.props.placement), v().gaController.sendEvent("user", "tag-assist:tag-click");
				else {
					if (ja(this.props.tag)) return;
					Ua.checkCollectionTags([this.props.tag.name], {
						useRedis: !1
					})
				}
			}, t
		}
		render() {
			const e = !!this.props.tag.lastScanOn;
			return Glamor.createElement(S().default.TagPill, {
				name: this.props.tag.name,
				color: this._getColor(),
				loading: this._isLoading(),
				selected: this._isSelected(),
				banned: this.props.tag.isBanned,
				flagged: this.props.tag.isFlagged,
				clickable: this._isClickable(),
				renderStatsPopup: e ? this._constructStatsPopup : null,
				onClick: this._onClick
			})
		}
		_isClickable() {
			return "sidebar" !== this.props.placement || "collections" !== this.props.selectedTabId || !ja(this.props.tag)
		}
		_getColor() {
			return this.props.noColor ? null : Ya(this.props.tag, this.props.accountStats, this.props.tiers)
		}
		_isLoading() {
			return ("collections" === this.props.selectedTabId ? this.props.collectionsLoadingTags : this.props.ladderLoadingTags).includes(this.props.tag.name)
		}
		_isSelected() {
			return !this.props.disableSelect && this.props.selectedTags.includes(this.props.tag.name)
		}
	}
	var Za = y().influx(((e, t) => ({
		tiers: e.tagAssist.ladderConfig.tiers,
		accountStats: ie().default.getAccountStats(),
		selectedTabId: e.tagAssist.selectedTabId,
		selectedTags: ie().default.getSelectedTags(t.placement),
		ladderLoadingTags: e.tagAssist.ladderLoadingTags,
		collectionsLoadingTags: e.tagAssist.collectionsLoadingTags
	})))(Ka);
	class Ja extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this.scuIgnoreFields = ["tagData"], this._constructTag = (e, t) => {
				let s = this.props.tagData[e] || null;
				return (!s || !this.props.hasPro && this.props.upsellDismissed) && (s = {
					name: e
				}), {
					id: e,
					element: Glamor.createElement(Za, {
						tag: s,
						noColor: !this.props.hasPro,
						placement: this.props.placement
					})
				}
			}, t
		}
		render() {
			return Glamor.createElement(S().default.TagAssistList, {
				label: this.props.label,
				tags: this.props.tagNames.map(this._constructTag),
				noTagsMessage: "No tags found"
			})
		}
	}
	var Xa = y().influx((e => ({
		hasPro: T().stateProxy.hasPro({
			feature: "tagAssist"
		}),
		upsellDismissed: e.tagAssist.tagMetricsUpsellDismissed
	})))(Ja);
	k(), S(), y(), T(), ie(), k(), S(), y(), ie();
	const {
		hasEngagementData: Qa,
		calcEngagement: en,
		calcRate: tn
	} = S().default.tagUtils;
	class sn extends k().default.Component {
		constructor(e) {
			super(e), this.scuIgnoreFields = ["tagData"], this._renderTagPill = ({
				name: e
			}) => Glamor.createElement(Za, {
				tag: this._getTag(e),
				placement: this.props.placement
			}), this._onEngagementSortClick = () => {
				y().transaction((e => {
					const t = "summary" === this.props.placement ? "summaryEngagementSort" : "ladderEngagementSort";
					e.tagAssist[t] ? "descending" === e.tagAssist[t] ? e.tagAssist[t] = "ascending" : e.tagAssist[t] = null : e.tagAssist[t] = "descending"
				}))
			}, this._onPostCountSortClick = () => {
				y().transaction((e => {
					const t = "summary" === this.props.placement ? "summaryPostCountSort" : "ladderPostCountSort";
					e.tagAssist[t] ? "descending" === e.tagAssist[t] ? e.tagAssist[t] = "ascending" : e.tagAssist[t] = null : e.tagAssist[t] = "descending"
				}))
			}, this._onSkipChange = e => {
				this.setState({
					skip: e
				})
			}, this._onCheckClick = e => {
				Ua.checkTags([e])
			}, this._onCheckAllClick = () => {
				const e = this._getPageTags().map((e => e.name));
				Ua.checkTags(e)
			}, this._onStopAllClick = () => {
				const e = this._getPageTags().map((e => e.name));
				Ua.stopTags(e)
			}, this.tags = [], this.state = {
				skip: 0
			}
		}
		render() {
			return this.tags = this._getSortedTags(), Glamor.createElement(S().default.TagAssistTable, {
				skip: this.state.skip,
				pageSize: this._getPageSize(),
				engagementSort: this.props.engagementSort,
				renderTagPill: this._renderTagPill,
				tags: this.tags,
				onEngagementSortClick: this.props.isNoDataGroup ? null : this._onEngagementSortClick,
				onPostCountSortClick: this.props.isNoDataGroup ? null : this._onPostCountSortClick,
				showCheckAll: this._shouldShowCheckAll(),
				showStopAll: this._shouldShowStopAll(),
				onSkipChange: this._onSkipChange,
				onCheckClick: this._onCheckClick,
				onCheckAllClick: this._onCheckAllClick,
				onStopAllClick: this._onStopAllClick
			})
		}
		_getPageSize() {
			return "summary" === this.props.placement ? 100 : window.innerHeight < 890 && "sidebar" === this.props.placement ? 8 : 10
		}
		_getSortedTags() {
			const e = this.props.engagementSort,
				t = this.props.postCountSort,
				s = en(this.props.accountStats);
			return this.props.tagNames.map((e => {
				const t = this._getTag(e),
					a = en(t);
				return {
					name: t.name,
					hasData: Qa(t),
					banned: t.isBanned,
					flagged: t.isFlagged,
					fetching: this.props.loadingTags.includes(t.name),
					postCount: t.avgPosts,
					engagementRate: tn(a, s)
				}
			})).sort(((s, a) => {
				if (e) {
					if (!s.hasData && !a.hasData) return null;
					if (s.hasData && !a.hasData) return -1;
					if (!s.hasData && a.hasData) return 1;
					return ("ascending" === e ? 1 : -1) * (s.engagementRate - a.engagementRate)
				}
				if (t) {
					return ("ascending" === t ? 1 : -1) * (s.postCount - a.postCount)
				}
				return null
			}))
		}
		_shouldShowCheckAll() {
			if (!this.props.isNoDataGroup) return !1;
			const e = this._getPageTags();
			return !e.some((e => this.props.loadingTags.includes(e.name))) && e.filter((e => !this.props.tagData[e.name])).length > 0
		}
		_shouldShowStopAll() {
			if (!this.props.isNoDataGroup) return !1;
			return this._getPageTags().some((e => this.props.loadingTags.includes(e.name)))
		}
		_getPageTags() {
			const e = this.state.skip,
				t = this._getPageSize();
			return this.tags.slice(e, e + t)
		}
		_getTag(e) {
			return this.props.tagData[e] || {
				name: e
			}
		}
	}
	var an = y().influx(((e, t) => {
		const s = "ladder" === e.tagAssist.selectedTabId && "nodata" === e.tagAssist.selectedGroupId;
		let a, n;
		return s ? (a = null, n = null) : "summary" === t.placement ? (a = e.tagAssist.summaryEngagementSort, n = e.tagAssist.summaryPostCountSort) : (a = e.tagAssist.ladderEngagementSort, n = e.tagAssist.ladderPostCountSort), {
			loadingTags: e.tagAssist.ladderLoadingTags,
			accountStats: ie().default.getAccountStats(),
			engagementSort: a,
			postCountSort: n,
			isNoDataGroup: s
		}
	}))(sn);
	class nn extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this.scuIgnoreFields = ["tagData"], this._onGroupClick = e => {
				y().transaction((t => {
					t.tagAssist.selectedGroupId = e
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.TagAssistLadder, {
				selectedGroupId: this.props.selectedGroupId,
				groups: this._getGroups(),
				content: this._constructContent(),
				blurContent: this._shouldBlurContent(),
				showNotEnoughDataWarning: this._shouldShowNotEnoughDataWarning(),
				onGroupClick: this._onGroupClick
			})
		}
		_getGroups() {
			return this.props.ladder ? [this._getGroupLow(), this._getGroupMedium(), this._getGroupHigh(), this._getGroupVHigh(), this._getGroupNoData()] : null
		}
		_constructContent() {
			if (!this.props.ladder) return null;
			const e = this.props.ladder[this.props.selectedGroupId];
			return 0 === e.length ? null : Glamor.createElement(an, {
				key: this.props.selectedGroupId,
				tagNames: e,
				tagData: this.props.tagData,
				tagDataKey: this.props.tagDataKey,
				placement: this.props.placement
			})
		}
		_shouldBlurContent() {
			return !this.props.hasPro
		}
		_shouldShowNotEnoughDataWarning() {
			if (!this.props.ladder) return !1;
			return this.props.ladder[this.props.selectedGroupId].length < 10
		}
		_getGroupLow() {
			const e = this._getGroupCount("low");
			return {
				id: "low",
				title: "LOW",
				count: e,
				tooltip: {
					text: [Glamor.createElement(k().default.Fragment, null, "Hashtags of this group have a ", Glamor.createElement("b", null, "LOW"), " engagement comparing to your account."), Glamor.createElement(k().default.Fragment, null, "It is quite likely to get ranked among the top posts for these hashtags. Using these hashtags should boost your engagement."), !!e && Glamor.createElement(k().default.Fragment, null, "You selected ", Glamor.createElement("b", null, e), " ", 1 === e ? "hashtag" : "hashtags", " of this group.")]
				}
			}
		}
		_getGroupMedium() {
			const e = this._getGroupCount("medium");
			return {
				id: "medium",
				title: "MEDIUM",
				count: e,
				tooltip: {
					text: [Glamor.createElement(k().default.Fragment, null, "Hashtags of this group have a ", Glamor.createElement("b", null, "MEDIUM"), " engagement comparing to your account."), Glamor.createElement(k().default.Fragment, null, "It is possible to get ranked among the top posts for these hashtags. Using these hashtags is optimal for your account. Mix them with low hashtags to get ranked higher among them."), !!e && Glamor.createElement(k().default.Fragment, null, "You selected ", Glamor.createElement("b", null, e), " ", 1 === e ? "hashtag" : "hashtags", " of this group.")]
				}
			}
		}
		_getGroupHigh() {
			const e = this._getGroupCount("high");
			return {
				id: "high",
				title: "HIGH",
				count: e,
				tooltip: {
					text: [Glamor.createElement(k().default.Fragment, null, "Hashtags of this group have a ", Glamor.createElement("b", null, "HIGH"), " engagement comparing to your account."), Glamor.createElement(k().default.Fragment, null, "It will be possible to get ranked among the top posts for these hashtags if your post also ranks among the low and medium hashtags."), !!e && Glamor.createElement(k().default.Fragment, null, "You selected ", Glamor.createElement("b", null, e), " ", 1 === e ? "hashtag" : "hashtags", " of this group.")]
				}
			}
		}
		_getGroupVHigh() {
			const e = this._getGroupCount("vhigh");
			return {
				id: "vhigh",
				title: "V.HIGH",
				tooltip: {
					text: [Glamor.createElement(k().default.Fragment, null, "Hashtags of this group have a ", Glamor.createElement("b", null, "VERY HIGH"), " engagement comparing to your account."), Glamor.createElement(k().default.Fragment, null, "It is unlikely to get ranked among the top posts for these hashtags. It is better to avoid these hashtags if possible."), !!e && Glamor.createElement(k().default.Fragment, null, "You selected ", Glamor.createElement("b", null, e), " ", 1 === e ? "hashtag" : "hashtags", " of this group.")]
				}
			}
		}
		_getGroupNoData() {
			if (0 === this.props.ladder.nodata.length) return null;
			return {
				id: "nodata",
				title: "NO DATA",
				tooltip: {
					text: [Glamor.createElement(k().default.Fragment, null, "Inssist has no engagement data for these hashtags (yet)."), Glamor.createElement(k().default.Fragment, null, "Click CHECK TAGS button to request data for these hashtags.")]
				}
			}
		}
		_getGroupCount(e) {
			return this.props.hasPro ? (this.props.ladder[e] || []).filter((e => this.props.selectedTags.includes(e))).length : null
		}
	}
	var on = y().influx(((e, t) => ({
		selectedGroupId: e.tagAssist.selectedGroupId,
		selectedTags: ie().default.getSelectedTags(t.placement),
		ladder: ie().default.getLadderLatinOnlyAware(),
		hasPro: T().stateProxy.hasPro({
			feature: "tagAssist"
		})
	})))(nn);
	k(), S(), y(), te();
	class rn extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onInput = e => {
				y().transaction((t => {
					t.tagAssist.sidebarSelectedTags = te().extractTags(e), t.tagAssist.sidebarSelectedTagsAsText = e
				}))
			}, this._onCopyAllClick = () => {
				const e = te().extractTags(this.props.sidebarSelectedTagsAsText);
				Ua.copyTags(e)
			}, t
		}
		componentDidUpdate(e) {
			const t = e,
				s = this.props;
			if (t.sidebarSelectedTagsAsText === s.sidebarSelectedTagsAsText) {
				const e = t.sidebarSelectedTags.map((e => `#${e}`)).join(" "),
					a = s.sidebarSelectedTags.map((e => `#${e}`)).join(" ");
				e !== a && y().transaction((e => {
					e.tagAssist.sidebarSelectedTagsAsText = a
				}))
			}
		}
		render() {
			return Glamor.createElement(S().default.TagAssistTextarea, {
				label: this._getLabel(),
				value: this._getValue(),
				onInput: this._onInput,
				onCopyAllClick: this._onCopyAllClick
			})
		}
		_getLabel() {
			return 0 === this.props.sidebarSelectedTags.length ? "hashtags" : `hashtags (${this.props.sidebarSelectedTags.length})`
		}
		_getValue() {
			return this.props.sidebarSelectedTagsAsText
		}
	}
	var ln = y().influx((e => ({
		sidebarSelectedTags: e.tagAssist.sidebarSelectedTags,
		sidebarSelectedTagsAsText: e.tagAssist.sidebarSelectedTagsAsText
	})))(rn);
	k(), S(), y(), ie();
	class cn extends k().default.Component {
		constructor(e) {
			super(e), this._onQueryInput = e => {
				y().transaction((t => {
					t.tagAssist.query = e, e ? t.tagAssist.searching = !0 : (t.tagAssist.searching = !1, t.tagAssist.ladder = null)
				})), clearTimeout(this.searchTimeout), this.searchTimeout = this.setTimeout((() => {
					this.searchTimeout = null, Ua.search()
				}), 800)
			}, this._onClearQueryClick = () => {
				y().transaction((e => {
					e.tagAssist.query = ""
				})), clearTimeout(this.searchTimeout), this.searchTimeout = null, Ua.search()
			}, this._onLatinOnlyClick = () => {
				y().transaction((e => {
					e.tagAssist.latinOnly = !e.tagAssist.latinOnly
				}))
			}, this.ladder = null, this.searchTimeout = null
		}
		componentDidMount() {
			this.props.query && Ua.search()
		}
		componentWillUnmount() {
			super.componentWillUnmount(), this.searchTimeout && Ua.search()
		}
		render() {
			return Glamor.createElement(S().default.TagAssistSearchInput, {
				type: this._getType(),
				query: this.props.query,
				placeholder: this._getPlaceholder(),
				loading: this.props.loading,
				latinOnly: this.props.latinOnly,
				onQueryInput: this._onQueryInput,
				onClearQueryClick: this._onClearQueryClick,
				onLatinOnlyClick: this._onLatinOnlyClick
			})
		}
		_getType() {
			return "search" === this.props.selectedTabId ? "search" : "ladder" === this.props.selectedTabId ? "ladder" : null
		}
		_getPlaceholder() {
			var e;
			const t = (null === (e = this.props.accountStats) || void 0 === e ? void 0 : e.mostUsedTags) || [];
			return 0 === t.length ? function (...e) {
				return e[Math.floor(Math.random() * e.length)]
			}("#travel", "#design", "#nature", "#life", "#instagram") : t.slice(0, 2).map((e => `#${e}`)).join(" ")
		}
	}
	var un = y().influx((e => ({
		query: e.tagAssist.query,
		loading: e.tagAssist.searching,
		latinOnly: e.tagAssist.latinOnly,
		selectedTabId: e.tagAssist.selectedTabId,
		accountStats: ie().default.getAccountStats()
	})))(cn);
	class dn extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this.scuIgnoreFields = ["tagData"], t
		}
		render() {
			return Glamor.createElement(S().default.TagAssistPanelSearch, {
				type: this._getType(),
				input: this._constructInput(),
				content: this._constructContent(),
				textarea: this._constructTextarea(),
				showWelcome: this._shouldShowWelcome()
			})
		}
		_getType() {
			return "search" === this.props.selectedTabId ? "search" : "ladder" === this.props.selectedTabId ? "ladder" : null
		}
		_constructInput() {
			return Glamor.createElement(un, null)
		}
		_constructContent() {
			if ("search" === this.props.selectedTabId) {
				var e;
				const t = (null === (e = this.props.ladder) || void 0 === e ? void 0 : e.relevant) || [];
				return Glamor.createElement(Xa, {
					label: "relevant hashtags",
					tagNames: t,
					tagData: this.props.tagData,
					tagDataKey: this.props.tagDataKey,
					placement: this.props.placement
				})
			}
			if ("ladder" === this.props.selectedTabId) return Glamor.createElement(on, {
				tagData: this.props.tagData,
				tagDataKey: this.props.tagDataKey,
				placement: this.props.placement
			})
		}
		_constructTextarea() {
			return "sidebar" === this.props.placement && (this.props.ladder || this.props.sidebarSelectedTags.length > 0) ? Glamor.createElement(ln, null) : null
		}
		_shouldShowWelcome() {
			return !this.props.ladder
		}
	}
	var hn = y().influx((e => ({
		ladder: ie().default.getLadderLatinOnlyAware(),
		selectedTabId: e.tagAssist.selectedTabId,
		sidebarSelectedTags: e.tagAssist.sidebarSelectedTags
	})))(dn);
	k(), S(), y(), ie();
	class mn extends k().default.Component {
		constructor(e) {
			super(e), this.scuIgnoreFields = ["tagData"], this._onTakeAllClick = () => {
				Ua.selectTags(this.tags, this.props.placement)
			}, this._onRemoveAllClick = () => {
				Ua.unselectTags(this.tags, this.props.placement)
			}, this._onCheckAllClick = () => {
				Ua.checkTags(this.tags)
			}, this._onStopAllClick = () => {
				Ua.stopAllTags()
			}, this.tags = [...e.selectedTags]
		}
		render() {
			const e = this._getRanking();
			return Glamor.createElement(S().default.TagAssistPanelSummary, {
				rankingTitle: e.title,
				rankingColor: e.color,
				rankingDescription: e.description,
				showTakeAll: this._shouldShowTakeAll(),
				showRemoveAll: this._shouldShowRemoveAll(),
				showCheckAll: this._shouldShowCheckAll(),
				showStopAll: this._shouldShowStopAll(),
				content: this._constructContent(),
				onTakeAllClick: this._onTakeAllClick,
				onRemoveAllClick: this._onRemoveAllClick,
				onCheckAllClick: this._onCheckAllClick,
				onStopAllClick: this._onStopAllClick
			})
		}
		_shouldShowTakeAll() {
			return 0 !== this.tags.length && this.tags.some((e => !this.props.selectedTags.includes(e)))
		}
		_shouldShowRemoveAll() {
			return 0 !== this.tags.length && this.tags.every((e => this.props.selectedTags.includes(e)))
		}
		_shouldShowCheckAll() {
			return 0 !== this.tags.length && (!(this.props.loadingTags.length > 0) && this.tags.filter((e => !this.props.tagData[e])).filter((e => !this.props.loadingTags.includes(e))).length > 0)
		}
		_shouldShowStopAll() {
			return this.tags.some((e => this.props.loadingTags.includes(e)))
		}
		_getRanking() {
			const e = this.props.summaryStatus.id;
			return "n/a" === e ? this._getRankingNA() : "not-enough-low" === e ? this._getRankingNotBalanced("low") : "not-enough-high" === e ? this._getRankingNotBalanced("high") : this._getRankingBalanced()
		}
		_constructContent() {
			return Glamor.createElement(an, {
				tagNames: this.tags,
				tagData: this.props.tagData,
				tagDataKey: this.props.tagDataKey,
				placement: this.props.placement
			})
		}
		_getRankingNA() {
			return {
				title: "N/A",
				color: this.props.summaryStatus.color,
				description: Glamor.createElement(k().default.Fragment, null, "Please add at least 9 hashtags with data to get a hashtag ladder ranking. Find out more in our ", Glamor.createElement("a", {
					href: "https://inssist.com/knowledge-base/ultimate-instagram-hashtag-guide",
					target: "_blank"
				}, "Hashtag Guide"), ".")
			}
		}
		_getRankingNotBalanced(e) {
			return e = e.toUpperCase(), {
				title: "Not Balanced",
				color: this.props.summaryStatus.color,
				description: Glamor.createElement(k().default.Fragment, null, "Your hashtag selection is missing hashtags from ", e, " engagement group. Try to find and add more ", e, " hashtags to your caption. Find out more in our ", Glamor.createElement("a", {
					href: "https://inssist.com/knowledge-base/ultimate-instagram-hashtag-guide",
					target: "_blank"
				}, "Hashtag Guide"), ".")
			}
		}
		_getRankingBalanced() {
			return {
				title: "Balanced",
				color: this.props.summaryStatus.color,
				description: Glamor.createElement(k().default.Fragment, null, "Your hashtag selection looks spot on with competitive and niche hashtags mixed together. Find out more in our ", Glamor.createElement("a", {
					href: "https://inssist.com/knowledge-base/ultimate-instagram-hashtag-guide",
					target: "_blank"
				}, "Hashtag Guide"), ".")
			}
		}
	}
	var pn = y().influx(((e, t) => ({
		loadingTags: e.tagAssist.ladderLoadingTags,
		selectedTags: ie().default.getSelectedTags(t.placement),
		summaryStatus: ie().default.getSummaryStatus(t.placement)
	})))(mn);
	k(), S(), y(), k(), S(), $(), y(), te(), T(), v(), ie();
	class gn extends k().default.Component {
		constructor(e) {
			super(e), this._constructTag = e => ({
				id: e.name,
				element: Glamor.createElement(Za, {
					tag: e,
					placement: this.props.placement
				})
			}), this._onCopyAllClick = () => {
				v().gaController.sendEvent("user", "tag-assist:collection-copy-all");
				const e = this.tags.map((e => e.name));
				Ua.copyTags(e)
			}, this._onTakeAllClick = () => {
				v().gaController.sendEvent("user", "tag-assist:collection-take-all");
				const e = this.tags.map((e => e.name));
				Ua.selectTags(e, this.props.placement)
			}, this._onRemoveAllClick = () => {
				v().gaController.sendEvent("user", "tag-assist:collection-remove-all");
				const e = this.tags.map((e => e.name));
				Ua.unselectTags(e, this.props.placement)
			}, this._onNameInput = e => {
				y().transaction((t => {
					this._getCollection(t).editName = e
				}))
			}, this._onTextInput = e => {
				y().transaction((t => {
					this._getCollection(t).editText = e
				}))
			}, this._onEditClick = () => {
				T().stateProxy.hasPro({
					feature: "tagAssist"
				}) ? y().transaction((e => {
					for (const t of e.tagAssist.collections) t.editing = !1;
					const t = this._getCollection(e);
					t.editing = !0, t.editName = t.editName || this.props.collection.name, t.editText = t.editText || this.tags.map((e => `#${e.name}`)).join(" ")
				})) : vt.openBilling("tag-assist")
			}, this._onSaveClick = () => {
				v().gaController.sendEvent("user", "tag-assist:collection-edit"), y().transaction((e => {
					const t = this._getCollection(e);
					t.name = t.editName, t.tags = te().extractTags(t.editText), t.editing = !1, t.editName = "", t.editText = ""
				})), Ua.registerUsage(), Ua.saveCollectionsToLs(), Ua.updateCollectionsTagData()
			}, this._onCancelClick = () => {
				y().transaction((e => {
					const t = this._getCollection(e);
					t.editing = !1, t.editName = "", t.editText = ""
				}))
			}, this._onDeleteClick = () => {
				v().gaController.sendEvent("user", "tag-assist:collection-delete"), y().transaction((e => {
					e.tagAssist.collections = e.tagAssist.collections.filter((e => e.id !== this.props.collectionId))
				})), Ua.saveCollectionsToLs(), Ua.updateCollectionsTagData()
			}, this.tags = []
		}
		render() {
			this.tags = this.props.collection.tags.map((e => this.props.tagData[e] || e)).map(te().decompressTag);
			const e = this.tags.every((e => this.props.selectedTags.includes(e.name))),
				t = te().extractTags(this.props.collection.editText).filter($().default).length;
			return Glamor.createElement(S().default.TagAssistCollection, {
				name: this.props.collection.name,
				tags: this.tags.map(this._constructTag),
				editing: this.props.collection.editing,
				editName: this.props.collection.editName,
				editText: this.props.collection.editText,
				editTextLabel: this._getEditTextLabel(t),
				editErrorMessage: this._getEditErrorMessage(t),
				showCopyAll: this.tags.length > 0,
				showTakeAll: "sidebar" !== this.props.placement && this.tags.length > 0 && !e,
				showRemoveAll: "sidebar" !== this.props.placement && this.tags.length > 0 && e,
				onCopyAllClick: this._onCopyAllClick,
				onTakeAllClick: this._onTakeAllClick,
				onRemoveAllClick: this._onRemoveAllClick,
				onNameInput: this._onNameInput,
				onTextInput: this._onTextInput,
				onEditClick: this._onEditClick,
				onSaveClick: this._onSaveClick,
				onCancelClick: this._onCancelClick,
				onDeleteClick: this._onDeleteClick
			})
		}
		_getEditTextLabel(e) {
			return e <= 1 ? "hashtags" : `hashtags (${e})`
		}
		_getEditErrorMessage(e) {
			return e <= 100 ? null : `Limit exceeded: ${e} / 100`
		}
		_getCollection(e) {
			return e.tagAssist.collections.find((e => e.id === this.props.collectionId))
		}
	}
	var fn = y().influx(((e, {
		collectionId: t,
		placement: s
	}) => ({
		tagData: e.tagAssist.collectionsTagData,
		collection: e.tagAssist.collections.find((e => e.id === t)),
		selectedTags: ie().default.getSelectedTags(s)
	})))(gn);
	Z(), k(), S(), $(), y(), T(), v(), te();
	class Cn extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onButtonClick = () => {
				T().stateProxy.hasPro({
					feature: "tagAssist"
				}) ? y().transaction((e => {
					e.tagAssist.newCollection.name = "Collection", e.tagAssist.newCollection.text = "", e.tagAssist.newCollection.showForm = !0
				})) : vt.openBilling("tag-assist")
			}, this._onNameInput = e => {
				y().transaction((t => {
					t.tagAssist.newCollection.name = e
				}))
			}, this._onTextInput = e => {
				y().transaction((t => {
					t.tagAssist.newCollection.text = e
				}))
			}, this._onSaveClick = () => {
				v().gaController.sendEvent("user", "tag-assist:collection-create"), y().transaction((e => {
					const t = e.tagAssist.newCollection;
					e.tagAssist.collections.unshift({
						id: Z().generate(),
						name: t.name,
						tags: te().extractTags(t.text),
						editing: !1,
						editName: "",
						editText: ""
					}), t.name = "", t.text = "", t.showForm = !1
				})), Ua.registerUsage(), Ua.saveCollectionsToLs(), Ua.updateCollectionsTagData()
			}, this._onCancelClick = () => {
				y().transaction((e => {
					e.tagAssist.newCollection.name = "", e.tagAssist.newCollection.text = "", e.tagAssist.newCollection.showForm = !1
				}))
			}, t
		}
		render() {
			const e = te().extractTags(this.props.newCollection.text).filter($().default).length;
			return Glamor.createElement(S().default.TagAssistNewCollection, {
				name: this.props.newCollection.name,
				text: this.props.newCollection.text,
				textLabel: this._getTextLabel(e),
				showForm: this.props.newCollection.showForm,
				errorMessage: this._getErrorMessage(e),
				onButtonClick: this._onButtonClick,
				onNameInput: this._onNameInput,
				onTextInput: this._onTextInput,
				onSaveClick: this._onSaveClick,
				onCancelClick: this._onCancelClick
			})
		}
		_getTextLabel(e) {
			return e <= 1 ? "hashtags" : `hashtags (${e})`
		}
		_getErrorMessage(e) {
			return e <= 100 ? null : `Limit exceeded: ${e} / 100`
		}
	}
	var wn = y().influx((e => ({
		newCollection: e.tagAssist.newCollection
	})))(Cn);
	class vn extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._constructCollection = e => ({
				id: e,
				element: Glamor.createElement(fn, {
					collectionId: e,
					placement: this.props.placement
				})
			}), t
		}
		render() {
			return Glamor.createElement(S().default.TagAssistPanelCollections, {
				newCollection: Glamor.createElement(wn, null),
				collections: this.props.collectionIds.map(this._constructCollection)
			})
		}
	}
	var kn = y().influx((e => ({
		collectionIds: e.tagAssist.collections.map((e => e.id))
	})))(vn);
	const Sn = ({
		props: e
	}) => ({
		width: 6,
		height: 6,
		marginLeft: k().default.space.g1,
		borderRadius: "50%",
		background: e.summaryColor
	});
	class bn extends k().default.Component {
		constructor(e) {
			super(e), this.scuIgnoreFields = ["tagData"], this._applyTagData = e => {
				this.setState({
					tagData: { ...e
					},
					tagDataKey: String(Date.now())
				})
			}, this._onTabClick = e => {
				v().gaController.sendEvent("user", `tag-assist:tab-click-${e.id}`), y().transaction((t => {
					t.tagAssist.selectedTabId = e.id
				}))
			}, this._onUpgradeClick = () => {
				v().gaController.sendEvent("user", "tag-assist:upgrade-to-pro-click"), vt.openBilling("tag-assist")
			}, this._onCloseClick = () => {
				y().transaction((e => {
					"schedule" === this.props.placement ? e.schedule.showTagAssist = !1 : e.tagAssist.shown = !1
				}))
			}, this.state = {
				tagData: {},
				tagDataKey: null
			}
		}
		componentDidMount() {
			(async() => {
				const e = await Ua.getTagData();
				this._applyTagData(e)
			})(), Ua.onTagDataUpdate(this._applyTagData)
		}
		componentWillUnmount() {
			Ua.onTagDataUpdate.off(this._applyTagData)
		}
		render() {
			return Glamor.createElement(S().default.TagAssistPanel, {
				placement: this.props.placement,
				selectedTabId: this.props.selectedTabId,
				tabs: this._getTabs(),
				proWarning: this._constructProWarning(),
				body: this._constructBody(),
				tips: this._constructTips(),
				onTabClick: this._onTabClick,
				onCloseClick: "sidebar" === this.props.placement ? null : this._onCloseClick
			})
		}
		_getTabs() {
			return [{
				id: "search",
				label: "SEARCH"
			}, {
				id: "ladder",
				label: "LADDER"
			}, {
				id: "collections",
				label: "COLLECTIONS"
			}, this.props.hasPro && "sidebar" !== this.props.placement && {
				id: "summary",
				label: Glamor.createElement(k().default.Fragment, null, "RANK", Glamor.createElement("div", {
					css: Sn(this)
				}))
			}].filter(Boolean)
		}
		_constructProWarning() {
			if (this.props.hasPro) return null;
			const e = this.props.selectedTabId;
			return "search" === e ? null : "ladder" !== e || this.props.hasLadder ? {
				text: "Upgrade to PRO to get tag metrics, ladders and hashtag collection features.",
				onUpgradeClick: this._onUpgradeClick
			} : null
		}
		_constructBody() {
			const e = this.props.selectedTabId;
			return "search" === e || "ladder" === e ? Glamor.createElement(hn, {
				tagData: this.state.tagData,
				tagDataKey: this.state.tagDataKey,
				placement: this.props.placement
			}) : "collections" === e ? Glamor.createElement(kn, {
				placement: this.props.placement
			}) : "summary" === e ? Glamor.createElement(pn, {
				tagData: this.state.tagData,
				tagDataKey: this.state.tagDataKey,
				placement: this.props.placement
			}) : null
		}
		_constructTips() {
			return "sidebar" === this.props.placement || "schedule" === this.props.placement ? null : Glamor.createElement($a, null)
		}
	}
	var yn = y().influx(((e, t) => ({
		hasPro: T().stateProxy.hasPro({
			feature: "tagAssist"
		}),
		selectedTabId: e.tagAssist.selectedTabId,
		summaryColor: ie().default.getSummaryStatus(t.placement).color,
		hasLadder: Boolean(e.tagAssist.ladder)
	})))(bn);
	k(), S(), y();
	class _n extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCloseClick = () => {
				y().transaction((e => {
					e.sidebar.isOpen = !1
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.TagAssistSidePanel, {
				title: "Tag Assistant",
				content: Glamor.createElement(yn, {
					placement: "sidebar"
				}),
				onCloseClick: this._onCloseClick
			})
		}
	}

	function En() {
		return (En = Object.assign || function (e) {
			for (var t = 1; t < arguments.length; t++) {
				var s = arguments[t];
				for (var a in s) Object.prototype.hasOwnProperty.call(s, a) && (e[a] = s[a])
			}
			return e
		}).apply(this, arguments)
	}
	k(), S(), y();
	class Tn extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCloseClick = () => {
				y().transaction((e => {
					e.tagAssist.errorCode = null
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "tag-assist-snackbar-item-mediator",
				show: !!this.props.errorCode
			}, Glamor.createElement(k().default.InfoCard, En({}, this._getTitleAndContent(), {
				icon: "warning-triangle",
				iconStyle: {
					width: 14,
					height: 16,
					color: k().default.color.error,
					position: "relative",
					top: 1
				},
				markerColor: k().default.color.error,
				onClose: this._onCloseClick
			})))
		}
		_getTitleAndContent() {
			if ("tag-fetch-failed" === this.props.errorCode) return {
				title: "Too Many Requests",
				content: "\n          Failed to fetch data for a Hashtag page.\n          Too many hashtag requests were made from your IP network recently.\n          Please give it some time (from 15 minutes to a few hours) and try again.\n        "
			}
		}
	}
	var An = y().influx((e => ({
		errorCode: e.tagAssist.errorCode
	})))(Tn);

	function Pn(...e) {
		const t = function (e, ...t) {
			let s = 0;
			return e.join("###").split(",").join("\n,\n").split("{").join("\n{").split("\n").map((e => {
				if (!e.includes("###")) return e;
				const a = ne().default(t[s]).map((t => e.split("###").join(t))).join(",\n");
				return s += 1, a
			})).join("\n")
		}(...e);
		document.head.insertAdjacentHTML("afterbegin", t)
	}
	d(), re(), ae(), x(), J(), ne(), P(), F(), U(), y(), I(), E(), f(), T(), D(), le(), ce();
	const In = {
		init: function () {
			this._initialized || (this._initialized = !0, this._initTestCommand(), this._initToggleExperimentsShortcut(), this._initGlobalVars())
		},
		_initTestCommand: function () {
			window.test = () => {
				chrome.tabs.create({
					url: "/tests.html",
					active: !0
				})
			}
		},
		_initToggleExperimentsShortcut: function () {
			document.addEventListener("keydown", (e => {
				e.ctrlKey && e.altKey && e.shiftKey && 40 === e.keyCode && y().transaction((e => {
					e.experiments.enabled = !e.experiments.enabled
				}))
			}))
		},
		_initGlobalVars: function () {
			window.env = F().env, window.chromeBus = I().chromeBus, window.iframeBus = E().iframeBus, window.tagAssistController = Ua, window.abTestingController = f().abTestingController, window.igApi = D().igApi, window.callAsync = x().default, window.createUrl = J().default, window.ls = re().default, window.safe = ae().default, window.idbController = U().idbController, window.insertMultistyle = Pn, (F().env.is.development || F().env.is.beta) && (window.model = y().model, window.transaction = y().transaction, window.stateProxy = T().stateProxy, window.scheduleProxy = le().default, window.scheduleGenerateSquarePreview = ce().default, window.setState = this.setState, Object.assign(window, P()), this.defineCommit())
		},
		setState: function (e) {
			const t = d().default.cloneDeep(e);
			T().replaceState.dispatch(t)
		},
		defineCommit: function () {
			Object.defineProperty(window, "commit", {
				get: () => (this.setState(y().model.state), null)
			})
		}
	};
	k(), S(), F(), y(), T(), E(), ue(), E(), v(), y(), Z(), de(), N(), A(), y(), T(), P(), v(), he(), I(), E(), C(), le(), me(), ce(), pe(), N(), y(), E(), le();
	var Fn = {
		init: function () {
			E().iframeBus.on("schedule.fcs-date-dialog-toggled", (async e => {
				y().transaction((t => {
					t.schedule.dateDialog.isOpen = e
				}));
				const t = await E().iframeBus.send("schedule.fcs-date-dialog-get-timezone");
				y().transaction((e => {
					e.schedule.dateDialog.timezone = t
				}))
			})), E().iframeBus.on("schedule.fcs-date-dialog-invalid-time", (() => {
				y().transaction((e => {
					e.schedule.dateDialog.isTimeError = !0
				}))
			}))
		},
		actualizeDataForIframe: function () {
			const e = y().model.state.schedule.dateDialog,
				t = le().default.getDateDialogSelectedDay();
			let s;
			s = e.selectedSlotTime ? N().default(t).getTime() + e.selectedSlotTime : e.customTime ? N().default(t).getTime() + e.customTime : null;
			E().iframeBus.send("schedule.fcs-date-dialog-set-publish-time", s), E().iframeBus.send("schedule.fcs-date-dialog-set-selected-option", e.selectedOption)
		}
	};
	Z(), C(), y();
	var xn = {
		addTask: function ({
			type: e,
			params: t,
			run: s
		}) {
			const a = Z().generate();
			return y().transaction((s => {
				s.schedule.tasks.push({
					id: a,
					type: e,
					params: t,
					doneAt: null,
					failed: !1
				})
			})), Gn.push({
				id: a,
				run: s
			}), On(), a
		}
	};
	const Gn = [];
	let Dn = !1;
	async function On() {
		if (Dn) return;
		if (0 === Gn.length) return;
		Dn = !0;
		const e = Gn.shift();
		try {
			await e.run()
		} catch (t) {
			C().sentryController.sendError("schedule-task-controller: failed to run task", "error", {
				task: e,
				details: t
			}, {
				actor: "schedule"
			}), y().transaction((t => {
				t.schedule.fcsError = "Failed to accept scheduling changes.", t.schedule.isErrorShown = !0;
				const s = t.schedule.tasks.find((t => t.id === e.id));
				s && (s.failed = !0)
			}))
		}
		y().transaction((t => {
			const s = t.schedule.tasks.find((t => t.id === e.id));
			s && (s.doneAt = Date.now())
		})), Dn = !1, On()
	}
	Z(), ae(), v(), U(), y(), me(), ce(), le();
	const Bn = {
		mimeTypes: ["image/jpeg", "image/png", "video/quicktime", "video/mp4", "video/webm"],
		maxPostCount: 150,
		maxFileSize: 524288e3,
		maxFileSizeStr: "500MB",
		minVideoDurationSec: 3,
		maxVideoDurationSec: 60,
		minVideoRatio: .8,
		maxVideoRatio: 1.778
	};
	var Un = {
		init: function () {
			! function () {
				const e = {};
				let t = !1,
					s = !1;
				y().model.observe((e => e.schedule.posts.filter((e => "local" === e.source)).map((e => e.id)).join("-")), (async function a() {
					if (t) return void(s = !0);
					t = !0;
					const n = y().model.state.schedule.posts.filter((e => "local" === e.source));
					for (const t of n) {
						if (e[t.id]) continue;
						const s = `schedule.local-post:${t.id}`,
							a = await ae().default((() => U().idbController.get(s)));
						if (!a) continue;
						const n = URL.createObjectURL(a.previewBlob);
						e[t.id] = n
					}
					y().transaction((t => {
						for (const s of t.schedule.posts) "local" === s.source && (s.image = e[s.id], s.preview = e[s.id])
					})), t = !1, s && (s = !1, a())
				}))
			}()
		},
		config: Bn,
		addFiles: async function e({
			files: t,
			isCarousel: s,
			postMode: a = null
		}) {
			const n = e;
			if (n.running) return;
			n.running = !0, y().transaction((e => {
				e.schedule.loading = !0
			}));
			let o = y().model.state.schedule.posts.filter((e => "local" === e.source)).length;
			const i = await Promise.all(t.map((async e => {
				if (o += 1, o > Bn.maxPostCount) return {
					file: e,
					error: "post-count-limit-reached"
				};
				if (e.size > Bn.maxFileSize) return {
					file: e,
					error: "file-size-limit-reached"
				};
				if (!Bn.mimeTypes.includes(e.type)) return {
					file: e,
					error: "unsupported-type"
				};
				if (!e.type.startsWith("video/")) return {
					file: e,
					error: null
				};
				let t;
				try {
					t = await new Promise(((t, s) => {
						const a = document.createElement("video");
						a.src = URL.createObjectURL(e), a.preload = "metadata", a.addEventListener("loadedmetadata", (() => {
							URL.revokeObjectURL(a.src), t({
								duration: a.duration,
								width: a.videoWidth,
								height: a.videoHeight
							})
						})), a.addEventListener("error", (() => {
							s(a.error)
						}))
					}))
				} catch (t) {
					return console.error("failed to load metadata", {
						file: e,
						details: t
					}), {
						file: e,
						error: "unknown"
					}
				}
				if (0 === t.width && 0 === t.height) return {
					file: e,
					error: "unsupported-codec"
				};
				if (t.duration < Bn.minVideoDurationSec || t.duration > Bn.maxVideoDurationSec) return {
					file: e,
					error: "unsupported-duration"
				};
				const s = t.width / t.height;
				return s < Bn.minVideoRatio || s > Bn.maxVideoRatio ? {
					file: e,
					error: "unsupported-ratio"
				} : {
					file: e,
					error: null
				}
			})));
			y().transaction((e => {
				e.schedule.bulkUploadErrors = i.filter((e => !!e.error)).map((e => ({
					type: e.error,
					filename: e.file.name
				})))
			}));
			const r = i.filter((e => !e.error)).map((e => e.file));
			if (0 === r.length) return y().transaction((e => {
				e.schedule.loading = !1
			})), void(n.running = !1);
			let l;
			l = s ? [{
				id: Z().generate(),
				files: [...r],
				preview: await ce().default(r[0])
			}] : await Promise.all(r.map((async e => ({
				id: Z().generate(),
				files: [e],
				preview: await ce().default(e)
			}))));
			for (const e of l) try {
				await Ln(e.id, {
					id: e.id,
					files: e.files,
					previewBlob: e.preview.blob
				})
			} catch (t) {
				y().transaction((t => {
					e.files.map((e => {
						t.schedule.bulkUploadErrors.push({
							type: "unknown",
							filename: e.name
						})
					}))
				}))
			}
			const c = Date.now();
			y().transaction((e => {
				l.reverse().forEach(((t, s) => {
					let a;
					a = t.files.length > 1 ? "carousel" : t.files[0].type.startsWith("video/") ? "video" : "photo", e.schedule.posts.push(me().default({
						id: t.id,
						source: "local",
						type: a,
						status: "draft",
						image: null,
						preview: null,
						imageAvgColor: t.preview.averageColor,
						on: null,
						createdOn: Date.now(),
						stats: null,
						crosspostToFb: null,
						saveStatus: null,
						draftOrder: c + s
					}))
				}))
			})), y().transaction((e => {
				e.schedule.loading = !1
			})), n.running = !1, v().gaController.sendEvent("user", "bulk:add-files", `${s?"carousel":"posts"}-${t.length}`);
			const u = l.map((e => e.id));
			if (1 === u.length && (s || 1 === t.length)) {
				const e = le().default.getPosts().find((e => e.id === u[0]));
				e && Rn.openPost(e, {
					postMode: a
				})
			}
			return u
		},
		selectFiles: function (e) {
			if (!y().model.state.authStatus.userId) return;
			const t = document.createElement("input");
			t.setAttribute("type", "file"), t.setAttribute("multiple", !0), t.setAttribute("accept", Bn.mimeTypes.join(", ")), t.addEventListener("change", (async t => {
				t.preventDefault();
				const s = Array.from(t.target.files);
				e && e(s)
			}), {
				once: !0
			}), t.click()
		},
		getPostFiles: async function (e) {
			const t = await ae().default((() => async function (e) {
				const t = `schedule.local-post:${e}`;
				return await U().idbController.get(t) || null
			}(e)));
			if (!t) return [];
			return t.files || []
		}
	};
	async function Ln(e, t) {
		const s = `schedule.local-post:${e}`;
		await U().idbController.set(s, t)
	}
	var Rn = {
		init: function () {
			E().iframeBus.on("schedule.get-ig-username", Hn), E().iframeBus.on("schedule.show-upsell", Zn), E().iframeBus.on("schedule.is-fallback-enabled", eo), E().iframeBus.on("schedule.has-pro", $n), E().iframeBus.on("schedule.get-post", Wn), E().iframeBus.on("schedule.is-creating-post", qn), E().iframeBus.on("schedule.delete-post", jn), E().iframeBus.on("schedule.apply-fcs-posts", Yn), E().iframeBus.on("schedule.fcs-connection-status", Kn), E().iframeBus.on("schedule.fcs-notification-error-appeared", Jn), E().iframeBus.on("schedule.fcs-critical-error", Qn), E().iframeBus.on("schedule.fcs-error", Xn), Un.init(), Fn.init(), window.addEventListener("beforeunload", (e => {
					const t = y().model.state,
						s = t.schedule.posts.some((e => "saving" === e.saveStatus)),
						a = t.schedule.posts.some((e => "deleting" === e.saveStatus));
					(to() || s || a) && (e.returnValue = "")
				})), y().model.observe((e => e.authStatus.username), (e => {
					e && I().chromeBus.send("schedule.update-ig-posts")
				})),
				function () {
					const e = 15 * P().MINUTE;
					let t, s;
					y().model.observe((e => e.schedule.lastIgPostsUpdateOn), (s => {
						if (!s) return;
						const a = s + e - Date.now();
						clearTimeout(t), t = setTimeout((() => {
							I().chromeBus.send("schedule.update-ig-posts")
						}), a)
					})), y().model.observe((e => e.schedule.lastFcsPostsUpdateOn), (t => {
						if (!t) return;
						const a = t + e - Date.now();
						clearTimeout(s), s = setTimeout((() => {
							E().iframeBus.send("schedule.fcs-refresh-data")
						}), a)
					}))
				}(), y().model.observe((() => T().stateProxy.hasPro()), (e => {
					e && y().transaction((e => {
						e.schedule.isUpsellShown = !1
					}))
				})),
				function () {
					const e = {};
					E().iframeBus.on("schedule.fcs-edit-post-request", (({
						postId: t,
						status: s,
						on: a
					}) => {
						const n = y().model.state.schedule.posts.find((e => e.id === t));
						n && (v().gaController.sendEvent("user", "schedule:save-post-click"), e[t] = Date.now(), "draft" === n.status && "draft" !== s && function (e) {
							T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:schedule");
							y().transaction((t => {
								t.billing.trial.schedule += e
							}))
						}(1), y().transaction((e => {
							const n = e.schedule.posts.find((e => e.id === t));
							"posted" === n.status || (n.on = "posted" === s ? Date.now() : a), n.status = s, n.saveStatus = "saving", e.schedule.navigation.isOpen = !1, "scheduled" === s && (e.schedule.recentScheduledOn = a)
						})))
					})), E().iframeBus.on("schedule.fcs-edit-post-response", (async({
						postId: t
					}) => {
						if (!y().model.state.schedule.posts.find((e => e.id === t))) return;
						const s = Date.now() - e[t],
							a = Math.max(1 * P().SECOND - s, 0);
						await A().default(a), delete e[t], y().transaction((e => {
							const s = e.schedule.posts.find((e => e.id === t));
							"posted" === s.status ? (s.saveStatus = "syncing", s.on = Date.now(), s.createdOn = Date.now(), E().iframeBus.send("schedule.fcs-refresh-data")) : s.saveStatus = null
						}))
					}))
				}(),
				function () {
					const e = {},
						t = {};
					E().iframeBus.on("schedule.fcs-create-post-request", (async({
						type: s,
						image: a,
						crosspostToFb: n,
						localPostId: o
					}) => {
						v().gaController.sendEvent("user", "schedule:save-post-click"), t[a] = Date.now(), y().transaction((t => {
							let i, r, l;
							t.schedule.navigation.isOpen = !1;
							const c = Date.now(),
								u = t.schedule.dateDialog;
							if ("publish-now" === u.selectedOption) i = c, r = "posted", l = c;
							else if ("save-as-draft" === u.selectedOption) i = null, r = "draft", l = c;
							else if ("schedule" === u.selectedOption) {
								const e = u.customTime || u.selectedSlotTime;
								i = u.selectedDay + e, r = "scheduled", l = i, t.schedule.recentScheduledOn = i
							}
							if (o) {
								const s = t.schedule.posts.find((e => e.id === o));
								if (!s) return;
								e[a] = s.id, s.status = r, s.image = a, s.on = i, s.createdOn = c, s.stats = {
									likes: 0,
									comments: 0
								}, s.crosspostToFb = n, s.saveStatus = "saving", "draft" !== r && (s.draftOrder = l)
							} else {
								const o = Z().generate();
								e[a] = o, t.schedule.posts.unshift({
									id: o,
									source: "fcs",
									type: s,
									status: r,
									image: a,
									preview: null,
									imageAvgColor: null,
									on: i,
									createdOn: c,
									stats: {
										likes: 0,
										comments: 0
									},
									crosspostToFb: n,
									saveStatus: "saving",
									draftOrder: l
								})
							}
						}))
					})), E().iframeBus.on("schedule.fcs-create-post-response", (async({
						image: s
					}) => {
						const a = y().model.state,
							n = e[s];
						if (!a.schedule.posts.find((e => e.id === n))) return console.error("post was not found"), void E().iframeBus.send("schedule.fcs-refresh-data");
						const o = Date.now() - t[s],
							i = Math.max(1 * P().SECOND - o, 0);
						await A().default(i), delete t[s], delete e[s], y().transaction((e => {
							e.schedule.posts.find((e => e.id === n)).saveStatus = "syncing"
						})), E().iframeBus.send("schedule.fcs-refresh-data")
					}))
				}()
		},
		isSaving: function () {
			return "saving" === le().default.getSavingState()
		},
		addFiles: Un.addFiles,
		selectFiles: function (e) {
			if (to()) return void Vn();
			Un.selectFiles(e)
		},
		openTab: function (e) {
			const t = y().model.state;
			if (t.schedule.fcsSetup.checking) return;
			if (!t.schedule.fcsSetup.connected) return void no();
			y().transaction((t => {
				t.schedule.navigation.isOpen = !0, t.schedule.navigation.selectedTabId = e
			}))
		},
		openNewPost: zn,
		openPost: async function (e, {
			postMode: t = null,
			isRetry: s = !1
		} = {}) {
			if (ao()) return void no();
			if (to()) return void Vn();
			const a = le().default.getRecentScheduledOn();
			if (y().transaction((e => {
					e.schedule.dateDialog.isOpen = !1
				})), "local" === e.source) return zn({
				postMode: t || (e.on ? "schedule" : "draft"),
				localPostId: e.id,
				localPostFiles: await Un.getPostFiles(e.id)
			}), void(e.on ? (so({
				on: e.on
			}), Fn.actualizeDataForIframe()) : so({
				on: a,
				takeNextTimeSlot: !0
			}));
			y().transaction((e => {
				e.schedule.navigation.isOpen = !0, e.schedule.navigation.selectedTabId = null, e.schedule.navigation.isFcsLoading = !0, e.schedule.navigation.withBackToCalendarButton = !1
			})), "scheduled" === e.status ? (y().transaction((e => {
				e.schedule.dateDialog.selectedOption = "schedule"
			})), so({
				on: e.on
			})) : "draft" === e.status && (y().transaction((e => {
				e.schedule.dateDialog.selectedOption = "save-as-draft"
			})), so({
				on: a,
				takeNextTimeSlot: !0
			}));
			Fn.actualizeDataForIframe(), await A().default(200), await E().iframeBus.send("schedule.fcs-open-post", e.id), y().transaction((e => {
				e.schedule.navigation.isFcsLoading = !1
			}))
		},
		addPostChanges: function (e) {
			y().transaction((e => {
				e.schedule.hasUncommitedChanges || "calendar" === e.schedule.navigation.selectedTabId || "time-slots" === e.schedule.navigation.selectedTabId || (e.schedule.navigation.isOpen = !1, e.schedule.navigation.selectedTabId = null), e.schedule.hasUncommitedChanges = !0
			}));
			const t = xn.addTask({
				type: "update-posts",
				params: {
					updates: e
				},
				run: function () {}
			});
			Mn.push(t), Object.assign(Nn, e)
		},
		commitPostChanges: function () {
			const e = { ...Nn
				},
				t = [...Mn];
			Nn = {}, Mn = [], y().transaction((e => {
				e.schedule.tasks = e.schedule.tasks.filter((e => {
					return s = e, !t.includes(s.id);
					var s
				})), e.schedule.hasUncommitedChanges = !1
			}));
			const s = xn.addTask({
				type: "update-posts",
				params: {
					updates: e,
					final: !0
				},
				run: async function () {
					const t = y().model.state,
						a = Date.now();
					for (const a in e) {
						const n = e[a];
						let o = t.schedule.posts.find((e => "fcs" === e.source && e.id === a)),
							i = !1;
						o || (o = t.schedule.posts.find((e => "local" === e.source && e.id === a)), i = !0);
						const r = n.on || o.on,
							l = n.status || o.status,
							c = n.draftOrder || o.draftOrder;
						if (i) y().transaction((e => {
							const t = e.schedule.posts.find((e => "local" === e.source && e.id === a));
							t.on = r, t.draftOrder = c
						}));
						else {
							let e;
							"draft" === l ? ("draft" !== o.status && (e = await I().chromeBus.send("fb-api.fcs-save-as-draft", a)), y().transaction((e => {
								const t = e.schedule.posts.find((e => "fcs" === e.source && e.id === a));
								t.status = l, t.draftOrder = c
							}))) : "scheduled" === l && (e = await I().chromeBus.send("fb-api.fcs-save-as-scheduled", a, r)), e && e.error && (C().sentryController.sendError("schedule-controller-popup: failed to update post data", "error", {
								details: e.error
							}, {
								actor: "schedule"
							}), y().transaction((e => {
								const t = e.schedule.tasks.find((e => e.id === s));
								t && delete t.params.updates[a]
							})))
						}
					}
					const n = 1 * P().SECOND,
						o = Date.now() - a;
					o < n && await A().default(n - o), E().iframeBus.send("schedule.fcs-refresh-data"), he().eventBus.once("schedule.fcs-posts-applied", (() => {
						y().transaction((e => {
							e.schedule.tasks = e.schedule.tasks.filter((e => !e.params.final))
						}))
					}))
				}
			})
		},
		cancelPostChanges: function () {
			y().transaction((e => {
				e.schedule.tasks = e.schedule.tasks.filter((e => {
					return t = e, !Mn.includes(t.id);
					var t
				})), e.schedule.hasUncommitedChanges = !1
			})), Mn = [], Nn = {}
		},
		closeBodyPanel: function () {
			y().transaction((e => {
				e.schedule.navigation.isOpen = !1
			})), setTimeout((() => {
				y().transaction((e => {
					e.schedule.navigation.selectedTabId = null
				}))
			}), 200)
		},
		blinkAddCardAttention: Vn,
		hasDndTasks: to
	};
	let Mn = [],
		Nn = {};
	async function zn({
		postMode: e = "publish",
		localPostId: t = null,
		localPostFiles: s = []
	} = {}) {
		const a = y().model.state;
		t && a.schedule.navigation.selectedPostId === t || (ao() ? no() : to() ? Vn() : (y().transaction((t => {
			t.schedule.navigation.isOpen = !0, t.schedule.navigation.isFcsLoading = !0, t.schedule.navigation.selectedTabId = "post", t.schedule.navigation.withBackToCalendarButton = !1, t.schedule.dateDialog.isOpen = !1, t.schedule.dateDialog = pe().default(t), t.schedule.dateDialog.selectedOption = {
				publish: "publish-now",
				draft: "save-as-draft",
				schedule: "schedule"
			}[e]
		})), so({
			on: le().default.getRecentScheduledOn(),
			takeNextTimeSlot: !0
		}), Fn.actualizeDataForIframe(), await E().iframeBus.send("schedule.fcs-open-new-post-form", {
			postMode: e,
			localPostId: t,
			localPostFiles: s
		}), y().transaction((e => {
			e.schedule.navigation.isFcsLoading = !1
		}))))
	}

	function Vn() {
		y().transaction((e => {
			e.schedule.gridAddCardAttention = !0
		})), setTimeout((() => {
			y().transaction((e => {
				e.schedule.gridAddCardAttention = !1
			}))
		}), 600)
	}

	function Hn() {
		return y().model.state.authStatus.username
	}

	function $n() {
		return T().stateProxy.hasPro({
			feature: "schedule"
		})
	}

	function Wn(e) {
		return y().model.state.schedule.posts.find((t => t.id === e))
	}

	function qn() {
		return "post" === y().model.state.schedule.navigation.selectedTabId
	}
	async function jn(e) {
		const t = y().model.state.schedule.posts.find((t => t.id === e));
		if (!t) return;
		v().gaController.sendEvent("user", "schedule:delete-post-click"), y().transaction((t => {
			t.schedule.navigation.isOpen = !1;
			t.schedule.posts.find((t => t.id === e)).saveStatus = "deleting"
		}));
		let s = null;
		if ("local" === t.source) await A().default(1 * P().SECOND);
		else {
			const t = await I().chromeBus.send("fb-api.fcs-delete-post", e);
			s = t.error || null
		}
		y().transaction((t => {
			if (!s) return void(t.schedule.posts = t.schedule.posts.filter((t => t.id !== e)));
			console.error("failed to delete post", s), t.schedule.fcsError = "Failed to delete post", t.schedule.isErrorShown = !0;
			t.schedule.posts.find((t => t.id === e)).saveStatus = null
		}))
	}
	async function Yn(e) {
		const t = Yn;
		let s = e.map((e => {
			const t = {
				IG_IMAGE: "photo",
				IG_VIDEO: "video",
				IG_CAROUSEL: "carousel"
			}[e.postType];
			if (!t) return null;
			const s = {
				DRAFT: "draft",
				POSTED: "posted",
				SCHEDULED: "scheduled"
			}[e.postStatus];
			let a = null;
			"draft" !== s && (a = 1e3 * e.scheduledOrLastAddedTimestamp);
			let n = null;
			"posted" === s && (n = {
				likes: e.likeCount,
				comments: e.commentCount
			});
			const o = y().model.state.schedule.posts.find((t => t.id === e.id));
			return me().default({
				id: e.id,
				source: "fcs",
				type: t,
				status: s,
				image: e.thumbnailSrc,
				preview: (null == o ? void 0 : o.preview) || null,
				imageAvgColor: (null == o ? void 0 : o.imageAvgColor) || null,
				on: a,
				createdOn: 1e3 * e.lastAddedTime,
				stats: n,
				crosspostToFb: (null == o ? void 0 : o.crosspostToFb) || !1,
				saveStatus: (null == o ? void 0 : o.saveStatus) || null,
				draftOrder: (null == o ? void 0 : o.draftOrder) || 1e3 * e.lastAddedTime
			})
		})).filter(Boolean);
		s = s.filter((e => !y().model.state.schedule.posts.find((t => "syncing" === t.saveStatus && t.id === e.id))));
		const a = []; {
			const e = y().model.state;
			for (const t of s) {
				if (!!e.schedule.posts.find((e => e.id === t.id))) continue;
				let s;
				const o = e.schedule.posts.filter((e => "syncing" === e.saveStatus && e.type === t.type && e.status === t.status));
				if ("photo" !== t.type) s = o[0] || null;
				else {
					var n;
					const e = [];
					for (const s of o) {
						const [a, n, o] = await oo(t.image), [i, r, l] = await oo(s.image);
						e.push({
							post: s,
							rgbDistance: Math.abs(a - i) + Math.abs(n - r) + Math.abs(o - l)
						})
					}
					e.sort(((e, t) => e.rgbDistance - t.rgbDistance)), s = (null === (n = e[0]) || void 0 === n ? void 0 : n.post) || null
				}
				s && (t.preview = s.preview, t.imageAvgColor = s.imageAvgColor, t.draftOrder = s.draftOrder, t.crosspostToFb = s.crosspostToFb, a.push(s.id))
			}
		}
		y().transaction((e => {
			e.schedule.loading = !1, e.schedule.lastFcsPostsUpdateOn = Date.now(), e.schedule.posts = e.schedule.posts.filter((e => !a.includes(e.id))).filter((e => "ig" !== e.source)).filter((e => "fcs" !== e.source || "syncing" === e.saveStatus)).concat(s)
		}));
		const o = y().model.state.schedule.posts.some((e => "syncing" === e.saveStatus));
		t.refreshCount = t.refreshCount || 0, t.refreshDelay = t.refreshDelay || 3 * P().SECOND, o && t.refreshCount < 4 ? (clearTimeout(t.refreshTimeout), t.refreshTimeout = setTimeout((() => {
			t.refreshCount += 1, t.refreshDelay *= 1.5, E().iframeBus.send("schedule.fcs-refresh-data")
		}), t.refreshDelay)) : (t.refreshCount = null, t.refreshDelay = null, t.refreshTimeout = null), y().transaction((e => {
			const t = [...e.schedule.posts];
			for (const s of t) {
				if ("syncing" !== s.saveStatus) continue;
				Date.now() - s.createdOn > 1 * P().HOUR && (e.schedule.fcsError = "Could not save or sync your post(s) to Instagram API. Instagram API is not responsive.", e.schedule.isErrorShown = !0, e.schedule.posts = e.schedule.posts.filter((e => e.id !== s.id)))
			}
		})), he().eventBus.send("schedule.fcs-posts-applied")
	}

	function Kn(e) {
		y().transaction((t => {
			t.schedule.fcsSetup.checking = !1, t.schedule.fcsSetup.connected = e, t.schedule.fcsSetup.showPanel = !1, e || I().chromeBus.send("schedule.update-ig-posts")
		}))
	}

	function Zn() {
		y().transaction((e => {
			e.schedule.isUpsellShown = !0
		}))
	}

	function Jn(e) {
		y().transaction((t => {
			t.schedule.fcsError = e, t.schedule.isErrorShown = !0, "post" === t.schedule.navigation.selectedTabId && (t.schedule.navigation.isOpen = !1, t.schedule.navigation.selectedPostId = null), "post" === t.schedule.navigation.selectedTabId && (t.schedule.navigation.isOpen = !1, t.schedule.navigation.selectedPostId = null)
		}))
	}

	function Xn({
		message: e,
		details: t = {},
		critical: s = !1
	}) {
		const a = Xn;
		t.isNetworkOk && C().sentryController.sendError(e, "error", {
			details: t
		}, {
			actor: "schedule"
		}), s && (a.init || (a.init = !0, a.criticalCount = 0), t.isNetworkOk && (a.criticalCount += 1), y().transaction((e => {
			e.schedule.fcsFailed = !0, e.schedule.isErrorShown = !0, e.schedule.fallback.hideSwitchToFallbackButton = !t.isNetworkOk || a.criticalCount < 2
		})))
	}

	function Qn({
		message: e,
		isNetworkOk: t
	}) {
		const s = Qn;
		s.init || (s.init = !0, s.count = 0), t && (v().gaController.sendEvent("user", "schedule:fcs-failed", e), s.count += 1), y().transaction((e => {
			e.schedule.fcsFailed = !0, e.schedule.isErrorShown = !0, "post" === e.schedule.navigation.selectedTabId && (e.schedule.navigation.isOpen = !1, e.schedule.navigation.selectedPostId = null), e.schedule.fallback.hideSwitchToFallbackButton = !t || s.count < 3
		}))
	}

	function eo() {
		return y().model.state.schedule.fallback.isEnabled
	}

	function to() {
		const e = y().model.state;
		return e.schedule.tasks.filter((e => !e.doneAt)).length > 0 || e.schedule.hasUncommitedChanges
	}

	function so({
		on: e,
		takeNextTimeSlot: t = !1
	}) {
		y().transaction((s => {
			const a = s.schedule.dateDialog;
			let n = N().default(e).getTime(),
				o = e - n;
			if (t) {
				const e = s.schedule.timeSlots;
				if (0 === e.length) a.periodStart = n, a.selectedDay = n, a.customTime = o, a.selectedSlotTime = null;
				else
					for (;;) {
						const t = e.find((e => e.time > o));
						t ? (a.periodStart = n, a.selectedDay = n, a.customTime = t.time, a.selectedSlotTime = t.time) : (a.periodStart = de().default(n, 1).getTime(), a.selectedDay = de().default(n, 1).getTime(), a.customTime = e[0].time, a.selectedSlotTime = e[0].time);
						const i = a.selectedDay + a.selectedSlotTime;
						if (!s.schedule.posts.find((e => "fcs" === e.source && e.on === i))) break;
						n = a.selectedDay, o = a.selectedSlotTime
					}
			} else a.periodStart = n, a.selectedDay = n, a.selectedSlotTime = null, a.customTime = o
		}))
	}

	function ao() {
		return !y().model.state.schedule.fcsSetup.connected
	}

	function no() {
		y().transaction((e => {
			e.schedule.fcsSetup.errorCode = null, e.schedule.fcsSetup.showPanel = !0
		}))
	}
	async function oo(e) {
		const t = oo;
		if (t.cache || (t.cache = {}), !t.cache[e]) {
			const s = await fetch(e),
				a = await s.blob(),
				{
					averageColor: n
				} = await ce().default(a),
				[o, i, r] = n.replace("rgb(", "").replace(")", "").split(", ").map(Number);
			t.cache[e] = [o, i, r], setTimeout((() => {
				delete t.cache[e]
			}), 10 * P().MINUTE)
		}
		return t.cache[e]
	}
	k(), S(), v(), y(), E(), I(), le(), k(), S(), y(), P(), E(), I(), le(), ge(), fe(), Ce(), we(), P(), k(), S(), ae(), y(), le();
	const io = {
		mediator1: null,
		mediator2: null,
		isDraggingGridPost: !1
	};
	class ro extends k().default.Component {
		constructor(e) {
			super(e), this._onClick = () => {
				const e = this.state.dragPost || this.props.post;
				Rn.openPost(e)
			}, this._onDragStart = () => {
				"saving" !== this.props.saveState ? (io.mediator1 = this, io.mediator2 = this, io.isDraggingGridPost = !0) : Rn.blinkAddCardAttention()
			}, this._onDragEnd = () => {
				io.isDraggingGridPost = !1;
				const e = ae().default((() => io.mediator1.props.post), null),
					t = ae().default((() => io.mediator2.props.post), null);
				if (io.mediator1 && io.mediator1.setState({
						dragPost: null
					}), io.mediator2 && io.mediator2.setState({
						dragPost: null
					}), !e || !t) return;
				if (e.id === t.id) return;
				let s = 0;
				if (e.draftOrder === t.draftOrder) {
					const a = e.index,
						n = t.index;
					s = Math.sign(a - n)
				}
				Rn.addPostChanges({
					[e.id]: {
						on: t.on,
						draftOrder: t.draftOrder + s,
						status: "local" === e.source ? "draft" : "local" === t.source && t.on ? "scheduled" : t.status
					},
					[t.id]: {
						on: e.on,
						draftOrder: e.draftOrder,
						status: "local" === t.source ? "draft" : "local" === e.source && e.on ? "scheduled" : e.status
					}
				})
			}, this._onDragEnter = () => {
				if (!io.isDraggingGridPost) return;
				"posted" !== this.props.post.status && (io.mediator2.setState({
					dragPost: null
				}), io.mediator2 = this, io.mediator1.setState({
					dragPost: io.mediator2.props.post
				}), io.mediator2.setState({
					dragPost: io.mediator1.props.post
				}))
			}, this._getDateText = e => {
				const t = new Date(e),
					s = ge().default(t, "HH:mm");
				let a;
				return a = fe().default(t) ? "Today" : Ce().default(t) ? "Tomorrow" : we().default(t) ? ge().default(t, "d MMM") : ge().default(t, "d MMM yyyy"), `${a}, ${s}`
			}, this.state = {
				dragPost: null
			}
		}
		render() {
			const e = this.state.dragPost || this.props.post;
			return Glamor.createElement(S().default.SchedulePostNew, {
				image: e.preview,
				backgroundColor: e.imageAvgColor,
				type: e.type,
				local: "local" === e.source && "syncing" !== e.saveStatus,
				blur: this._isBlur(),
				grayscale: this._isGrayscale(),
				index: e.index,
				canClick: this._canClick(),
				canDrag: this._canDrag(),
				showFooter: this.props.showInfo,
				info: this._getInfo(),
				stats: e.stats,
				onClick: this._onClick,
				onDragStart: this._onDragStart,
				onDragEnter: this._onDragEnter,
				onDragEnd: this._onDragEnd
			})
		}
		_isBlur() {
			const e = this.state.dragPost || this.props.post;
			return "saving" === e.saveStatus || "syncing" === e.saveStatus || "deleting" === e.saveStatus
		}
		_isGrayscale() {
			const e = this.state.dragPost || this.props.post;
			return "saving" === e.saveStatus || "deleting" === e.saveStatus
		}
		_canClick() {
			const e = this.state.dragPost || this.props.post;
			return "saving" !== e.saveStatus && ("syncing" !== e.saveStatus && "deleting" !== e.saveStatus)
		}
		_canDrag() {
			const e = this.state.dragPost || this.props.post;
			return !e.saveStatus && "posted" !== e.status
		}
		_getInfo() {
			const e = this.state.dragPost || this.props.post;
			return "saving" === e.saveStatus ? {
				text: "posted" === e.status ? "PUBLISHING..." : "SAVING...",
				color: k().default.color.bgLight1
			} : "syncing" === e.saveStatus ? {
				text: "SYNCING...",
				color: k().default.color.bgLight1
			} : "deleting" === e.saveStatus ? {
				text: "DELETING...",
				color: k().default.color.bgLight1
			} : "failed" === e.saveStatus ? {
				text: "Failed to save",
				color: k().default.color.error
			} : this._isFailedToPost() ? {
				text: "Failed to post",
				color: k().default.color.error
			} : "posted" === e.status ? null : "local" === e.source ? this.props.post.on ? {
				text: this._getDateText(this.props.post.on),
				color: k().default.color.attention
			} : null : this.props.post.on ? this.props.post.on ? {
				text: this._getDateText(this.props.post.on),
				color: k().default.color.positive
			} : null : {
				text: "Draft",
				color: k().default.color.positive
			}
		}
		_isFailedToPost() {
			const e = Date.now(),
				t = this.state.dragPost || this.props.post,
				s = t.on + 5 * P().MINUTE,
				a = "scheduled" === t.status,
				n = e > s;
			return this.props.lastFcsPostsUpdateOn > s && a && n
		}
	}
	var lo = y().influx((e => ({
		saveState: le().default.getSavingState(),
		showInfo: e.schedule.filters.showInfo,
		lastFcsPostsUpdateOn: e.schedule.lastFcsPostsUpdateOn
	})))(ro);
	class co extends k().default.Component {
		constructor(e) {
			super(e), this._MAX_IG_POSTS_TO_SHOW = 60, this._onSendReportClick = () => {
				this.setState({
					isReportSent: !0
				}), I().chromeBus.send("overseer.send-report", {
					key: "user"
				})
			}, this._onReloadPageClick = () => {
				location.reload()
			}, this._loadingTimer = null, this._fcsAutorefreshed = !1, this.state = {
				isLongLoading: !1,
				isReportSent: !1
			}, e.loading && !e.isFcsConnected && this._setLoadingTimer()
		}
		componentDidUpdate(e) {
			const t = e.loading && !e.isFcsConnected,
				s = this.props.loading && !this.props.isFcsConnected;
			!t && s ? this._setLoadingTimer() : t && !s && (clearTimeout(this._loadingTimer), this.setState({
				isLongLoading: !1
			}))
		}
		render() {
			return Glamor.createElement(S().default.ScheduleGrid, {
				posts: this._getPosts(),
				animatePostsOrder: !this.props.hasDndTasks,
				overlay: this._getOverlay(),
				message: this._getMessage()
			})
		}
		_getPosts() {
			let e = 0;
			return this.props.posts.map((t => "ig" === t.source && (e += 1, e > this._MAX_IG_POSTS_TO_SHOW) ? null : {
				id: t.id,
				element: Glamor.createElement(lo, {
					post: t
				})
			})).filter(Boolean)
		}
		_getOverlay() {
			return {
				shown: this.props.loading,
				icon: "spinner",
				...this.state.isLongLoading && {
					text: Glamor.createElement(k().default.Fragment, null, "Oops... Establishing connection takes longer that usual. Please try again later or send us a report."),
					buttons: [{
						id: "send-report",
						element: Glamor.createElement("div", {
							css: [k().default.row, k().default.alignItems.center]
						}, Glamor.createElement(k().default.LinkButton, {
							label: "SEND REPORT",
							onClick: this._onSendReportClick
						}), Glamor.createElement(k().default.Spacer, {
							width: "g1"
						}), Glamor.createElement(k().default.InfoCircle, {
							tooltip: {
								text: Glamor.createElement(k().default.Fragment, null, "Clicking this button will send an internal technical data on this connectivity error to the development team for research. This data is not shared with 3rd parties.")
							}
						}))
					}, {
						id: "reload-page",
						element: Glamor.createElement(k().default.LinkButton, {
							cancel: !0,
							label: "RELOAD PAGE",
							onClick: this._onReloadPageClick
						})
					}]
				},
				...this.state.isReportSent && {
					icon: "success",
					text: Glamor.createElement(k().default.Fragment, null, "Thank you! The bug report has been sent.", Glamor.createElement("br", null), "We will check what could be wrong and try", Glamor.createElement("br", null), "to fix that in the future version."),
					buttons: [{
						id: "reload-page",
						element: Glamor.createElement(k().default.LinkButton, {
							label: "RELOAD PAGE",
							onClick: this._onReloadPageClick
						})
					}]
				}
			}
		}
		_getMessage() {
			return this.props.posts.filter((e => "ig" === e.source)).length > this._MAX_POSTS_TO_SHOW ? "Older posts are omitted" : 0 === this.props.posts.length ? "No posts to show" : null
		}
		_setLoadingTimer() {
			this._loadingTimer = setTimeout((() => {
				if (!this._fcsAutorefreshed) return this._fcsAutorefreshed = !0, E().iframeBus.send("schedule.fcs-refresh-page"), void this._setLoadingTimer();
				this.setState({
					isLongLoading: !0
				}), setTimeout((() => {
					this.state.isReportSent || I().chromeBus.send("overseer.send-report", {
						key: "schedule-auto"
					})
				}), 5 * P().SECOND)
			}), 30 * P().SECOND)
		}
	}
	var uo = y().influx((e => ({
		posts: le().default.getPosts({
			applyGridFilters: !0
		}),
		loading: le().default.isLoading(),
		isFcsConnected: e.schedule.fcsSetup.connected,
		hasDndTasks: Rn.hasDndTasks()
	})))(co);
	k(), S(), v(), y();
	class ho extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onMediaTypeClick = e => {
				v().gaController.sendEvent("user", `schedule:media-type-filter-click-${e}`), y().transaction((t => {
					this._toggleInArray(t.schedule.filters.mediaTypes, e)
				}))
			}, this._onStatusClick = e => {
				v().gaController.sendEvent("user", `schedule:status-filter-click-${e}`), y().transaction((t => {
					this._toggleInArray(t.schedule.filters.statuses, e)
				}))
			}, this._onShowInfoClick = () => {
				v().gaController.sendEvent("user", "schedule:show-info-click"), y().transaction((e => {
					e.schedule.filters.showInfo = !e.schedule.filters.showInfo
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.ScheduleGridControls, {
				dropdown1: this._getMediaTypesDropdown(),
				dropdown2: this._getStatusesDropdown(),
				switch: {
					label: "Show info",
					checked: this.props.filters.showInfo,
					onClick: this._onShowInfoClick
				}
			})
		}
		_getMediaTypesDropdown() {
			const e = this.props.filters.mediaTypes;
			return {
				label: 3 === e.length ? "All media types" : 0 === e.length ? "Not selected" : `Media types: ${e.length}`,
				items: [{
					id: "photo",
					label: "Photos",
					selected: e.includes("photo")
				}, {
					id: "video",
					label: "Videos",
					selected: e.includes("video")
				}, {
					id: "carousel",
					label: "Carousels",
					selected: e.includes("carousel")
				}],
				onItemClick: this._onMediaTypeClick
			}
		}
		_getStatusesDropdown() {
			const e = this.props.filters.statuses;
			return {
				label: 4 === e.length ? "Any status" : 0 === e.length ? "Not selected" : `Status: ${e.length}`,
				items: [{
					id: "posted",
					label: "Posted",
					selected: e.includes("posted")
				}, {
					id: "local",
					label: "Local",
					color: k().default.color.attention,
					selected: e.includes("local")
				}, {
					id: "draft",
					label: "Draft",
					color: k().default.color.positive,
					selected: e.includes("draft")
				}, {
					id: "scheduled",
					label: "Scheduled",
					color: k().default.color.positive,
					selected: e.includes("scheduled")
				}],
				onItemClick: this._onStatusClick
			}
		}
		_toggleInArray(e, t) {
			const s = e.indexOf(t); - 1 === s ? e.push(t) : e.splice(s, 1)
		}
	}
	var mo = y().influx((e => ({
		filters: e.schedule.filters
	})))(ho);
	k(), S(), P(), y(), le();
	class po extends k().default.Component {
		constructor(e) {
			super(e), this._onAppKeyDown = e => {
				"drop-form" === this.state.viewMode && "Escape" === e.key && (this.setState({
					files: []
				}), this.setState({
					viewMode: "default"
				}))
			}, this._onAppDragEnter = e => {
				this.props.isScheduleTabOpen && (e.preventDefault(), clearTimeout(this._appDragLeaveTimer), "Files" === e.dataTransfer.types[0] && this.setState({
					viewMode: "dropzone"
				}))
			}, this._onAppDragOver = e => {
				this.props.isScheduleTabOpen && (e.preventDefault(), clearTimeout(this._appDragLeaveTimer), "Files" === e.dataTransfer.types[0] && (this.setState({
					viewMode: "dropzone"
				}), this._appDragLeaveTimer = setTimeout((() => {
					"dropzone" === this.state.viewMode && this.setState({
						viewMode: "default"
					})
				}), 50)))
			}, this._onAppDrop = e => {
				this.props.isScheduleTabOpen && (e.preventDefault(), clearTimeout(this._appDragLeaveTimer), setTimeout((() => {
					"dropzone" === this.state.viewMode && this.setState({
						viewMode: "default"
					})
				})))
			}, this._onScheduleClick = () => {
				this.props.loading || Rn.selectFiles((e => {
					this._onFilesAdded(e)
				}))
			}, this._onSaveChangesClick = () => {
				Rn.commitPostChanges()
			}, this._onCancelClick = () => {
				Rn.cancelPostChanges(), this.setState({
					viewMode: "default"
				})
			}, this._onLongPress = () => {
				y().transaction((e => {
					e.schedule.fallback.isEnabled = !0
				}))
			}, this._onDropzoneDrop = e => {
				const t = Array.from(e.dataTransfer.files);
				this._onFilesAdded(t)
			}, this._onDropFormOptionClick = e => {
				this.setState({
					selectedDropOption: e
				})
			}, this._onDropFormConfirmClick = () => {
				Un.addFiles({
					files: this._files,
					isCarousel: "carousel" === this.state.selectedDropOption
				}), this.setState({
					viewMode: "default"
				}), this.setState({
					fileCount: 0
				}), this._files = []
			}, this._onDropFormCancelClick = () => {
				this.setState({
					viewMode: "default"
				}), this.setState({
					fileCount: 0
				}), this._files = []
			}, this._onFilesAdded = e => {
				1 === e.length || e.length > 10 ? (this.setState({
					viewMode: "default"
				}), Un.addFiles({
					files: e,
					isCarousel: !1
				})) : (this.setState({
					viewMode: "drop-form"
				}), this.setState({
					fileCount: e.length
				}), this._files = e)
			}, this._appDragLeaveTimer = null, this._savedViewModeTimer = null, this._files = [], this.state = {
				viewMode: "default",
				fileCount: 0,
				selectedDropOption: "multiple"
			}
		}
		componentDidUpdate(e) {
			const t = e.savingState,
				s = this.props.savingState;
			s && this.setState({
				viewMode: s
			}), "save" === s ? clearTimeout(this._savedViewModeTimer) : "saving" !== t || s || (this.setState({
				viewMode: "saved"
			}), this._savedViewModeTimer = setTimeout((() => {
				this.setState({
					viewMode: "default"
				})
			}), 2 * P().SECOND))
		}
		componentDidMount() {
			const e = document.documentElement;
			document.addEventListener("keydown", this._onAppKeyDown), e.addEventListener("dragenter", this._onAppDragEnter), e.addEventListener("dragover", this._onAppDragOver), e.addEventListener("drop", this._onAppDrop)
		}
		componentWillUnmount() {
			const e = document.documentElement;
			document.removeEventListener("keydown", this._onAppKeyDown), e.removeEventListener("dragenter", this._onAppDragEnter), e.removeEventListener("dragover", this._onAppDragOver), e.removeEventListener("drop", this._onAppDrop), clearTimeout(this._appDragLeaveTimer), clearTimeout(this._savedViewModeTimer)
		}
		render() {
			const e = this.state.fileCount;
			return Glamor.createElement(S().default.ScheduleGridAddCard, {
				texts: {
					title: "SELECT FILES",
					description: "Click or drop photos and videos to draft, schedule or publish posts and carousels.",
					dropzoneTitle: "DRAG & DROP",
					dropzoneSubtitle: "your photos and videos to schedule them to Instagram.",
					dropFormDescription: `Select what has to be done with the ${e} files you uploaded:`,
					dropFormOptionMultiple: `Create ${e} new posts`,
					dropFormOptionCarousel: "Create a carousel post",
					dropFormConfirm: "CONFIRM",
					dropFormCancel: "CANCEL"
				},
				viewMode: this.state.viewMode,
				attention: this.props.attention,
				dropFormSelectedOption: this.state.selectedDropOption,
				onScheduleClick: this._onScheduleClick,
				onSaveChangesClick: this._onSaveChangesClick,
				onCancelClick: this._onCancelClick,
				onLongPress: this._onLongPress,
				onDropzoneDrop: this._onDropzoneDrop,
				onDropFormOptionClick: this._onDropFormOptionClick,
				onDropFormConfirmClick: this._onDropFormConfirmClick,
				onDropFormCancelClick: this._onDropFormCancelClick
			})
		}
	}
	var go = y().influx((e => ({
		loading: le().default.isLoading(),
		attention: e.schedule.gridAddCardAttention,
		savingState: le().default.getSavingState(),
		isScheduleTabOpen: e.sidebar.isOpen && "tab-scheduling" === e.sidebar.selectedTabId
	})))(po);
	k(), S(), y(), I(), ve();
	class fo extends k().default.Component {
		constructor(e) {
			super(e), this._onCloseClick = () => {
				y().transaction((e => {
					e.schedule.fcsSetup.showPanel = !1
				}))
			}, this._onConnectClick = () => {
				I().chromeBus.send("schedule.connect-to-fcs")
			}, this.state = {
				tryAgainWasClicked: !1
			}
		}
		render() {
			return Glamor.createElement(S().default.ScheduleSetupPanel, {
				connecting: this.props.connecting,
				onCloseClick: this._onCloseClick,
				onConnectClick: this._onConnectClick,
				alternativeContent: this._getAlternativeContent()
			})
		}
		_getAlternativeContent() {
			if (!this.props.errorCode) return null;
			if (this.props.errorCode === ve().default.nonProfessionalAccount) return {
				isError: !1,
				title: "Setup Post Assistant",
				text: Glamor.createElement(k().default.Fragment, null, "Please switch your Instagram to ", Glamor.createElement("b", null, "Professional Account"), " from Instagram Mobile App.", Glamor.createElement(k().default.Spacer, {
					height: "g1"
				}), Glamor.createElement("div", {
					css: {
						marginLeft: k().default.space.g2,
						...k().default.text.of({
							family: k().default.font.secondary,
							size: 14,
							height: "19px",
							weight: 600
						})
					}
				}, "1. Open Instagram Mobile App", Glamor.createElement(k().default.Spacer, {
					height: "g1"
				}), "2. Click Settings → Account", Glamor.createElement(k().default.Spacer, {
					height: "g1"
				}), "3. Click Switch to Professional Account", Glamor.createElement(k().default.Spacer, {
					height: "g1"
				}))),
				buttons: [{
					label: "OK, DONE",
					onClick: () => {
						y().transaction((e => {
							e.schedule.fcsSetup.errorCode = null
						}))
					}
				}]
			};
			if (this.props.errorCode === ve().default.fbLoginRequired) return {
				isError: !0,
				title: "CROSSPOSTING SETUP FAILED",
				text: Glamor.createElement(k().default.Fragment, null, "Please make sure to login to ", Glamor.createElement("a", {
					href: "https://facebook.com",
					target: "_blank"
				}, "Facebook.com")),
				buttons: [{
					label: "OK, GOT IT",
					onClick: () => {
						y().transaction((e => {
							e.schedule.fcsSetup.showPanel = !1
						}))
					}
				}, {
					label: "OPEN FACEBOOK.COM",
					onClick: () => {
						chrome.tabs.create({
							active: !0,
							url: "https://facebook.com?elcw"
						})
					}
				}]
			};
			if (this.props.errorCode === ve().default.noConnectedFbPage) {
				const e = this.props.username ? `@${this.props.username}` : "Your";
				return {
					isError: !0,
					title: "CROSSPOSTING SETUP FAILED",
					text: Glamor.createElement(k().default.Fragment, null, e, " Instagram account is not connected to a Facebook Page."),
					buttons: [{
						label: "OK, GOT IT",
						onClick: () => {
							y().transaction((e => {
								e.schedule.fcsSetup.showPanel = !1
							}))
						}
					}, {
						label: "OPEN FACEBOOK.COM",
						onClick: () => {
							chrome.tabs.create({
								active: !0,
								url: "https://facebook.com?elcw"
							})
						}
					}]
				}
			}
			return this.state.tryAgainWasClicked ? {
				isError: !0,
				title: "SETUP FAILED",
				text: Glamor.createElement(k().default.Fragment, null, "Failed to setup Post Assistant automatically. Please check your Internet connection or try setting up connection manually."),
				buttons: [{
					label: "OK, GOT IT",
					onClick: () => {
						y().transaction((e => {
							e.schedule.fcsSetup.errorCode = null
						}))
					}
				}, {
					label: "SETUP MANUALLY",
					onClick: () => {
						chrome.tabs.create({
							active: !0,
							url: "https://business.facebook.com/creatorstudio?tab=instagram_accounts&mode=instagram&collection_id=all_pages"
						})
					}
				}]
			} : {
				isError: !0,
				title: "SETUP FAILED",
				text: Glamor.createElement(k().default.Fragment, null, "Failed to setup Post Assistant automatically. Please check your Internet connection and try again."),
				buttons: [{
					label: "TRY AGAIN",
					onClick: () => {
						this.setState({
							tryAgainWasClicked: !0
						}), y().transaction((e => {
							e.schedule.fcsSetup.errorCode = null
						}))
					}
				}]
			}
		}
	}
	var Co = y().influx((e => ({
		username: e.authStatus.username,
		connecting: e.schedule.fcsSetup.connecting,
		errorCode: e.schedule.fcsSetup.errorCode
	})))(fo);
	class wo extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCloseClick = () => {
				y().transaction((e => {
					e.sidebar.isOpen = !1
				}))
			}, this._onBlurOverlayClick = () => {
				y().transaction((e => {
					e.schedule.fcsSetup.connecting || e.schedule.fcsSetup.errorCode || (e.schedule.fcsSetup.showPanel = !1)
				}))
			}, this._onRefreshClick = () => {
				v().gaController.sendEvent("user", "schedule:refresh-click"), y().transaction((e => {
					e.schedule.loading = !0
				}));
				y().model.state.schedule.fcsSetup.connected ? E().iframeBus.send("schedule.fcs-refresh-data") : I().chromeBus.send("schedule.update-ig-posts")
			}, this._onTabClick = e => {
				Rn.openTab(e)
			}, this._onDndTipOkClick = () => {
				y().transaction((e => {
					e.acknowledged.scheduleDndTip = Date.now()
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.ScheduleSidePanel, {
				isRefreshing: this.props.loading,
				navigationControls: {
					tabs: [{
						id: "calendar"
					}, {
						id: "time-slots"
					}],
					selectedTabId: this.props.selectedTabId,
					onTabSelected: this._onTabClick
				},
				gridControls: Glamor.createElement(mo, null),
				gridAddCard: Glamor.createElement(go, null),
				gridTip: this.props.dndTipShown && {
					icon: "bulb",
					content: Glamor.createElement(k().default.Fragment, null, Glamor.createElement("b", null, "Tips:"), " You can rearrange any drafts and scheduled post in a grid or calendar by dragging them with a mouse."),
					buttons: [{
						label: "OK, GOT IT",
						onClick: this._onDndTipOkClick
					}]
				},
				grid: Glamor.createElement(uo, null),
				setupPanel: {
					shown: this.props.setupPanelShown,
					content: Glamor.createElement(Co, null)
				},
				onBlurOverlayClick: this._onBlurOverlayClick,
				onRefreshClick: this._onRefreshClick,
				onCloseClick: this._onCloseClick
			})
		}
	}
	var vo = y().influx((e => ({
		selectedTabId: e.schedule.navigation.selectedTabId,
		dndTipShown: !e.acknowledged.scheduleDndTip,
		setupPanelShown: e.schedule.fcsSetup.showPanel,
		loading: le().default.isLoading()
	})))(wo);
	k(), S(), J(), y(), T(), k(), S(), F(), y();
	class ko extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCloseClick = () => {
				this.props.goToCalendarOnClose ? y().transaction((e => {
					e.schedule.navigation.selectedTabId = "calendar"
				})) : Rn.closeBodyPanel()
			}, this._onTagAssistClick = () => {
				y().transaction((e => {
					e.schedule.showTagAssist = !e.schedule.showTagAssist
				}))
			}, t
		}
		render() {
			if (!this.props.url) return null;
			const e = He.createName("inssist-fcs", {
					theme: this.state.theme,
					fusionConfig: We.getConfig(),
					isElectron: !!window.electron
				}),
				t = !1 !== F().env.features.iframes ? this.props.url : null;
			return Glamor.createElement(S().default.ScheduleFcsScreen, {
				title: this.props.title,
				loading: this.props.isLoading,
				action: this._getAction(),
				onCloseClick: this._onCloseClick,
				iframeAttrs: {
					name: e,
					allow: "geolocation",
					src: t
				}
			})
		}
		_getAction() {
			return {
				label: "# HASHTAGS",
				selected: this.props.showTagAssist,
				onClick: this._onTagAssistClick
			}
		}
	}
	var So = y().influx((e => {
		let t;
		return t = "post" === e.schedule.navigation.selectedTabId ? "Create New Post" : "Post Details", {
			title: t,
			isLoading: e.schedule.navigation.isFcsLoading,
			showTagAssist: e.schedule.showTagAssist,
			goToCalendarOnClose: e.schedule.navigation.withBackToCalendarButton
		}
	}))(k().default.theme.ThemeAware(ko));
	k(), S(), y(), le();
	class bo extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onPostDrop = (e, t) => {
				Rn.addPostChanges({
					[e.id]: {
						on: t
					}
				})
			}, t
		}
		render() {
			return Glamor.createElement(S().default.ScheduleCalendarScreen, {
				title: "Post Calendar",
				periodType: this.props.periodType,
				periodStart: this.props.periodStart || Date.now(),
				showSlots: this.props.showSlots,
				posts: this.props.posts,
				slots: this.props.slots,
				minMinutesFromNow: 15,
				maxDaysFromNow: 170,
				dragDisabled: this.props.isSaving,
				onDragFail: Rn.blinkAddCardAttention,
				onCloseClick: Rn.closeBodyPanel,
				onPeriodTypeChange: this._onPeriodTypeChange,
				onPeriodStartChange: this._onPeriodStartChange,
				onShowSlotsChange: this._onShowSlotsChange,
				onPostClick: this._onPostClick,
				onPostDrop: this._onPostDrop,
				onAddPostClick: this._onAddPostClick
			})
		}
		_onPeriodTypeChange(e) {
			y().transaction((t => {
				t.schedule.calendar.periodType = e
			}))
		}
		_onPeriodStartChange(e) {
			y().transaction((t => {
				t.schedule.calendar.periodStart = e
			}))
		}
		_onShowSlotsChange(e) {
			y().transaction((t => {
				t.schedule.calendar.showTimeSlots = e
			}))
		}
		_onAddPostClick({
			dayStart: e,
			time: t
		}) {
			const s = y().model.state.schedule.timeSlots.find((e => e.time === t));
			Rn.openNewPost({
				postMode: "schedule"
			}), y().transaction((a => {
				a.schedule.navigation.withBackToCalendarButton = !0, a.schedule.dateDialog.periodStart = e, a.schedule.dateDialog.selectedDay = e, a.schedule.dateDialog.selectedSlotTime = s ? t : null, a.schedule.dateDialog.customTime = s ? null : t
			})), Fn.actualizeDataForIframe()
		}
		_onPostClick({
			post: e
		}) {
			Rn.openPost(e), y().transaction((e => {
				e.schedule.navigation.withBackToCalendarButton = !0
			}))
		}
	}
	var yo = y().influx((e => ({
		posts: le().default.getPosts({
			applyGridFilters: !0
		}).filter((e => "draft" !== e.status)).map((e => ({ ...e,
			key: e.id
		}))),
		slots: e.schedule.timeSlots,
		periodType: e.schedule.calendar.periodType,
		periodStart: e.schedule.calendar.periodStart,
		showSlots: e.schedule.calendar.showTimeSlots,
		isSaving: e.schedule.tasks.filter((e => !e.doneAt)).length > 0
	})))(bo);
	k(), S(), y(), P();
	class _o extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onAddSlotClick = e => {
				y().transaction((t => {
					!!t.schedule.timeSlots.find((t => t.time === e)) || (t.schedule.recentScheduledOn = null, t.schedule.timeSlots.push({
						time: e
					}), t.schedule.timeSlots.sort(((e, t) => {
						let s = e.time - 5 * P().HOUR;
						s < 0 && (s = 24 * P().HOUR + s);
						let a = t.time - 5 * P().HOUR;
						return a < 0 && (a = 24 * P().HOUR + a), s - a
					})))
				}))
			}, this._onDeleteSlotClick = e => {
				y().transaction((t => {
					const s = t.schedule.timeSlots.findIndex((t => t.time === e)); - 1 !== s && t.schedule.timeSlots.splice(s, 1)
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.ScheduleSlotsScreen, {
				title: "Time Slots",
				description: "Manage slots for quick scheduling",
				slots: this.props.slots,
				onCloseClick: Rn.closeBodyPanel,
				onAddSlotClick: this._onAddSlotClick,
				onDeleteSlotClick: this._onDeleteSlotClick
			})
		}
	}
	var Eo = y().influx((e => ({
		slots: e.schedule.timeSlots
	})))(_o);
	k(), S(), E(), y();
	class To extends k().default.Component {
		render() {
			return this.props.url ? Glamor.createElement(S().default.ScheduleFallback, {
				topPanel: {
					icon: "warning-triangle",
					iconStyle: {
						width: 14,
						height: 16,
						color: k().default.color.error
					},
					text: "Failed connecting to Facebook. Interface was switched to fallback version.",
					button: {
						label: "RETRY FB CONNECTION",
						loadingText: "Retrying...",
						color: k().default.color.link,
						isLoading: this.props.isRetryingFbConnection,
						onClick: this._onRetryClick
					}
				},
				iframeAttrs: {
					name: He.createName("inssist-fcs", {
						fusionConfig: We.getConfig(),
						isElectron: !!window.electron
					}),
					src: this.props.url
				}
			}) : null
		}
		async _onRetryClick() {
			y().transaction((e => {
				e.schedule.fallback.isRetryingFbConnection = !0, e.schedule.fcsSetup.connected = !1
			}));
			const e = await E().iframeBus.send("schedule.fcs-check-critical-vars");
			y().transaction((t => {
				e ? (t.schedule.navigation.isOpen = !1, t.schedule.navigation.selectedTabId = null, t.schedule.fallback.isEnabled = !1, t.schedule.fallback.isRetryingFbConnection = !1) : (t.schedule.isErrorShown = !0, t.schedule.fallback.isFailedToReconnect = !0, t.schedule.fallback.isRetryingFbConnection = !1)
			}))
		}
	}
	var Ao = y().influx((e => ({
		isRetryingFbConnection: e.schedule.fallback.isRetryingFbConnection
	})))(To);
	const Po = ({
		visible: e
	}) => ({
		height: "100%",
		...!e && { ...k().default.fixed(),
			visibility: "hidden",
			pointerEvents: "none",
			opacity: 0
		}
	});
	class Io extends k().default.Component {
		constructor(e) {
			super(e), this._onUpsellOverlayClick = () => {
				y().transaction((e => {
					e.schedule.isUpsellShown = !1
				}))
			}, this.fcsMainPageUrl = J().default("https://business.facebook.com/creatorstudio", {
				tab: "instagram_content_posts",
				mode: "instagram",
				collection_id: "all_pages",
				content_table: "INSTAGRAM_POSTS",
				locale: "en_US"
			})
		}
		render() {
			return this.props.isFallbackEnabled ? Glamor.createElement(Ao, {
				url: this.fcsMainPageUrl
			}) : Glamor.createElement(S().default.ScheduleBodyPanel, {
				loading: this.props.isLoading,
				showTagAssistPanel: this.props.showTagAssist,
				onLoadingCloseClick: Rn.closeBodyPanel,
				upsellOverlay: this._constructUpsellOverlay(),
				tagAssistPanel: this._constructTagAssistPanel()
			}, this._renderContent())
		}
		_renderContent() {
			const e = this.props;
			let t = null,
				s = !1;
			return "calendar" === e.selectedTabId ? t = Glamor.createElement(yo, null) : "time-slots" === e.selectedTabId ? t = Glamor.createElement(Eo, null) : e.isFcsConnected && (s = !0), Glamor.createElement(k().default.Fragment, null, Glamor.createElement("div", {
				css: Po({
					visible: s
				})
			}, Glamor.createElement(So, {
				url: this.fcsMainPageUrl
			})), t)
		}
		_constructUpsellOverlay() {
			return Glamor.createElement(Bs, {
				show: this.props.showUpsell,
				feature: "schedule",
				onOverlayClick: this._onUpsellOverlayClick
			})
		}
		_constructTagAssistPanel() {
			return Glamor.createElement(yn, {
				placement: "schedule"
			})
		}
	}
	var Fo = y().influx((e => {
		const t = T().stateProxy.hasPro(),
			s = e.schedule.fcsSetup.connected;
		return {
			isLoading: !s,
			isFcsConnected: s,
			isFallbackEnabled: e.schedule.fallback.isEnabled,
			selectedTabId: e.schedule.navigation.selectedTabId,
			showUpsell: !t && s && e.schedule.isUpsellShown,
			showTagAssist: e.schedule.showTagAssist
		}
	}))(Io);

	function xo() {
		return (xo = Object.assign || function (e) {
			for (var t = 1; t < arguments.length; t++) {
				var s = arguments[t];
				for (var a in s) Object.prototype.hasOwnProperty.call(s, a) && (e[a] = s[a])
			}
			return e
		}).apply(this, arguments)
	}
	k(), S(), E(), y();
	class Go extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onCloseClick = () => {
				y().transaction((e => {
					e.schedule.isErrorShown = !1
				}))
			}, this._onRefreshClick = () => {
				y().transaction((e => {
					e.schedule.loading = !0, e.schedule.isErrorShown = !1, e.schedule.navigation.isOpen = !1, e.schedule.navigation.selectedTabId = null
				})), E().iframeBus.send("schedule.fcs-refresh-page")
			}, this._onSwitchToFallbackClick = () => {
				y().transaction((e => {
					e.schedule.isErrorShown = !1, e.schedule.fallback.isEnabled = !0
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "schedule-error-card-mediator",
				show: this.props.isShown
			}, Glamor.createElement(S().default.ScheduleErrorCard, xo({}, this._getErrorCardData(), {
				onCloseClick: this._onCloseClick
			})))
		}
		_getErrorCardData() {
			return this.props.failedToReconnect ? {
				title: "Failed connecting to Instagram API Server",
				content: "\n          We experience temporary technical issues connecting\n          to Instagram API Server and some grid functionality\n          may be unavailable. We are working on fixing the problem.\n        "
			} : this.props.fcsFailed ? {
				title: "Failed connecting to Instagram API Server",
				content: "\n          We experience temporary technical issues connecting\n          to Instagram API Server and some grid functionality\n          may be unavailable. We are working on fixing the problem.\n        ",
				actions: [{
					label: "REFRESH",
					onClick: this._onRefreshClick
				}, !this.props.hideSwitchToFallbackButton && {
					label: "SWITCH TO FALLBACK VERSION",
					onClick: this._onSwitchToFallbackClick
				}]
			} : this.props.fcsError ? {
				title: "Instagram API error.",
				content: this.props.fcsError
			} : null
		}
	}
	var Do = y().influx((e => ({
		isShown: "tab-scheduling" === e.sidebar.selectedTabId && e.sidebar.isOpen && e.schedule.isErrorShown,
		fcsError: e.schedule.fcsError,
		fcsFailed: e.schedule.fcsFailed,
		failedToReconnect: e.schedule.fallback.isFailedToReconnect,
		hideSwitchToFallbackButton: e.schedule.fallback.hideSwitchToFallbackButton
	})))(Go);
	k(), S(), E(), y(), P(), le();
	const Oo = ({
		props: e
	}) => ({ ...k().default.fixed(". 33 66 ."),
		...k().default.transition.fast,
		zIndex: 1,
		...!e.isOpen && {
			opacity: 0,
			transform: "translateY(10px)",
			pointerEvents: "none"
		},
		...e.showTagAssist && {
			right: 473
		}
	});
	class Bo extends k().default.Component {
		constructor(e) {
			super(e), this._onOptionClick = async e => {
				if (y().transaction((t => {
						t.schedule.dateDialog.selectedOption = e
					})), E().iframeBus.send("schedule.fcs-date-dialog-select-option", e), "schedule" === e) {
					const e = await E().iframeBus.send("schedule.fcs-date-dialog-get-timezone");
					y().transaction((t => {
						t.schedule.dateDialog.timezone = e
					}))
				}
				Fn.actualizeDataForIframe()
			}, this._onDaySelect = e => {
				y().transaction((t => {
					t.schedule.dateDialog.selectedDay = e
				})), Fn.actualizeDataForIframe()
			}, this._onPeriodStartChange = e => {
				y().transaction((t => {
					t.schedule.dateDialog.periodStart = e
				}))
			}, this._onTimeInputInput = () => {
				y().transaction((e => {
					e.schedule.dateDialog.isTimeError = !1
				}))
			}, this._onTimeInputChange = e => {
				y().transaction((t => {
					t.schedule.dateDialog.customTime = e
				})), Fn.actualizeDataForIframe()
			}, this._onSlotTimeChange = e => {
				y().transaction((t => {
					t.schedule.dateDialog.selectedSlotTime = e, t.schedule.dateDialog.isTimeError = !1
				})), Fn.actualizeDataForIframe()
			}, this._now = Date.now()
		}
		render() {
			return Glamor.createElement("div", {
				css: Oo(this)
			}, Glamor.createElement(S().default.ScheduleDateDialog, {
				selectedOption: this.props.selectedOption,
				onOptionClick: this._onOptionClick,
				datePicker: this._renderDatePicker(),
				timePicker: this._renderTimePicker()
			}))
		}
		_renderDatePicker() {
			return Glamor.createElement(S().default.ScheduleDatePicker, {
				periodStart: this.props.periodStart,
				maxDay: this._now + 74 * P().DAY,
				selectedDay: this.props.selectedDay,
				onDaySelect: this._onDaySelect,
				onPeriodStartChange: this._onPeriodStartChange
			})
		}
		_renderTimePicker() {
			let e = null;
			return this.props.isTimeError && (e = {
				hasError: !0,
				message: "should be at least 10 minutes from now"
			}), Glamor.createElement(S().default.ScheduleTimePicker, {
				label: "time to publish",
				timezone: this.props.timezone,
				slots: this.props.slots,
				selectedSlotTime: this.props.selectedSlotTime,
				timeInputValue: this.props.customTime,
				error: e,
				onTimeInputInput: this._onTimeInputInput,
				onTimeInputChange: this._onTimeInputChange,
				onSlotTimeChange: this._onSlotTimeChange
			})
		}
	}
	var Uo = y().influx((e => {
		const t = e.schedule.dateDialog,
			s = e.schedule.navigation.selectedTabId;
		return {
			isOpen: e.sidebar.isOpen && "tab-scheduling" === e.sidebar.selectedTabId && e.schedule.navigation.isOpen && (!s || "post" === s) && !e.schedule.isUpsellShown && t.isOpen,
			slots: e.schedule.timeSlots,
			selectedOption: t.selectedOption,
			periodStart: le().default.getDateDialogPeriodStart(),
			selectedDay: le().default.getDateDialogSelectedDay(),
			selectedSlotTime: t.selectedSlotTime,
			customTime: t.customTime,
			timezone: t.timezone,
			isTimeError: t.isTimeError,
			showTagAssist: e.schedule.showTagAssist
		}
	}))(Bo);
	k(), S(), y();
	const Lo = {
		content: {
			marginTop: k().default.space.g0h
		},
		errorGroup: {
			marginBottom: k().default.space.g1h,
			"&:last-child": {
				marginBottom: 0
			}
		},
		errorGroupTitle: { ...k().default.row
		},
		errorFilename: e => ({
			fontWeight: 700,
			...k().default.text.nowrap,
			...e && { ...k().default.text.ellipsis
			}
		}),
		errorFilenameDivider: {
			opacity: .5,
			...k().default.margin("0 g1")
		},
		errorGroupDescription: {
			marginTop: k().default.g0h,
			"& a": {
				color: k().default.color.link,
				...k().default.clickable
			}
		}
	};
	class Ro extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._renderContent = () => {
				const e = [];
				for (const t of this.props.bulkUploadErrors) {
					let s = e.find((e => e.type === t.type));
					s || (s = {
						type: t.type,
						filenames: []
					}, e.push(s)), s.filenames.push(t.filename)
				}
				return Glamor.createElement("div", {
					css: Lo.content
				}, e.map(this._renderErrorGroup))
			}, this._renderErrorGroup = e => {
				const t = e.filenames,
					s = t.slice(0, 2);
				return t.length > 2 && s.push(t.length - 2 + " more..."), Glamor.createElement("div", {
					css: Lo.errorGroup,
					key: e.type
				}, Glamor.createElement("div", {
					css: Lo.errorGroupTitle
				}, s.map(((e, t) => {
					const a = t === s.length - 1;
					return Glamor.createElement(k().default.Fragment, {
						key: t
					}, Glamor.createElement("div", {
						css: Lo.errorFilename(t < 2)
					}, e), !a && Glamor.createElement("span", {
						css: Lo.errorFilenameDivider
					}, "|"))
				}))), Glamor.createElement("div", {
					css: Lo.errorGroupDescription
				}, this._renderErrorDescription(e.type)))
			}, this._renderErrorDescription = e => "unsupported-type" === e ? Glamor.createElement(k().default.Fragment, null, "Unsupported file type. Please use PNG / JPG for photos and MP4 / MOV for videos.") : "unsupported-duration" === e ? Glamor.createElement(k().default.Fragment, null, "Instagram does not accept videos longer than 1 minute. Please cut this video in parts with ", Glamor.createElement("a", {
				href: "https://ezgif.com/cut-video",
				target: "_blank"
			}, "EzGif"), " or post it as IGTV.") : "unsupported-ratio" === e ? Glamor.createElement(k().default.Fragment, null, "Unsupported size ratio. Please check the aspect ratios Instagram supports on our ", Glamor.createElement("a", {
				href: "https://inssist.com/faq#images-are-cut-or-videos-do-not-upload",
				target: "_blank"
			}, "FAQ page"), ".") : "unsupported-codec" === e ? Glamor.createElement(k().default.Fragment, null, "Unsupported video codec. A MP4-h264 or WEBM is recommended. Find more on our ", Glamor.createElement("a", {
				href: "https://inssist.com/faq#images-are-cut-or-videos-do-not-upload",
				target: "_blank"
			}, "FAQ page"), ".") : "file-size-limit-reached" === e ? Glamor.createElement(k().default.Fragment, null, "The file is above ", Un.config.maxFileSizeStr, " and is too large to upload. Please resize or compress the file and try again.") : "post-count-limit-reached" === e ? Glamor.createElement(k().default.Fragment, null, "Post count limit reached. You have reached a limit of ", Un.config.maxPostCount, " local posts. Please schedule or remove some posts before adding more.") : "unknown" === e ? Glamor.createElement(k().default.Fragment, null, "Oops... Instagram did not like this file 🤭. Please try again later or pick another file.") : "", this._onCloseClick = () => {
				y().transaction((e => {
					e.schedule.bulkUploadErrors = []
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.SnackbarItem, {
				id: "schedule-bulk-error-card-mediator",
				show: this.props.bulkUploadErrors.length > 0
			}, Glamor.createElement(S().default.ScheduleErrorCard, {
				title: "Failed uploading files",
				content: this._renderContent(),
				onCloseClick: this._onCloseClick
			}))
		}
	}
	var Mo = y().influx((e => ({
			bulkUploadErrors: e.schedule.bulkUploadErrors
		})))(Ro),
		No = {
			init: function () {
				document.addEventListener("click", (() => {
					E().iframeBus.send("feature-encourage.app-click")
				})), E().iframeBus.on("feature-encourage.toggle-creation-card", zo), E().iframeBus.on("feature-encourage.get-unfollowers-data", Vo), E().iframeBus.on("feature-encourage.show-unfollowers", Ho), E().iframeBus.on("feature-encourage.create-insights-task", $o), E().iframeBus.on("feature-encourage.post-creation-carousel-click", Wo), E().iframeBus.on("feature-encourage.post-creation-schedule-click", qo)
			}
		};

	function zo(e) {
		y().transaction((t => {
			t.igView.creationCardShown = e
		}))
	}
	async function Vo() {
		const e = Date.now(),
			t = y().model.state.analytics.lastScanOn,
			s = (await xt.fetchAnalytics(t)).uFollowers;
		let a, n = 1,
			o = [];
		for (; !(o = s.filter((t => ue().default(e, t.on) < n)), o.length || n > 7);) n += 1;
		return a = n <= 1 ? "Today" : 2 === n ? "Yesterday" : n < 5 ? `Past ${n-1} days` : n <= 7 ? "Past week" : `Past ${n} days`, {
			count: o.length,
			avatarUrls: o.slice(0, 2).map((e => e.avatarUrl)),
			period: a
		}
	}

	function Ho() {
		y().transaction((e => {
			e.sidebar.selectedTabId = "tab-analytics", e.sidebar.isOpen = !0, e.analytics.selectedCard = "uFollowers", e.analytics.isReportOpen = !0
		}))
	}

	function $o(e) {
		y().transaction((e => {
			e.sidebar.selectedTabId = "tab-analytics", e.sidebar.isOpen = !0, e.analytics.isReportOpen = !1, e.analytics.selectedCard = null
		})), setTimeout((() => {
			Qe.addTask(e)
		}), 300)
	}
	async function Wo(e) {
		return v().gaController.sendEvent("user", "feature-encourage:post-creation-carousel-click"), new Promise((t => {
			Rn.selectFiles((async s => {
				y().transaction((e => {
					e.sidebar.isOpen = !0, e.sidebar.selectedTabId = "tab-scheduling"
				})), await Rn.addFiles({
					files: [e, ...s],
					isCarousel: !0,
					postMode: "publish"
				}), t()
			}))
		}))
	}
	async function qo(e) {
		return v().gaController.sendEvent("user", "feature-encourage:post-creation-schedule-click"), new Promise((t => {
			y().transaction((e => {
				e.sidebar.isOpen = !0, e.sidebar.selectedTabId = "tab-scheduling"
			})), setTimeout((async() => {
				await Rn.addFiles({
					files: [e],
					isCarousel: !1,
					postMode: "schedule"
				}), t()
			}))
		}))
	}
	k(), S(), E(), T(), v(), y();
	const jo = {
		story: "feature-encourage-ig-creation-card-mediator.story",
		post: "feature-encourage-ig-creation-card-mediator.post",
		reels: "feature-encourage-ig-creation-card-mediator.reels",
		igtv: "feature-encourage-ig-creation-card-mediator.igtv",
		carousel: "feature-encourage-ig-creation-card-mediator.carousel",
		scheduled: "feature-encourage-ig-creation-card-mediator.scheduled"
	};
	k().default.SvgIcon.registerSvgIcons([`<symbol id="${jo.story}" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12.474 22.779a11.28 11.28 0 01-5.294-1.32.777.777 0 11.732-1.37 9.741 9.741 0 006.775.884.777.777 0 01.353 1.513 11.326 11.326 0 01-2.566.293zm-7.205-2.871a.773.773 0 01-.534-.213 11.218 11.218 0 01-3.2-5.509.777.777 0 011.51-.366 9.667 9.667 0 002.757 4.748.777.777 0 01-.534 1.34zm-3.221-8.651h-.077a.776.776 0 01-.7-.849 11.174 11.174 0 01.995-3.632.777.777 0 011.408.656 9.618 9.618 0 00-.854 3.122.777.777 0 01-.772.703zm3.258-6.58a.777.777 0 01-.6-1.269q.1-.127.211-.25a.777.777 0 111.171 1.02c-.062.071-.122.143-.182.215a.776.776 0 01-.6.284zm12.543 16.62a.777.777 0 01-.4-1.443 9.7 9.7 0 00-4.975-18.03.777.777 0 110-1.554 11.255 11.255 0 015.773 20.917.77.77 0 01-.398.11z" fill="currentColor"/><path d="M17.723 10.788h-4.45v-4.45H11.72v4.45H7.27v1.553h4.45v4.45h1.553v-4.45h4.45z" fill="currentColor"/></symbol>`, `<symbol id="${jo.post}" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M7.164 22.654A5.17 5.17 0 012 17.49V7.164A5.17 5.17 0 017.164 2H17.49a5.17 5.17 0 015.164 5.164V17.49a5.17 5.17 0 01-5.164 5.164zm0-1.757H17.49a3.794 3.794 0 003.41-3.407V14.8l-3.68-3.661-4.394 5.934L8 14.866 3.766 17.7a3.832 3.832 0 003.398 3.2zM3.757 7.164v8.4L7.8 12.785l4.5 2.081 4.681-6.525 3.919 3.922v-5.1a3.794 3.794 0 00-3.41-3.406H7.164a3.794 3.794 0 00-3.407 3.407zm3.943.874a1.709 1.709 0 111.7 1.708 1.709 1.709 0 01-1.7-1.708z" fill="currentColor"/></symbol>`, `<symbol id="${jo.reels}" viewBox="0 0 24 24"><path d="M15.548 14.007l-.006-.005-.006-.004-4.696-2.73s0 0 0 0a.736.736 0 00-.381-.113.73.73 0 00-.373.108c-.262.11-.407.371-.407.662v5.464c0 .262.155.511.398.658h0l.009.004a.899.899 0 00.352.098.88.88 0 00.314-.069l.063-.022.012-.004.012-.006 4.697-2.732h0l.001-.001a.743.743 0 00.357-.658c0-.259-.152-.505-.346-.65zM20.764 3.85h0l-.003-.004c-.534-.513-1.125-.945-1.943-1.247-.816-.301-1.85-.47-3.264-.47h-6.5c-1.394 0-2.417.169-3.233.476-.818.307-1.42.75-1.974 1.283h0l-.003.004C3.33 4.428 2.9 5.02 2.599 5.834c-.3.812-.469 1.837-.469 3.234v6.514c0 1.396.169 2.432.475 3.255.306.824.748 1.428 1.282 1.964l.002.002c.534.513 1.126.945 1.943 1.246.816.302 1.85.47 3.264.47h6.5c1.394 0 2.428-.168 3.249-.475.823-.307 1.425-.75 1.96-1.285l.001-.002c.513-.535.944-1.128 1.245-1.947.3-.817.469-1.853.469-3.27V9.068c0-1.397-.169-2.422-.475-3.24-.306-.819-.748-1.422-1.28-1.977zm-5.168-.2c1.31 0 2.174.166 2.788.41.612.244.985.567 1.314.896.466.468.897 1.045 1.149 2.216h-3.665L15.19 3.65h.406zm-6.948 0h4.734l1.991 3.522H10.64L8.648 3.65zM4.952 4.957c.435-.436.982-.87 2.065-1.12l1.897 3.336h-5.11c.25-1.17.682-1.748 1.148-2.216zm16.05 10.667c0 1.313-.167 2.178-.41 2.794-.243.613-.565.987-.894 1.316-.328.33-.712.652-1.33.896-.62.244-1.482.411-2.772.411H9.054c-1.31 0-2.174-.167-2.788-.411-.612-.243-.985-.566-1.314-.896a3.699 3.699 0 01-.893-1.332c-.244-.62-.41-1.486-.41-2.778v-6.93h17.353v6.93z" fill="currentColor" stroke="currentColor" stroke-width=".26"/></symbol>`, `<symbol id="${jo.igtv}" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><g fill="currentColor"><path d="M22.643 10.555c-.007-.53-.023-.874-.045-1.35v-.109a7.646 7.646 0 00-.478-2.512 5.3 5.3 0 00-3.039-3.04 7.729 7.729 0 00-2.509-.48 47.27 47.27 0 00-2.311-.057l1.3-1.412a.941.941 0 00-.07-1.347.966.966 0 00-1.358.064l-1.806 1.96L10.529.314a.973.973 0 00-1.36-.066.944.944 0 00-.317.659.921.921 0 00.253.686L10.4 3.009c-1.088.005-1.5.017-2.311.055a7.771 7.771 0 00-2.512.48 5.1 5.1 0 00-1.838 1.2 5.147 5.147 0 00-1.2 1.841 7.74 7.74 0 00-.477 2.516C2.012 10.186 2 10.548 2 13.332s.012 3.146.062 4.236a7.673 7.673 0 00.481 2.509 5.322 5.322 0 003.037 3.04 7.62 7.62 0 002.512.483c1.088.047 1.415.057 4.238.057s3.151-.01 4.236-.057a7.565 7.565 0 002.509-.483 5.3 5.3 0 003.039-3.04 7.578 7.578 0 00.478-2.509c.052-1.061.062-1.382.062-4.236 0-1.34 0-2.177-.012-2.777m-1.959 2.777c0 1.432 0 2.165-.01 2.7-.01.555-.025.9-.047 1.434v.007a5.654 5.654 0 01-.352 1.89 3.326 3.326 0 01-1.915 1.91 5.463 5.463 0 01-1.885.349c-1.058.05-1.38.06-4.145.06s-3.087-.01-4.147-.06a5.538 5.538 0 01-1.882-.345 3.139 3.139 0 01-1.157-.753 3.128 3.128 0 01-.756-1.157 5.691 5.691 0 01-.347-1.89c-.047-1.045-.062-1.325-.062-4.144s.015-3.1.062-4.147a5.6 5.6 0 01.343-1.885 3.072 3.072 0 01.756-1.16A3.137 3.137 0 016.3 5.385a5.679 5.679 0 011.888-.349c1.07-.047 1.392-.057 4.147-.057s3.077.01 4.145.057a5.617 5.617 0 011.885.349 3.16 3.16 0 011.162.756 3.049 3.049 0 01.748 1.16 5.53 5.53 0 01.352 1.885c.047 1.07.057 1.395.057 4.147"/><path d="M11.475 14.88v-1.45a.124.124 0 00-.158-.119l-4.012 1.155a.93.93 0 01-1.185-.827.945.945 0 01.694-.966l5.57-1.6a.743.743 0 01.949.714v1.449a.124.124 0 00.158.119l3.859-1.112a.93.93 0 011.185.827.943.943 0 01-.694.965l-5.417 1.561a.743.743 0 01-.949-.714"/></g></symbol>`, `<symbol id="${jo.carousel}" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M10.03 22a4.283 4.283 0 01-2.605-.876 4.363 4.363 0 01-1.539-2.212h2a2.483 2.483 0 002.14 1.235h8.647a2.474 2.474 0 002.471-2.471V9.03a2.483 2.483 0 00-1.235-2.14v-2a4.365 4.365 0 012.212 1.539A4.283 4.283 0 0123 9.03v8.647A4.329 4.329 0 0118.677 22zm-3.706-3.706A4.328 4.328 0 012 13.97V5.324A4.328 4.328 0 016.324 1h8.646a4.328 4.328 0 014.324 4.324v8.646a4.328 4.328 0 01-4.324 4.324zM3.853 5.324v8.646a2.474 2.474 0 002.471 2.471h8.646a2.474 2.474 0 002.471-2.471V5.324a2.474 2.474 0 00-2.471-2.471H6.324a2.474 2.474 0 00-2.471 2.471z" fill="currentColor"/></symbol>`, `<symbol id="${jo.scheduled}" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13.841 1.565A9.936 9.936 0 003.906 11.5H.594l4.372 4.449 4.46-4.449H6.114a7.765 7.765 0 112.274 5.453L6.82 18.522a9.933 9.933 0 107.021-16.957zm-1.1 5.52v5.52l4.725 2.8.795-1.336-3.864-2.3V7.085z" fill="currentColor"/></symbol>`]);
	class Yo extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onStoryClick = () => {
				v().gaController.sendEvent("user", "feature-encourage:story-click"), E().iframeBus.send("feature-encourage.start-story-creation")
			}, this._onPhotoVideoClick = () => {
				v().gaController.sendEvent("user", "feature-encourage:photo-video-click"), E().iframeBus.send("feature-encourage.start-post-creation")
			}, this._onReelsClick = () => {
				v().gaController.sendEvent("user", "feature-encourage:reels-click"), E().iframeBus.send("feature-encourage.start-reels-creation")
			}, this._onIgtvVideoClick = () => {
				v().gaController.sendEvent("user", "feature-encourage:igtv-video-click"), y().transaction((e => {
					e.igtvUpload.shown = !0
				}))
			}, this._onCarouselPostClick = () => {
				v().gaController.sendEvent("user", "feature-encourage:carousel-post-click"), this._openScheduleNewPost()
			}, this._onScheduledPostClick = () => {
				v().gaController.sendEvent("user", "feature-encourage:scheduled-post-click"), this._openScheduleNewPost()
			}, this._openScheduleNewPost = () => {
				y().transaction((e => {
					e.sidebar.selectedTabId = "tab-scheduling", e.sidebar.isOpen = !0
				})), Rn.openNewPost()
			}, t
		}
		render() {
			return Glamor.createElement(S().default.IgCreationCard, {
				buttons: [{
					icon: jo.story,
					label: "STORY",
					onClick: this._onStoryClick
				}, {
					icon: jo.post,
					label: "PHOTO / VIDEO",
					onClick: this._onPhotoVideoClick
				}, this.props.reelsSupported && {
					icon: jo.reels,
					label: "REELS",
					// badge: this.props.hasReelsPro ? null : "PRO",
					onClick: this._onReelsClick
				}, {
					icon: jo.igtv,
					label: "IGTV VIDEO",
					onClick: this._onIgtvVideoClick
				}, {
					icon: jo.carousel,
					label: "CAROUSEL POST",
					// badge: this.props.hasScheduleAccess ? null : "PRO",
					onClick: this._onCarouselPostClick
				}, {
					icon: jo.scheduled,
					label: "SCHEDULED POST",
					// badge: this.props.hasScheduleAccess ? null : "PRO",
					onClick: this._onScheduledPostClick
				}].filter(Boolean)
			})
		}
	}
	var Ko = y().influx((e => ({
		reelsSupported: e.reels.supported,
		hasReelsPro: T().stateProxy.hasPro({
			feature: "reels"
		})
	})))(Yo);
	A(), J(), v(), y(), E(), C(), T();
	var Zo = {
		init: function () {
			window.addEventListener("message", (e => {
					"ig-patch-corrupted" === e.data.name && v().gaController.sendEvent("system", "ig-patch-corrupted")
				})),
				function () {
					let e = !1;
					window.addEventListener("hashchange", (() => {
						if (e) return;
						const t = location.hash;
						if (!t.startsWith("#instagram.com/")) return;
						const s = t.split("instagram.com")[1];
						E().iframeBus.send("ig.ajax-go", s)
					})), E().iframeBus.on("ig.url-change", (t => {
						e = !0;
						const s = `instagram.com${t}`;
						history.replaceState(null, null, `inssist.html#${s}`), e = !1
					}))
				}(), E().iframeBus.on("ig.error", Jo), E().iframeBus.on("ig.path-change", Xo), E().iframeBus.on("ig.media-open", ti), E().iframeBus.on("ig.media-download", si), E().iframeBus.on("ig.media-fullscreen-enter", Qo), E().iframeBus.on("ig.media-save-to-pinterest", ei), E().iframeBus.on("ig.open-sidebar-dm", ai), E().iframeBus.on("ig.start-conversation-in-sidebar-dm", ni), E().iframeBus.on("ig.check-has-pro", oi), E().iframeBus.on("ig.is-dm-supported", ii), E().iframeBus.on("ig.update-ig-view", ri), E().iframeBus.on("ig.is-fullscreen", li)
		}
	};

	function Jo(e) {
		C().sentryController.sendError(`ig: ${e}`, "error", null, {
			actor: "instagram"
		})
	}

	function Xo(e) {
		e.startsWith("/stories/") || e.startsWith("/direct/") || localStorage.setItem("instagram-iframe-path", e)
	}

	function Qo({
		url: e,
		currentTime: t,
		volume: s,
		muted: a,
		paused: n
	}) {
		const o = document.createElement("video"),
			i = o.requestFullscreen || o.webkitRequestFullScreen;
		document.body.appendChild(o), o.setAttribute("src", e), i.call(o), o.disablePictureInPicture = !0, o.currentTime = t, o.volume = s, o.muted = a, n || o.play(), document.addEventListener("fullscreenchange", (function t() {
			document.fullscreenElement || (E().iframeBus.send("ig.media-fullscreen-exit", {
				url: e,
				currentTime: o.currentTime,
				volume: o.volume,
				muted: o.muted
			}), o.remove(), document.removeEventListener("fullscreenchange", t))
		}))
	}

	function ei({
		igUrl: e,
		url: t
	}) {
		v().gaController.sendEvent("user", "ig:media-save-to-pinterest"), chrome.tabs.create({
			url: J().default("https://www.pinterest.ru/pin-builder/", {
				url: e,
				media: t,
				method: "extension"
			}),
			active: !0
		})
	}

	function ti({
		url: e
	}) {
		chrome.tabs.create({
			url: e,
			active: !0
		})
	}

	function si({
		filename: e,
		url: t,
		html: s
	}) {
		t ? chrome.downloads.download({
			filename: e,
			url: t
		}) : C().sentryController.sendError("No url detected on downloaded media", "error", {
			html: s
		}, {
			actor: "instagram"
		})
	}

	function ai() {
		y().transaction((e => {
			e.sidebar.isOpen = !0, e.sidebar.selectedTabId = "tab-dm"
		}))
	}
	async function ni(e) {
		E().iframeBus.send("dm.start-conversation", e), await A().default(120), ai()
	}

	function oi() {
		return T().stateProxy.hasPro()
	}

	function ii() {
		return y().model.state.dm.supported
	}
	async function ri(e) {
		y().transaction((t => {
			"fullscreenWidth" in e && (t.igView.fullscreenWidth = e.fullscreenWidth), "withBorder" in e && (t.igView.withBorder = e.withBorder)
		})), await new Promise((e => {
			requestAnimationFrame(e)
		}))
	}

	function li() {
		return y().model.state.igView.fullscreen
	}
	var ci = {
		init: function () {
			Zo.init()
		},
		getInitialUrl: function () {
			let e;
			"instagram-iframe-path" in localStorage ? (localStorage.removeItem("instagram-iframe-url"), e = `https://www.instagram.com${localStorage.getItem("instagram-iframe-path")}`) : e = "instagram-iframe-url" in localStorage ? localStorage.getItem("instagram-iframe-url") : "https://www.instagram.com";
			(function (e) {
				const t = e.split("/");
				return ui.some((e => {
					const s = e.split("/");
					if (s.length !== t.length) return !1;
					for (let e = 0; e < t.length; e += 1) {
						const a = t[e],
							n = s[e];
						if (n !== a && (!n.startsWith("<") || !n.endsWith(">"))) return !1
					}
					return !0
				}))
			})(function (e) {
				let t = e.split("?")[0].split("/").slice(3).join("/");
				t.startsWith("/") || (t = `/${t}`);
				t.endsWith("/") || (t = `${t}/`);
				return t
			}(e)) || (e = "https://www.instagram.com");
			return e
		}
	};
	const ui = ["/", "/explore/", "/explore/people/suggested/", "/accounts/activity/", "/accounts/edit/", "/<username>/", "/<username>/followers/", "/<username>/following/", "/<username>/feed/", "/<username>/channel/", "/<username>/saved/", "/<username>/tagged/", "/<username>/reels/", "/p/<postId>/", "/reel/<postId>/"];
	class di extends k().default.Component {
		constructor(e) {
			super(e), this._initIframe = () => {
				this.setState({
					iframeUrl: ci.getInitialUrl()
				})
			}, this._onIframeReady = () => {
				this.setState({
					isLoadingShown: !1
				})
			}, this._onIframePathChange = e => {
				this.setState({
					isDarkBackground: e.startsWith("/stories/") || e.startsWith("/create/story")
				})
			}, this.state = {
				iframeUrl: null,
				isDarkBackground: !1,
				isLoadingShown: !0
			}
		}
		componentDidMount() {
			E().iframeBus.on("ig.ready", this._onIframeReady), E().iframeBus.on("ig.path-change", this._onIframePathChange), setTimeout(this._initIframe, 70)
		}
		componentWillUnmount() {
			E().iframeBus.off("ig.ready", this._onIframeReady), E().iframeBus.off("ig.path-change", this._onIframePathChange)
		}
		render() {
			let e = this.state.isDarkBackground;
			return "night" === this.state.theme && (e = !e), Glamor.createElement(S().default.IgView, {
				iframeAttrs: {
					src: !1 !== F().env.features.iframes ? this.state.iframeUrl : null,
					name: He.createName("inssist-ig", {
						theme: this.state.theme,
						fusionConfig: We.getConfig(),
						isElectron: !!window.electron
					}),
					allow: "geolocation; autoplay"
				},
				isFrameless: this.props.isFrameless,
				isDarkBackground: e,
				isLoadingShown: this.state.isLoadingShown,
				isSplashShown: this.props.isSplashShown,
				isCreationCardShown: this.props.creationCardShown,
				creationCard: Glamor.createElement(Ko, {
					hasScheduleAccess: this.props.hasScheduleAccess
				})
			})
		}
	}
	var hi = y().influx((e => ({
		creationCardShown: e.igView.creationCardShown,
		hasScheduleAccess: T().stateProxy.hasPro({
			feature: "schedule"
		})
	})))(k().default.theme.ThemeAware(di));
	x(), I();
	var mi = {
		init: async function () {
			const e = await x().default(chrome.tabs.getCurrent);
			I().chromeBus.send("core-web-request.popup-tab-id", e.id)
		}
	};
	k(), re(), E();
	var pi = {
		init: function () {
			E().iframeBus.on("theme.get-theme", gi), k().default.theme.onThemeChange(fi)
		}
	};

	function gi() {
		return k().default.theme.getTheme()
	}

	function fi(e) {
		re().default.set("theme", e), E().iframeBus.send("theme.switch-theme", e)
	}
	E(), ke(), y(), y(), T(), E(), v();
	var Ci = {
		init: function () {
			y().model.observe((() => T().stateProxy.hasPro({
				feature: "dmAdvanced"
			})), (e => {
				e || y().transaction((e => {
					e.dm.ghostModeEnabled = !1
				}))
			})), y().model.observe((e => e.dm.ghostModeEnabled), (e => {
				E().iframeBus.send("dm.ghost-mode:toggled", e)
			})), E().iframeBus.on("dm.ghost-mode:is-enabled", wi), E().iframeBus.on("dm.ghost-mode:failed", vi), E().iframeBus.on("dm.ghost-mode:increment-trial-usage", ki)
		}
	};

	function wi() {
		return y().model.state.dm.ghostModeEnabled
	}

	function vi(e = {}) {
		y().transaction((e => {
			e.dm.ghostModeFailed = !0, e.dm.ghostModeEnabled = !1
		})), v().gaController.sendEvent("user", "dm:ghost-mode-failed", JSON.stringify(e))
	}

	function ki(e = 0) {
		T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:dm-ghost-mode"), y().transaction((t => {
			t.billing.trial.dmAdvanced += e
		}))
	}
	var Si = {
		init: function () {
			E().iframeBus.on("dm.ig-go", yi), E().iframeBus.on("dm.update-badge", _i), async function () {
				const e = setTimeout((() => {
					const e = setTimeout((() => {
						y().transaction((e => {
							e.dm.supported = !1, "tab-dm" === e.sidebar.selectedTabId && (e.sidebar.isOpen = !1, e.sidebar.selectedTabId = null)
						}))
					}), 2e3);
					E().iframeBus.send("dm.ping", (() => {
						clearTimeout(e)
					}))
				}), 5e3);
				E().iframeBus.on("dm.support-status", (t => {
					clearTimeout(e), y().transaction((e => {
						e.dm.supported = t
					}))
				}))
			}(), ke().resetController.onReset(bi), Ci.init()
		}
	};

	function bi() {
		y().transaction((e => {
			e.dm.supported = !1
		}))
	}

	function yi(e) {
		E().iframeBus.send("ig.ajax-go", e)
	}

	function _i(e) {
		y().transaction((t => {
			t.dm.badgeText = e
		}))
	}
	k(), S(), F(), y(), T(), E(), v();
	class Ei extends k().default.Component {
		constructor(e) {
			super(e), this._onChange = ({
				filterString: e,
				filterUnread: t,
				filterFlagged: s,
				ghostModeEnabled: a
			}) => {
				let n;
				y().transaction((e => {
					e.dm.ghostModeEnabled !== a && (a ? v().gaController.sendEvent("user", "dm:ghost-mode-on") : v().gaController.sendEvent("user", "dm:ghost-mode-off")), e.dm.ghostModeEnabled = a
				})), this.state.filterUnread && s && (t = !1), this.state.filterFlagged && t && (s = !1), this.setState({
					filterString: e,
					filterUnread: t,
					filterFlagged: s
				}), E().iframeBus.send("dm.set-filters", {
					string: e,
					unread: t,
					flagged: s
				}), e && this.state.filterString !== e ? n = "dm:filter-string" : !this.state.filterUnread && t ? n = "dm:filter-unread" : !this.state.filterFlagged && s && (n = "dm:filter-flagged"), clearTimeout(this._sendGaEventTimeout), n && (this._sendGaEventTimeout = setTimeout((() => {
					v().gaController.sendEvent("user", n)
				}), 1e3)), clearTimeout(this._incrementTrialTimeout);
				(e || t || s) && (this._incrementTrialTimeout = setTimeout((() => {
					T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:dm-filters"), y().transaction((e => {
						e.billing.trial.dmAdvanced += 1
					}))
				}), 1e3))
			}, this._onUpgradeClick = () => {
				vt.openBilling("dm")
			}, this._onCancelClick = () => {
				this._onChange({
					filterString: "",
					filterUnread: !1,
					filterFlagged: !1,
					ghostModeEnabled: !1
				})
			}, this._sendGaEventTimeout = null, this._incrementTrialTimeout = null, this.state = {
				filterString: "",
				filterUnread: !1,
				filterFlagged: !1
			}
		}
		componentDidUpdate(e) {
			e.dmOpen && !this.props.dmOpen && E().iframeBus.send("dm.go-to-inbox")
		}
		render() {
			const e = He.createName("inssist-dm", {
					theme: this.state.theme,
					fusionConfig: We.getConfig(),
					isElectron: !!window.electron
				}),
				t = !1 !== F().env.features.iframes ? "https://www.instagram.com/direct/inbox/" : null;
			return Glamor.createElement(S().default.DmSidePanel, {
				iframeAttrs: {
					name: e,
					src: t,
					allow: "geolocation; autoplay"
				},
				filterString: this.state.filterString,
				filterUnread: this.state.filterUnread,
				filterFlagged: this.state.filterFlagged,
				ghostModeEnabled: this.props.ghostModeFailed ? null : this.props.ghostModeEnabled,
				ghostModeTooltip: {
					title: "GHOST MODE",
					text: ["If enabled, the messages you read won’t be marked as seen and other users won’t know that you read their messages.", "Chat Filters and Ghost Mode are PRO features and available to try out for a limited time."]
				},
				showOverlay: this.showOverlay(),
				onChange: this._onChange,
				onUpgradeClick: this._onUpgradeClick,
				onCancelClick: this._onCancelClick,
				upsellOverlay: this._renderUpsellOverlay(),
				upsellOverlayHeaderText: Glamor.createElement(k().default.Fragment, null, "DM filters & Ghost Mode are PRO features.", Glamor.createElement("br", null), "Please consider upgrading to support development.")
			})
		}
		_renderUpsellOverlay() {
			return Glamor.createElement(Bs, {
				show: this.showOverlay(),
				feature: "dm"
			})
		}
		showOverlay() {
			const {
				hasDmAdvanced: e,
				ghostModeEnabled: t,
				ghostModeFailed: s
			} = this.props, {
				filterString: a,
				filterUnread: n,
				filterFlagged: o
			} = this.state;
			return !e && (!(s || !t) || !("" === a && !n && !o))
		}
	}
	var Ti = y().influx((e => ({
		dmOpen: e.sidebar.isOpen && "tab-dm" === e.sidebar.selectedTabId,
		hasDmAdvanced: T().stateProxy.hasPro({
			feature: "dmAdvanced"
		}),
		ghostModeFailed: e.dm.ghostModeFailed,
		ghostModeEnabled: e.dm.ghostModeEnabled
	})))(k().default.theme.ThemeAware(Ei));
	I(), y();
	var Ai = {
		cleanUpState: async function () {
			await new Promise((e => {
				let t;
				I().chromeBus.send("cleanup.clean-up-state").then((e => {
					t = e
				}));
				const s = y().model.observe((e => e.cleanupId), (a => {
					t && a === t && (y().transaction((e => {
						delete e.cleanupId
					})), setTimeout((() => s())), e())
				}))
			}))
		}
	};
	E(), T();
	var Pi = {
		init: function () {
			E().iframeBus.on("watermark.check-enabled", Ii)
		}
	};

	function Ii() {
		return !T().stateProxy.hasProPaid()
	}
	E();
	var Fi = {
		init: function () {
			window.addEventListener("blur", xi), window.addEventListener("focus", Gi), E().iframeBus.on("dnd-iframe-fix.blur", Di), E().iframeBus.on("dnd-iframe-fix.focus", Oi), Pn `
    <style>
      .dnd-iframe-fix--disable-iframes iframe :not(.dnd-immune) {
        pointer-events: none !important;
      }
    </style>
  `
		}
	};

	function xi() {
		Bi(!1)
	}

	function Gi() {
		Bi(!0)
	}

	function Di() {
		document.hasFocus() || Bi(!1)
	}

	function Oi() {
		Bi(!0)
	}

	function Bi(e) {
		document.documentElement.classList.toggle("dnd-iframe-fix--disable-iframes", !e)
	}
	P(), v(), y(), T(), E();
	var Ui = {
		init: function () {
			E().iframeBus.on("zen.is-enabled", Li), E().iframeBus.on("zen.is-expired", Ri), E().iframeBus.on("zen.increment-trial-usage", Ni), E().iframeBus.on("zen.go-pro-click", zi), E().iframeBus.on("zen.turn-off-click", Vi)
		},
		toggleZenMode: Mi
	};

	function Li() {
		return y().model.state.zen.enabled
	}

	function Ri() {
		return !T().stateProxy.hasPro({
			feature: "zen"
		})
	}

	function Mi({
		value: e = null,
		sendGaEvent: t = !0
	} = {}) {
		y().transaction((t => {
			t.zen.enabled = "boolean" == typeof e ? e : !t.zen.enabled
		}));
		const s = y().model.state.zen.enabled;
		t && (s ? v().gaController.sendEvent("user", "zen:toggle-on") : v().gaController.sendEvent("user", "zen:toggle-off")), E().iframeBus.send("zen.toggled", s)
	}

	function Ni(e = 1) {
		T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:zen"), T().stateProxy.hasPro() || y().transaction((t => {
			const s = Date.now();
			t.zen.lastTrialUpdateOn && s - t.zen.lastTrialUpdateOn < 24 * P().HOUR || (t.billing.trial.zen += e, t.zen.lastTrialUpdateOn = s)
		}))
	}

	function zi() {
		vt.openBilling("zen"), v().gaController.sendEvent("user", "zen:go-pro-click")
	}

	function Vi() {
		Mi({
			value: !1,
			sendGaEvent: !1
		}), v().gaController.sendEvent("user", "zen:turn-off-click")
	}
	y(), E();
	var Hi = {
		init: function () {
			E().iframeBus.on("new-post-extra.click-option", $i), E().iframeBus.on("new-post-extra.enter-page", Wi), E().iframeBus.on("new-post-extra.exit-page", qi), y().model.observe((e => e.coverAssist.shown ? "cover-assist" : e.tagAssist.shown ? "tag-assist" : null), (e => {
				E().iframeBus.send("new-post-extra.synch-selected-option", e)
			}))
		}
	};

	function $i(e) {
		y().transaction((t => {
			const s = t.coverAssist.shown ? "cover-assist" : t.tagAssist.shown ? "tag-assist" : null;
			t.tagAssist.shown = !1, t.coverAssist.shown = !1, e !== s && ("tag-assist" === e ? t.tagAssist.shown = !0 : "cover-assist" === e && (t.coverAssist.shown = !0))
		}))
	}

	function Wi({
		isVideo: e
	}) {
		y().transaction((t => {
			e ? (t.tagAssist.shown = !1, t.coverAssist.shown = !0) : (t.tagAssist.shown = !0, t.coverAssist.shown = !1), t.sidebar.isOpen = !1
		}))
	}

	function qi() {
		y().transaction((e => {
			e.tagAssist.shown = !1, e.coverAssist.shown = !1
		}))
	}
	v(), y(), T(), I(), E(), Se();
	var ji = {
		init: function () {
			y().model.observe((e => e.coverAssist.coverUrl), (async e => {
				if (e) {
					const t = await fetch(e),
						s = await t.blob();
					E().iframeBus.send("cover-assist.synch-cover", s)
				} else E().iframeBus.send("cover-assist.synch-cover", null)
			})), E().iframeBus.on("new-post-extra.creation-video-change", Yi), E().iframeBus.on("ig.submit-post", Ki)
		}
	};
	async function Yi(e) {
		if (y().transaction((e => {
				Object.assign(e.coverAssist, Se().default)
			})), !e) return;
		const t = await async function (e, t) {
				return new Promise((s => {
					const a = document.createElement("canvas"),
						n = a.getContext("2d"),
						o = [],
						i = document.createElement("video");
					i.src = e, i.muted = !0, i.addEventListener("loadedmetadata", (() => {
						a.width = i.videoWidth, a.height = i.videoHeight, r()
					})), i.addEventListener("timeupdate", (() => {
						n.drawImage(i, 0, 0), a.toBlob((e => {
							const t = URL.createObjectURL(e);
							o.push(t), r()
						}), "image/jpeg")
					}));
					const r = () => {
						const e = i.duration / (t + 1),
							n = o.length;
						n < t ? i.currentTime = (n + 1) * e : (i.remove(), a.remove(), s(o))
					}
				}))
			}(e, 12),
			s = y().model.state.authStatus.username,
			a = (await I().chromeBus.send("ig-api.fetch-user-posts", s, 14)).result.map((e => e.imgx));
		y().transaction((s => {
			s.coverAssist.loading = !1, s.coverAssist.videoUrl = e, s.coverAssist.frameGalleryImages = t, s.coverAssist.frameGallerySelectedImage = null, s.coverAssist.gridImages = a
		}))
	}

	function Ki() {
		const e = y().model.state;
		if (!e.coverAssist.coverUrl) return;
		const t = e.coverAssist.selectedTabId;
		"auto" === t ? v().gaController.sendEvent("user", "cover-assist:submit-cover-auto") : "frame" === t ? v().gaController.sendEvent("user", "cover-assist:submit-cover-frame") : "upload" === t && v().gaController.sendEvent("user", "cover-assist:submit-cover-upload"),
			function (e = 0) {
				T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:cover-assist");
				y().transaction((t => {
					t.billing.trial.coverAssist += e
				}))
			}(1)
	}
	k(), S(), v(), y(), T(), E();
	class Zi extends k().default.Component {
		constructor(e) {
			super(e), this._onCloseClick = () => {
				y().transaction((e => {
					e.coverAssist.shown = !1
				}))
			}, this._onTabClick = e => {
				y().transaction((t => {
					t.coverAssist.selectedTabId = e, this.props.hasCoverAssistFeature && ("auto" === e ? t.coverAssist.coverUrl = t.coverAssist.frameGallerySelectedImage : "frame" === e ? t.coverAssist.coverUrl = t.coverAssist.frameSelectImage : "upload" === e && (t.coverAssist.coverUrl = t.coverAssist.frameUploadImage))
				}))
			}, this._onUpgradeToProClick = () => {
				vt.openBilling("cover-assist"), v().gaController.sendEvent("user", "cover-assist:upgrade-to-pro-click")
			}, this._onResetClick = () => {
				y().transaction((e => {
					e.coverAssist.selectedTabId = "auto", e.coverAssist.frameGallerySelectedImage = null, e.coverAssist.coverUrl = null
				}))
			}, this._onGridButtonClick = () => {
				y().transaction((e => {
					e.coverAssist.showGrid = !e.coverAssist.showGrid
				}))
			}, this._onFrameGalleryImageClick = e => {
				y().transaction((t => {
					t.coverAssist.frameGallerySelectedImage = e, this.props.hasCoverAssistFeature ? t.coverAssist.coverUrl = e : this._showUpsell()
				}))
			}, this._onFrameSelectValueChange = e => {
				y().transaction((t => {
					t.coverAssist.frameSelectValue = e
				}))
			}, this._onFrameSelectImageSelect = e => {
				y().transaction((t => {
					"frame" === t.coverAssist.selectedTabId && (t.coverAssist.frameSelectImage = e, this.props.hasCoverAssistFeature ? t.coverAssist.coverUrl = e : this._showUpsell())
				}))
			}, this._onFrameUploadFileSelect = async e => {
				const t = document.createElement("img");
				let s = null,
					a = null;
				await new Promise((n => {
					t.src = URL.createObjectURL(e), t.addEventListener("load", (() => {
						s = t.width, a = t.height, n()
					}))
				}));
				const n = document.createElement("canvas"),
					o = Math.min(s, a),
					i = s > a ? (s - o) / 2 : 0,
					r = a > s ? (a - o) / 2 : 0,
					l = n.getContext("2d");
				n.width = o, n.height = o, l.drawImage(t, i, r, o, o, 0, 0, o, o);
				const c = await new Promise((e => {
						n.toBlob(e, "image/jpeg")
					})),
					u = URL.createObjectURL(c);
				y().transaction((e => {
					e.coverAssist.frameUploadImage = u, this.props.hasCoverAssistFeature ? e.coverAssist.coverUrl = u : this._showUpsell()
				}))
			}, this._showUpsell = () => {
				setTimeout((() => {
					this.setState({
						showUpsell: !0
					})
				}), 500)
			}, this._onUpsellOverlayClick = () => {
				this.setState({
					showUpsell: !1
				})
			}, this.state = {
				showUpsell: !1,
				defaultIgCoverUrl: null
			}, E().iframeBus.send("cover-assist.get-default-ig-cover-url", (e => {
				this.setState({
					defaultIgCoverUrl: e
				})
			}))
		}
		render() {
			return Glamor.createElement(S().default.CoverAssistPanel, {
				loading: this.props.loading,
				selectedTabId: this.props.selectedTabId,
				showGrid: this.props.showGrid,
				showResetButton: this.props.showResetButton,
				hasPro: this.props.hasCoverAssistFeature,
				resetDisabled: this.props.resetDisabled,
				frameGallery: this._getFrameGallery(),
				frameSelect: this._getFrameSelect(),
				frameUpload: this._getFrameUpload(),
				grid: this._getGrid(),
				upsellOverlay: this._getUpsellOverlay(),
				onCloseClick: this._onCloseClick,
				onTabClick: this._onTabClick,
				onUpgradeToProClick: this._onUpgradeToProClick,
				onResetClick: this._onResetClick,
				onGridButtonClick: this._onGridButtonClick
			})
		}
		_getFrameGallery() {
			return Glamor.createElement(S().default.CoverAssistFrameGallery, {
				images: this.props.frameGalleryImages,
				selectedImage: this.props.frameGallerySelectedImage,
				isReelsView: this.props.isCreatingReels,
				onImageClick: this._onFrameGalleryImageClick
			})
		}
		_getFrameSelect() {
			return Glamor.createElement(S().default.CoverAssistFrameSelect, {
				videoUrl: this.props.videoUrl,
				value: this.props.frameSelectValue,
				isReelsView: this.props.isCreatingReels,
				onValueChange: this._onFrameSelectValueChange,
				onImageSelect: this._onFrameSelectImageSelect
			})
		}
		_getFrameUpload() {
			return Glamor.createElement(S().default.CoverAssistFrameUpload, {
				image: this.props.frameUploadImage,
				isReelsView: this.props.isCreatingReels,
				onFileSelect: this._onFrameUploadFileSelect
			})
		}
		_getGrid() {
			const e = [this.props.coverUrl || this.state.defaultIgCoverUrl, ...this.props.gridImages];
			return Glamor.createElement(S().default.CoverAssistGrid, {
				images: e
			})
		}
		_getUpsellOverlay() {
			return Glamor.createElement(Bs, {
				show: this.state.showUpsell,
				feature: "cover-assist",
				onOverlayClick: this._onUpsellOverlayClick
			})
		}
	}
	var Ji = y().influx((e => ({
		loading: e.coverAssist.loading,
		selectedTabId: e.coverAssist.selectedTabId,
		showGrid: e.coverAssist.showGrid,
		showResetButton: !!e.coverAssist.coverUrl,
		coverUrl: e.coverAssist.coverUrl,
		gridImages: e.coverAssist.gridImages,
		videoUrl: e.coverAssist.videoUrl,
		frameGalleryImages: e.coverAssist.frameGalleryImages,
		frameGallerySelectedImage: e.coverAssist.frameGallerySelectedImage,
		frameSelectImage: e.coverAssist.frameSelectImage,
		frameSelectValue: e.coverAssist.frameSelectValue,
		frameUploadImage: e.coverAssist.frameUploadImage,
		hasCoverAssistFeature: T().stateProxy.hasPro({
			feature: "coverAssist"
		}),
		isCreatingReels: e.reels.creating
	})))(Zi);
	E(), T(), v(), y();
	var Xi = {
		init: function () {
			E().iframeBus.on("story-link.get-user-data", Qi), E().iframeBus.on("story-link.save-click", er), E().iframeBus.on("story-link.upgrade-to-pro-click", tr), E().iframeBus.on("story-link.story-with-link-submit", sr)
		}
	};

	function Qi() {
		const e = y().model.state,
			t = T().stateProxy.hasProPaid() || T().stateProxy.hasProPromocode(),
			s = 2 - e.billing.trial.addLinkToStory;
		return {
			hasEnoughFollowers: e.userDetails[e.authStatus.userId].followersCount > 1e4,
			needProUpgrade: !t && 0 === s,
			hasProPaid: t,
			trialLeftCount: s
		}
	}

	function er() {
		v().gaController.sendEvent("user", "story-link:save-click")
	}

	function tr() {
		vt.openBilling("story-link"), v().gaController.sendEvent("user", "story-link:upgrade-to-pro-click")
	}

	function sr() {
		T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:add-link-to-story"), y().transaction((e => {
			e.billing.trial.addLinkToStory += 1
		}))
	}
	E(), T(), y(), v();
	var ar = {
		init: function () {
			E().iframeBus.on("repost.can-repost", nr), E().iframeBus.on("repost.repost-click", or)
		}
	};

	function nr() {
		return T().stateProxy.hasPro({
			feature: "repost"
		})
	}

	function or({
		isVideo: e,
		isCarousel: t,
		isReels: s
	}) {
		if (!nr()) return v().gaController.sendEvent("user", "repost:open-billing"), void vt.openBilling("repost");
		T().stateProxy.hasPro() || y().transaction((e => {
			e.billing.trial.repost += 1
		})), T().stateProxy.hasProPaid() && v().gaController.sendEvent("user", "pro-paid-usage:repost"), t ? e ? v().gaController.sendEvent("user", "repost:submit-carousel-video") : v().gaController.sendEvent("user", "repost:submit-carousel-photo") : s ? v().gaController.sendEvent("user", "repost:submit-reels") : e ? v().gaController.sendEvent("user", "repost:submit-single-video") : v().gaController.sendEvent("user", "repost:submit-single-photo")
	}
	x(), V(), E(), I(), T(), y(), v();
	var ir = {
		init: function () {
			E().iframeBus.on("reels.is-pro", rr), E().iframeBus.on("reels.is-mobile-session", lr), E().iframeBus.on("reels.authorize", cr), E().iframeBus.on("reels.open-billing", ur), E().iframeBus.on("reels.supported-yes", (() => {
				y().transaction((e => {
					e.reels.supported = !0
				}))
			})), E().iframeBus.on("reels.creation-session-start", (() => {
				y().transaction((e => {
					e.reels.creating = !0
				}))
			})), E().iframeBus.on("reels.creation-session-end", (() => {
				y().transaction((e => {
					e.reels.creating = !1
				}))
			})), E().iframeBus.on("reels.submit", (() => {
				v().gaController.sendEvent("user", "reels:submit"), T().stateProxy.hasProPaid() ? v().gaController.sendEvent("user", "pro-paid-usage:reels") : y().transaction((e => {
					e.billing.trial.reels += 1
				}))
			}))
		}
	};

	function rr() {
		return T().stateProxy.hasPro({
			feature: "reels"
		})
	}

	function lr() {
		return y().model.state.authStatus.isMobileSession
	}
	async function cr() {
		await I().chromeBus.send("auth.toggle-session-watcher", !1), await x().default(chrome.cookies.remove, {
			url: "https://*.instagram.com/*",
			name: "sessionid"
		});
		const e = await V().utils.createWindow("https://www.instagram.com/accounts/login/", {
				name: "reels.auth-window"
			}),
			t = await new Promise((t => {
				(async() => {
					await V().utils.waitForWindowClose(e), setTimeout((() => {
						t(!1)
					}), 2e3)
				})(), chrome.cookies.onChanged.addListener((function e({
					cookie: s,
					cause: a,
					removed: n
				}) {
					if (!s.domain.includes("instagram.com")) return;
					if ("reels.auth-result-cookie" !== s.name) return;
					if ("explicit" !== a) return;
					if (n) return;
					const o = "1" === s.value;
					chrome.cookies.onChanged.removeListener(e), chrome.cookies.remove({
						url: "https://www.instagram.com",
						name: "reels.auth-result-cookie"
					}), t(o)
				}))
			}));
		await I().chromeBus.send("auth.toggle-session-watcher", !0), E().iframeBus.send("reels.auth-performed", t), I().chromeBus.send("auth.refresh-user", {
			isSettingSessionId: t
		})
	}

	function ur() {
		v().gaController.sendEvent("user", "reels:open-billing"), vt.openBilling("reels")
	}
	V(), J(), I();
	var dr = {
		init: function () {
			I().chromeBus.on("fb-api.auth-fcs", hr)
		}
	};
	async function hr({
		appId: e,
		stateCode: t,
		withCrossposting: s = !1
	}) {
		const a = s ? "https://business.facebook.com/media_manager/instagram/oauthlink/" : "https://business.facebook.com/business/loginpage/igoauth/callback/",
			n = await V().utils.createWindow(J().default("https://api.instagram.com/oauth/authorize/", {
				app_id: e,
				state: t,
				force_reauth: "true",
				response_type: "code",
				redirect_uri: a
			}));
		let o = null;
		return window.addEventListener("message", (function e(t) {
			var s;
			"fb-api.connect-fcs" === t.data.name && (window.removeEventListener("message", e), o = null == t || null === (s = t.data) || void 0 === s ? void 0 : s.code)
		})), await V().utils.waitForWindowClose(n), o
	}
	E();
	var mr = {
		init: function () {
			if (pr = !!window.electron, gr = He.isIframe() && He.getParams().isElectron, !pr && !gr) return;
			pr && E().iframeBus.on("electron-links.open-url", fr);
			document.addEventListener("click", (e => {
				const t = e.target.closest("a");
				if (!t) return;
				if ("_blank" !== t.getAttribute("target")) return;
				const s = t.getAttribute("href");
				s.startsWith("/") || (e.preventDefault(), e.stopPropagation(), gr ? E().iframeBus.send("electron-links.open-url", s) : fr(s))
			}), {
				capture: !0
			})
		}
	};
	let pr, gr;

	function fr(e) {
		chrome.tabs.create({
			url: e,
			active: !0
		})
	}
	var Cr = {
		waitForBackground: async function () {
			const e = await new Promise((e => chrome.runtime.getBackgroundPage((t => e(t)))));
			await new Promise((t => {
				const s = setInterval((() => {
					e._backgroundReady && (clearInterval(s), t())
				}), 50)
			}))
		}
	};
	y();
	var wr = {
		init: function () {
			if (y().model.state.version > 70) return;
			document.body.innerHTML = '\n    <style>\n      .min-version {\n        width: 100%;\n        display: flex;\n        align-items: center;\n        justify-content: center;\n      }\n\n      .min-version__content {\n        width: 440px;\n        position: relative;\n        top: -40px;\n      }\n\n      .min-version__title {\n        font-size: 18px;\n        line-height: 24px;\n        font-weight: 600;\n      }\n\n      .min-version__text {\n        margin-top: 12px;\n      }\n\n      .min-version__paragraph {\n        font-size: 16px;\n        line-height: 24px;\n        margin-top: 12px;\n      }\n      .min-version__paragraph:first-child {\n        margin-top: 0;\n      }\n    </style>\n\n    <div class="min-version">\n      <div class="min-version__content">\n        <div class="min-version__title">\n          FAILED TO UPDATE\n        </div>\n        <div class="min-version__text">\n          <div class="min-version__paragraph">\n            Your Inssist plugin version is too old and Chrome has failed to fix and update it automatically 😵.\n          </div>\n          <div class="min-version__paragraph">\n            Please uninstall Inssist from <a href="chrome://extensions">chrome://extensions</a> page\n            in Chrome and reinstall from <a href="https://get.inssist.com">https://get.inssist.com</a>\n          </div>\n        </div>\n      </div>\n    </div>\n  ', document.body.addEventListener("click", (e => {
				const t = e.target.closest("a");
				if (!t) return;
				e.preventDefault();
				const s = t.getAttribute("href");
				chrome.tabs.create({
					active: !0,
					url: s
				})
			}))
		}
	};
	I();
	var vr = {
		init: function () {
			return new Promise((e => {
				const t = Math.random();
				I().chromeBus.send("chrome-tab.close", t), I().chromeBus.on("chrome-tab.close", (e => {
					t !== e && window.close()
				})), setTimeout(e, 50)
			}))
		}
	};
	y(), E(), v();
	var kr = function () {
		E().iframeBus.on("ig.open-media", (() => {
			v().gaController.sendEvent("user", "ig:media-open")
		})), E().iframeBus.on("ig.download-media", (() => {
			v().gaController.sendEvent("user", "ig:media-download")
		})), E().iframeBus.on("ig.submit-post", (e => {
			"photo" === e ? v().gaController.sendEvent("user", "ig:submit-post-photo") : "video" === e && v().gaController.sendEvent("user", "ig:submit-post-video"), y().transaction((e => {
				e.billing.trial.postsPublished = (e.billing.trial.postsPublished || 0) + 1
			}))
		})), E().iframeBus.on("ig.submit-story", (e => {
			"photo" === e ? v().gaController.sendEvent("user", "ig:submit-story-photo") : "video" === e && v().gaController.sendEvent("user", "ig:submit-story-video"), y().transaction((e => {
				e.billing.trial.storiesPublished = (e.billing.trial.storiesPublished || 0) + 1
			}))
		})), E().iframeBus.on("ig.open-dm", (() => {
			v().gaController.sendEvent("user", "ig:dm-open")
		})), E().iframeBus.on("dm.message-sent", (() => {
			y().transaction((e => {
				e.billing.trial.dmsSent = (e.billing.trial.dmsSent || 0) + 1
			}))
		}))
	};
	I(), d(), be(), ye(), y(), k();
	k(), S(), y(), E(), k(), S(), y(), v(), T(), k(), y(), b(), I(), v(), T(), S(), k(), F(), y();
	const Sr = {
		placeholder: {
			width: "100%",
			height: 300
		},
		iframeWrapper: { ...k().default.relative()
		},
		iframeSubWrapper: { ...k().default.absolute(". -16 . -124")
		},
		iframe: {
			border: "none"
		}
	};
	class br extends k().default.Component {
		render() {
			const e = !1 !== F().env.features.iframes && `https://inssist.com/share?theme=${this.state.theme}`;
			return this.props.isOpen ? Glamor.createElement("div", {
				css: Sr.iframeWrapper
			}, Glamor.createElement("div", {
				css: Sr.iframeSubWrapper
			}, Glamor.createElement("iframe", {
				css: Sr.iframe,
				src: e,
				width: "100%",
				height: "300px"
			}))) : Glamor.createElement("div", {
				css: Sr.placeholder
			})
		}
	}
	var yr = y().influx((e => ({
		isOpen: e.sidebar.isOpen && "logo" === e.sidebar.selectedTabId
	})))(k().default.theme.ThemeAware(br));
	const _r = {
		root: { ...k().default.padding("g2 g2 0 23")
		},
		card: {
			marginBottom: 24,
			marginRight: 10,
			"& a": {
				color: k().default.color.link,
				fontWeight: "inherit"
			}
		},
		rateUs: {
			marginLeft: -1,
			marginBottom: 24,
			marginRight: 8,
			"& a": {
				color: "inherit !important",
				fontWeight: 700
			}
		},
		contacts: { ...k().default.text.bleak
		},
		iframeWrapper: { ...k().default.relative()
		},
		iframeSubWrapper: { ...k().default.absolute(". -16 . -124")
		},
		iframe: {
			border: "none"
		}
	};
	k().default.Image.registerImages({
		"whats-new.fire": "ui-igswiss/billing-fire.png:50:50"
	});
	class Er extends k().default.Component {
		constructor(e) {
			super(e), this._onRateUsClick = () => {
				v().gaController.sendEvent("user", "rate-us-mini:yes-click"), T().actions.acknowledge.dispatch("rateUs"), chrome.tabs.create({
					url: b().common.reviewsUrl
				})
			}, this._onUpgradeClick = () => {
				v().gaController.sendEvent("user", "whats-new:upgrade-click"), vt.openBilling("whats-new")
			}, this._onSendReportClick = () => {
				this.setState({
					isReportSent: !0
				}), I().chromeBus.send("overseer.send-report", {
					key: "user"
				})
			}, this._whatsNewItems = T().getWhatsNewItems(), this.state = {
				isReportSent: !1
			}
		}
		render() {
			return Glamor.createElement("div", {
				css: _r.root
			}, this._renderHeader(), Glamor.createElement(k().default.Spacer, {
				height: "g1"
			}), this._renderProPanel(), Glamor.createElement(k().default.Spacer, {
				height: "g3"
			}), this._renderWhatsNew(), this._renderRateUs(), this._renderContacts())
		}
		_renderHeader() {
			return Glamor.createElement("div", {
				css: [k().default.row, k().default.alignItems.center, k().default.text.mainTitle]
			}, "What's New")
		}
		_renderProPanel() {
			const {
				hasProPaid: e,
				hasProPromocode: t
			} = this.props;
			let s, a;
			return e || t ? (a = !1, s = "\n        Inssist PRO is active. All advanced features activated:\n        Scheduling, Analytics, Insights and more.") : (a = !0, s = "\n        All basic features are free forever. Scheduling, Analytics, Insights\n        and more features are available on PRO:"), Glamor.createElement("div", {
				css: k().default.row
			}, Glamor.createElement("div", null, Glamor.createElement(k().default.Image, {
				src: "whats-new.fire",
				width: 19,
				height: 19
			})), Glamor.createElement(k().default.Spacer, {
				width: "g1"
			}), Glamor.createElement("div", {
				css: [k().default.column]
			}, Glamor.createElement("div", {
				css: [k().default.row, k().default.text.bleak]
			}, s), a && Glamor.createElement(k().default.Fragment, null, Glamor.createElement(k().default.Spacer, {
				height: "g1"
			}), Glamor.createElement(k().default.LinkButton, {
				label: "UPGRADE TO PRO",
				onClick: this._onUpgradeClick
			}))))
		}
		_renderWhatsNew() {
			return Glamor.createElement("div", {
				css: _r.content
			}, this.props.whatsNew.map((e => {
				const t = this._whatsNewItems.find((t => t.id === e.id));
				return t ? Glamor.createElement(k().default.Card, {
					key: e.id,
					header: t.header,
					marker: !e.acknowledged && k().default.color.link,
					subheader: t.subheader,
					hexImage: t.hexImage,
					content: this._renderWhatsNewItemContent(t),
					style: _r.card
				}) : null
			})))
		}
		_renderWhatsNewItemContent(e) {
			return "ladder" === e.id ? [Glamor.createElement(k().default.Fragment, null, "Hashtag Analytics just got smarter! 🧠"), Glamor.createElement(k().default.Fragment, null, "Searching for relevant hashtags now returns up to ", Glamor.createElement("b", null, "1000 tags"), " broken down into groups of low, medium, high and very high engagement; relative to your account!"), Glamor.createElement(k().default.Fragment, null, "Having low competitive hashtags on your post gives it a chance to rank high."), Glamor.createElement(k().default.Fragment, null, "That in turn boosts post engagement and ranks it even higher among the more competitive tags. And so on, and so forth, boosting your reach."), Glamor.createElement(k().default.Fragment, null, "Read our blog about ", Glamor.createElement("a", {
				href: "https://inssist.com/knowledge-base/ultimate-instagram-hashtag-guide",
				target: "_blank"
			}, "hashtag ladders"), " and grow your Instagram faster!")] : e.content
		}
		_renderRateUs() {
			return this.props.showRateUs ? Glamor.createElement("div", {
				css: _r.rateUs
			}, Glamor.createElement(S().default.RateUsMini, {
				image: "rate-us-heart",
				content: Glamor.createElement("div", null, "Something is broken? Reach us at ", Glamor.createElement("a", {
					href: "mailto:inssist@slashedio.io"
				}, "inssist@slashed.io"), Glamor.createElement(k().default.Spacer, {
					height: "g1"
				}), this.state.isReportSent ? Glamor.createElement(k().default.Fragment, null, "Thank you! The bug report has been sent.") : Glamor.createElement(k().default.Fragment, null, "Or send us a bug report:", Glamor.createElement("div", {
					css: k().default.row
				}, Glamor.createElement(k().default.LinkButton, {
					small: !0,
					label: "SEND BUG REPORT",
					onClick: this._onSendReportClick
				}), Glamor.createElement(k().default.Spacer, {
					width: "g1"
				}), Glamor.createElement(k().default.InfoCircle, {
					tooltip: {
						text: Glamor.createElement(k().default.Fragment, null, "Clicking this button will send an internal technical data on this connectivity error to the development team for research. This data is not shared with 3rd parties.")
					}
				})))),
				onRateUsClick: this._onRateUsClick
			})) : null
		}
		_renderContacts() {
			return Glamor.createElement("div", {
				css: _r.contacts
			}, "Follow us on Instagram: ", Glamor.createElement("a", {
				href: "https://www.instagram.com/inssistapp/",
				target: "_blank"
			}, "@inssistapp"), Glamor.createElement(k().default.Spacer, {
				height: 4
			}), "Our contact details: ", Glamor.createElement("a", {
				href: "mailto:inssist@slashed.io"
			}, "inssist@slashed.io"), Glamor.createElement(k().default.Spacer, {
				height: 24
			}), "Share a word about us:", Glamor.createElement(k().default.Spacer, {
				height: 4
			}), Glamor.createElement(yr, null))
		}
	}
	var Tr = y().influx((e => ({
		hasPro: T().stateProxy.hasPro(),
		hasProPaid: T().stateProxy.hasProPaid(),
		hasProPromocode: T().stateProxy.hasProPromocode(),
		whatsNew: e.whatsNew,
		showRateUs: !e.rateUs.acknowledged
	})))(Er);
	y();
	var Ar = y().action("whats-new.acknowledge-all", ((e, t) => ({ ...e,
		whatsNew: e.whatsNew.map((e => ({ ...e,
			acknowledged: !0
		})))
	})));
	const Pr = ({
			props: e
		}) => ({
			width: "100%",
			height: "100%",
			..."tab-dm" !== e.selectedTabId && { ...k().default.fixed("0 0 0 0"),
				visibility: "hidden",
				pointerEvents: "none"
			}
		}),
		Ir = ({
			props: e
		}) => ({
			width: "100%",
			height: "100%",
			...e.isScheduleFallback && {
				width: "calc(100vw - 84px)",
				marginLeft: 4
			},
			..."tab-scheduling" !== e.selectedTabId && { ...k().default.fixed("0 0 0 0"),
				visibility: "hidden",
				pointerEvents: "none"
			}
		}),
		Fr = {
			dm: "sidebar-mediator.dm",
			covers: "sidebar-mediator.covers",
			schedule: "sidebar-mediator.schedule",
			insights: "sidebar-mediator.insights",
			analytics: "sidebar-mediator.analytics",
			hashtags: "sidebar-mediator.hashtags",
			multiaccount: "sidebar-mediator.multiaccount",
			billing: "sidebar-mediator.billing"
		};
	k().default.Image.registerImages({
		"sidebar-mediator.billing": "tab-billing.png:34:45"
	}), k().default.SvgIcon.registerSvgIcons([`<symbol id="${Fr.dm}" viewBox="0 0 43.414 43.414"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path d="M42 1.414l-22.55 22.55M42 1.414l-14.35 41-8.2-18.45L1 15.764z"/></g></symbol>`, `<symbol id="${Fr.covers}" viewBox="0 0 35.699 48.18"><g transform="translate(-1317 -255)"><line y1="8" transform="translate(1326.5 256.5)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="2"/><path d="M0,16V0" transform="translate(1344.5 256.5)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="2"/><path d="M0,12V0" transform="translate(1335.5 256.5)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="2"/><path d="M30.891,0H2.808A2.809,2.809,0,0,0,0,2.808V22.466H33.7V2.808A2.809,2.809,0,0,0,30.891,0ZM0,29.331a5.617,5.617,0,0,0,5.617,5.617h5.617v5.617a5.617,5.617,0,0,0,11.233,0V34.947h5.617A5.617,5.617,0,0,0,33.7,29.331V26.523H0Zm16.85,9.127a2.106,2.106,0,1,1-2.106,2.106A2.106,2.106,0,0,1,16.85,38.458Z" transform="translate(1318 256)" fill="none" stroke="currentColor" stroke-width="2"/></g></symbol>`, `<symbol id="${Fr.schedule}" viewBox="0 0 42.004 42.004"><path d="M10.002 42.002a24.394 24.394 0 01-10-10v-26a6.007 6.007 0 016-6h26a24.394 24.394 0 0110 10v26a6.007 6.007 0 01-6 6zm-4-6a4.006 4.006 0 004 4h26a4 4 0 004-4V28.69l-.635-.6-1.855-1.748-.213-.207a1.713 1.713 0 00-2.3-.059l-4.164 3.563a3.726 3.726 0 01-2.619.908l2.154 4.092a.971.971 0 01-.365 1.3.881.881 0 01-.447.117.927.927 0 01-.812-.5l-6.648-12.6-4.57-8.67a1.722 1.722 0 00-3.039-.059L6.001 29.084zm-4-30v26a4 4 0 002 3.463V10.002a6 6 0 016-6h4a1 1 0 010 2h-4a4.008 4.008 0 00-4 4v15.313l6.9-12.062a3.539 3.539 0 016.256.107l2.939 5.582 1.637 3.092 2.92 5.537a1.968 1.968 0 001.367 1.031 1.926 1.926 0 001.637-.432l4.164-3.568a3.529 3.529 0 013.693-.564l1.855 1.459.635.6V10.002a4.006 4.006 0 00-4-4h-14a1 1 0 110-2H35.47a4.117 4.117 0 00-.637-.83 3.991 3.991 0 00-2.831-1.17h-26a4.007 4.007 0 00-4 4zm24.605 8.381a4.556 4.556 0 114.553 4.553 4.562 4.562 0 01-4.553-4.555zm1.768 0a2.789 2.789 0 102.785-2.791 2.788 2.788 0 00-2.785 2.789zM17.002 5.002a1 1 0 111 1 1 1 0 01-1-1z" fill="currentColor"/></symbol>`, `<symbol id="${Fr.insights}" _dy="-1.85%" viewBox="0 0 39.902 54.569"><g fill="currentColor"><path d="M34.964 6.321H29.63v-.254A2.865 2.865 0 0 0 26.768 3.2h-1.831A5.346 5.346 0 0 0 20.046 0h-.191a5.346 5.346 0 0 0-4.891 3.2h-1.83a2.865 2.865 0 0 0-2.862 2.862v.254H4.938C1.846 6.321 0 7.8 0 10.272v39.359a4.944 4.944 0 0 0 4.938 4.938h30.026a4.944 4.944 0 0 0 4.936-4.938V23.276a.988.988 0 1 0-1.975 0v26.355a2.966 2.966 0 0 1-2.963 2.963H4.938a2.966 2.966 0 0 1-2.963-2.963V10.272c0-.488 0-1.975 2.963-1.975h5.333v1.382a.988.988 0 0 0 .988.988h17.384a.988.988 0 0 0 .988-.988V8.3h5.333a2.966 2.966 0 0 1 2.963 2.963v3.325a.988.988 0 1 0 1.975 0V11.26a4.944 4.944 0 0 0-4.938-4.939zm-7.309 2.37H12.247V6.067a.888.888 0 0 1 .887-.887h2.529a.988.988 0 0 0 .953-.729 3.363 3.363 0 0 1 3.24-2.476h.191a3.364 3.364 0 0 1 3.24 2.476.987.987 0 0 0 .953.729h2.529a.887.887 0 0 1 .886.887zm0 0"/><path d="M38.915 17.73a.986.986 0 1 0 .7.288.995.995 0 0 0-.7-.288zm-27.293-3.507a4.148 4.148 0 1 0 4.148 4.148 4.153 4.153 0 0 0-4.148-4.148zm0 6.321a2.173 2.173 0 1 1 2.173-2.173 2.175 2.175 0 0 1-2.173 2.173zm0 5.531a4.148 4.148 0 1 0 4.148 4.148 4.153 4.153 0 0 0-4.148-4.148zm0 6.321a2.173 2.173 0 1 1 2.173-2.173 2.175 2.175 0 0 1-2.173 2.173zm0 5.531a4.148 4.148 0 1 0 4.148 4.148 4.153 4.153 0 0 0-4.148-4.148zm0 6.321a2.173 2.173 0 1 1 2.173-2.173 2.175 2.175 0 0 1-2.173 2.173zm19.819-23.704H20.116a.988.988 0 1 0 0 1.975h11.326a.988.988 0 1 0 0-1.975zm0-4.741H20.116a.988.988 0 1 0 0 1.975h11.326a.988.988 0 1 0 0-1.975zm0 16.593H20.116a.988.988 0 1 0 0 1.975h11.326a.988.988 0 1 0 0-1.975zm0-4.741H20.116a.988.988 0 1 0 0 1.975h11.326a.988.988 0 1 0 0-1.975zm0 16.594H20.116a.988.988 0 10 0 1.975h11.326a.988.988 0 1 0 0-1.975zm0-4.742H20.116a.988.988 0 1 0 0 1.975h11.326a.988.988 0 1 0 0-1.975zm0 0"/></g></symbol>`, `<symbol id="${Fr.analytics}" viewBox="0 0 40 44"><g transform="translate(-19 -346)"><path d="M101,8500H97a4,4,0,0,1-4-4v-21a4.005,4.005,0,0,1,4-4h4a4.005,4.005,0,0,1,4,4v21A4,4,0,0,1,101,8500Zm-4-27a2,2,0,0,0-2,2v21a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2v-21a2,2,0,0,0-2-2Z" transform="translate(-74 -8114)" fill="currentColor"/><path d="M101,8511H97a4,4,0,0,1-4-4v-32a4,4,0,0,1,4-4h4a4,4,0,0,1,4,4v32A4.005,4.005,0,0,1,101,8511Zm-4-38a2,2,0,0,0-2,2v32a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2v-32a2,2,0,0,0-2-2Z" transform="translate(-60 -8125)" fill="currentColor"/><path d="M101,8504H97a4,4,0,0,1-4-4v-25a4.005,4.005,0,0,1,4-4h4a4.005,4.005,0,0,1,4,4v25A4,4,0,0,1,101,8504Zm-4-31a2,2,0,0,0-2,2v25a2,2,0,0,0,2,2h4a2,2,0,0,0,2-2v-25a2,2,0,0,0-2-2Z" transform="translate(-46 -8118)" fill="currentColor"/><path d="M204.54,279.535h-10a1,1,0,0,0,0,2h10a1,1,0,0,0,0-2Zm0,0" transform="translate(-174.54 108.465)" fill="currentColor"/><path d="M204.54,279.535h-10a1,1,0,0,0,0,2h10a1,1,0,0,0,0-2Zm0,0" transform="translate(-160.54 108.465)" fill="currentColor"/><path d="M204.54,279.535h-10a1,1,0,0,0,0,2h10a1,1,0,0,0,0-2Zm0,0" transform="translate(-146.54 108.465)" fill="currentColor"/></g></symbol>`, `<symbol id="${Fr.hashtags}" viewBox="0 0 38.717 48.907"><path d="M3.802 48.907a3.889 3.889 0 01-3.8-3.8V4.725A4.726 4.726 0 014.725.003h33.051a.945.945 0 010 1.889 2.836 2.836 0 000 5.672.943.943 0 01.943.944v37.564a2.844 2.844 0 01-2.832 2.838zm-1.914-3.8a2.1 2.1 0 001.914 1.909h32.082a.943.943 0 00.943-.944V9.452H4.722a4.679 4.679 0 01-2.834-.974zm0-40.382a2.836 2.836 0 002.834 2.838H33.99a4.734 4.734 0 010-5.672H4.722a2.832 2.832 0 00-2.834 2.831zm19.471 35.412l.8-6.676h-7.207l-.84 6.676h-1.9l.8-6.676h-4.99v-1.934h5.238l.916-7.387H9.145v-1.934h5.273l.811-6.675h1.934l-.846 6.675h7.244l.809-6.675h1.9l-.811 6.675h4.955v1.934h-5.2l-.914 7.387h4.99v1.934h-5.24l-.8 6.676zm-6.189-8.609h7.244l.914-7.387h-7.207z" fill="currentColor"/></symbol>`, `<symbol id="${Fr.billing}" _dx="23%" viewBox="0 0 47.703 36.216"><path d="M18.108 18.108a8.554 8.554 0 10-8.554-8.554 8.579 8.579 0 008.554 8.554zm0 4.277C12.441 22.385 1 25.272 1 30.939v4.277h34.216v-4.277c0-5.667-11.441-8.554-17.108-8.554z" fill="none" stroke="currentColor" stroke-width="2"/><path d="M32.695 1.008h-2.424v5.6h1.3V5.064h1.124c1.5 0 2.432-.776 2.432-2.024s-.936-2.032-2.432-2.032zm-.072 3h-1.056V2.064h1.056c.792 0 1.192.36 1.192.976s-.4.968-1.192.968zm8.384 2.6l-1.256-1.8a1.821 1.821 0 001.156-1.768c0-1.256-.94-2.032-2.436-2.032h-2.424v5.6h1.3v-1.56h1.188l1.08 1.56zM39.591 3.04c0 .608-.4.976-1.192.976h-1.056V2.064h1.056c.792 0 1.192.36 1.192.976zm5.048 3.668a2.9 2.9 0 003.068-2.9 2.9 2.9 0 00-3.064-2.9 2.9 2.9 0 00-3.064 2.9 2.9 2.9 0 003.06 2.9zm0-1.1a1.708 1.708 0 01-1.752-1.8 1.708 1.708 0 011.752-1.792 1.708 1.708 0 011.752 1.792A1.708 1.708 0 0144.639 5.6z" fill="currentColor"/></symbol>`, `<symbol id="${Fr.multiaccount}" viewBox="0 0 42 34.727"><g transform="translate(-0.5 -3.5)"><path d="M30.591,33.409V29.773A7.273,7.273,0,0,0,23.318,22.5H8.773A7.273,7.273,0,0,0,1.5,29.773v3.636" transform="translate(0 3.818)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M22.045,11.773A7.273,7.273,0,1,1,14.773,4.5a7.273,7.273,0,0,1,7.273,7.273Z" transform="translate(1.273 0)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M35.455,33.368V29.731A7.273,7.273,0,0,0,30,22.7" transform="translate(6.045 3.86)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M24,4.7a7.273,7.273,0,0,1,0,14.091" transform="translate(4.773 0.041)" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></symbol>`]);
	class xr extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onElectronTabOpen = ({
				tab: e
			}) => {
				this._onTabClick(e)
			}, this._onTabClick = e => {
				y().model.state.sidebar.selectedTabId !== e && v().gaController.sendEvent("user", `sidebar:open-${e}`), y().transaction((t => {
					t.sidebar.selectedTabId === e ? t.sidebar.isOpen = !t.sidebar.isOpen : (t.sidebar.isOpen = !0, t.sidebar.selectedTabId = e, t.tagAssist.shown = !1, t.coverAssist.shown = !1)
				})), "logo" === this.props.selectedTabId && Ar.dispatch()
			}, this._onSidePanelCloseClick = () => {
				y().transaction((e => {
					e.sidebar.isOpen = !1
				})), "logo" === this.props.selectedTabId && Ar.dispatch()
			}, this._onClosed = () => {
				y().transaction((e => {
					e.sidebar.isOpen = !1, e.sidebar.selectedTabId = null
				}))
			}, this._onBodyPanelClosed = () => {
				y().transaction((e => {
					"tab-analytics" === e.sidebar.selectedTabId && (e.analytics.selectedCard = null)
				}))
			}, t
		}
		componentDidMount() {
			window.electron && window.electron.addExtensionListener("sidebar-mediator.open-tab", this._onElectronTabOpen)
		}
		componentWillUnmount() {
			window.electron && window.electron.removeExtensionListener("sidebar-mediator.open-tab", this._onElectronTabOpen)
		}
		render() {
			return Glamor.createElement(S().default.Sidebar, {
				open: this.props.isOpen,
				selectedTabId: this.props.selectedTabId,
				logoTab: this._getLogoTab(),
				menu: this._getMenu(),
				sidePanel: this._getSidePanel(),
				bodyPanel: this._getBodyPanel(),
				onTabClick: this._onTabClick,
				onSidePanelCloseClick: this._onSidePanelCloseClick,
				onClosed: this._onClosed,
				onBodyPanelClosed: this._onBodyPanelClosed
			})
		}
		_getLogoTab() {
			return {
				svgIcon: "igswiss.logo",
				withMarker: this.props.withWhatsNewMarker,
				withProSticker: this.props.hasPro
			}
		}
		_getMenu() {
			const e = T().stateProxy.hasProPaid("inssist-pro-infinite") ? "an infinite" : T().stateProxy.hasProPaid("inssist-pro-lifetime") ? "a lifetime" : T().stateProxy.hasProPaid("inssist-pro-monthly") ? "a monthly" : "";
			return {
				disabled: !this.props.isLoggedIn,
				tabs: [this.props.dmSupported && {
					id: "tab-dm",
					svgIcon: Fr.dm,
					title: "Direct Messages",
					features: ["send messages", "filter chats", "ghost mode, peek at photos"],
					badgeText: this.props.dmBadgeText
				}, {
					id: "tab-scheduling",
					svgIcon: Fr.schedule,
					title: "Post Assistant",
					features: ["grid planner, bulk upload", "post carousels", "schedule with auto-post"]
				}, {
					id: "tab-tag-assist",
					svgIcon: Fr.hashtags,
					title: "Hashtag Assistant",
					features: ["manage hashtag collections", "find effective hashtags", "create hashtag ladders"]
				}, {
					id: "tab-analytics",
					svgIcon: Fr.analytics,
					title: "Analytics",
					features: ["see who unfollowed you", "engage with new followers", "competitor analysis"]
				}, {
					id: "tab-billing",
					title: "PRO Version",
					iconStyle: {
						position: "relative",
						left: -1
					},
					...this.props.hasProPaid ? {
						svgIcon: Fr.billing,
						features: ["access to all advanced features", `enabled with ${e} plan`, "thank you 😘"]
					} : {
						svgIcon: Fr.billing,
						iconStyle: {
							position: "relative",
							left: -1
						},
						features: ["access to all advanced features", "from $7.90 / month", "support development"]
					}
				}].filter(Boolean)
			}
		}
		_getSidePanel() {
			let e, t = !1;
			const s = this.props.selectedTabId;
			"logo" === s ? e = Glamor.createElement(Tr, null) : "tab-analytics" === s ? e = Glamor.createElement(Bt, null) : "tab-tag-assist" === s ? e = Glamor.createElement(_n, null) : "tab-scheduling" === s ? this.props.isScheduleFallback ? (t = !0, e = null) : (t = this.props.showScheduleTagAssist, e = Glamor.createElement(vo, null)) : "tab-billing" === s && (t = !0, e = null);
			const a = this.props.isLoggedIn && Glamor.createElement("div", {
				css: Pr(this)
			}, Glamor.createElement(Ti, null));
			return {
				hidden: t,
				content: Glamor.createElement(k().default.Fragment, null, e, a),
				width: "tab-dm" === s ? 740 : null,
				hideCloseButton: "tab-billing" === s || "tab-scheduling" === s || "tab-analytics" === s || "tab-tag-assist" === s
			}
		}
		_getBodyPanel() {
			const e = this.props,
				t = e.selectedTabId;
			let s = !1,
				a = null;
			"tab-analytics" === t ? (s = e.isAnalyticsBodyOpen, a = Glamor.createElement(ws, null)) : "tab-scheduling" === t ? s = e.isScheduleBodyOpen || e.isScheduleFallback : "tab-billing" === t && (s = !0, a = Glamor.createElement(Fs, null));
			const n = this.props.isLoggedIn && Glamor.createElement("div", {
				css: Ir(this)
			}, Glamor.createElement(Fo, null));
			return {
				open: s,
				content: Glamor.createElement(k().default.Fragment, null, a, n)
			}
		}
	}
	var Gr = y().influx((e => ({
		hasAllFeatures: T().stateProxy.hasPro({
			feature: "*"
		}),
		selectedTabId: e.sidebar.selectedTabId,
		isAnalyticsBodyOpen: e.analytics.isReportOpen,
		isScheduleBodyOpen: e.schedule.navigation.isOpen,
		isScheduleFallback: e.schedule.fallback.isEnabled,
		isBillingBodyOpen: e.billing.navigation.isBodyOpen,
		withWhatsNewMarker: e.whatsNew.some((e => !e.acknowledged)),
		isLoggedIn: e.authStatus.isLoggedIn,
		hasPro: T().stateProxy.hasPro(),
		hasProPaid: T().stateProxy.hasProPaid(),
		dmBadgeText: e.dm.badgeText,
		dmSupported: e.dm.supported,
		showScheduleTagAssist: e.schedule.showTagAssist
	})))(xr);
	k(), S(), y();
	class Dr extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onLetsGoClick = () => {
				y().transaction((e => {
					e.welcome.shown = !1
				}))
			}, t
		}
		render() {
			return Glamor.createElement(S().default.Welcome, {
				title: "Instagram Assistant",
				description: Glamor.createElement(k().default.Fragment, null, "Inssist is your personal Instagram Assistant and is the best way to use ", Glamor.createElement("b", null, "Instagram on Desktop"), ", ads free:"),
				listItems: [Glamor.createElement(k().default.Fragment, null, "Post photos, videos, IGTVs, stories, reels. Send DMs."), Glamor.createElement(k().default.Fragment, null, "Schedule posts, videos and carousels to be published automatically. Calendar and time slots included."), Glamor.createElement(k().default.Fragment, null, "Monitor unfollowers. Export followers / followings and check credibility of any public account, gain insights on your competitors, get tag suggestions and more.")],
				buttonLabel: "OK, LET'S GO",
				onButtonClick: this._onLetsGoClick,
				disclaimer: Glamor.createElement(k().default.Fragment, null, "Disclaimer: this app is not endorsed or certified by Instagram™. All Instagram™ logos and trademarks displayed on this app are property of third parties. Inssist is distributed AS IS. By using Inssist you agree to the Inssist terms of service found here: ", Glamor.createElement("a", {
					href: "https://inssist.com/terms"
				}, "https://inssist.com/terms"), ".", Glamor.createElement("br", null), Glamor.createElement("br", null), Glamor.createElement("b", null, "We do not use or sell your data. Inssist is ads free."))
			})
		}
	}
	y(), b(), v(), T(), k(), S();
	const Or = e => ({ ...k().default.text.nowrap,
			opacity: 0,
			pointerEvents: "none",
			...e.showTosSummary && {
				opacity: 1,
				pointerEvents: "inherit"
			}
		}),
		Br = (e, t) => ({ ...k().default.absolute(". 0 31 ."),
			...k().default.transition.fast,
			width: 406,
			opacity: 0,
			transform: "translateY(20px)",
			pointerEvents: "none",
			...e.showTosSummary && t.panelShown && {
				opacity: 1,
				transform: "none",
				pointerEvents: "inherit"
			}
		});
	class Ur extends k().default.Component {
		constructor(e) {
			super(e), this._onButtonClick = () => {
				this.setState({
					panelShown: !this.state.panelShown
				})
			}, this._onClose = () => {
				v().gaController.sendEvent("user", "tos-summary:close-click"), T().actions.acknowledge.dispatch("tosSummary")
			}, this._onOkClick = () => {
				v().gaController.sendEvent("user", "tos-summary:ok-click"), T().actions.acknowledge.dispatch("tosSummary")
			}, this._onFullTermsClick = () => {
				v().gaController.sendEvent("user", "tos-summary:terms-click"), chrome.tabs.create({
					url: b().common.termsUrl
				})
			}, this._onPrivacyPolicyClick = () => {
				v().gaController.sendEvent("user", "tos-summary:privacy-click"), chrome.tabs.create({
					url: b().common.policyUrl
				})
			}, this._onFaqClick = () => {
				v().gaController.sendEvent("user", "tos-summary:faq-click"), chrome.tabs.create({
					url: b().common.faqUrl
				})
			}, this.state = {
				panelShown: !1
			}
		}
		componentDidUpdate(e) {
			!e.showTosSummary && this.props.showTosSummary && v().gaController.sendEvent("user", "tos-summary:shown")
		}
		render() {
			return Glamor.createElement(k().default.Fragment, null, Glamor.createElement("div", {
				css: Or(this.props)
			}, Glamor.createElement(k().default.LinkButton, {
				label: "TOS & PRIVACY",
				color: k().default.color.textPassive,
				small: !0,
				onClick: this._onButtonClick
			})), Glamor.createElement("div", {
				css: Br(this.props, this.state)
			}, Glamor.createElement(S().default.TosSummary, {
				onClose: this._onClose,
				onOkClick: this._onOkClick,
				onFullTermsClick: this._onFullTermsClick,
				onPrivacyPolicyClick: this._onPrivacyPolicyClick,
				onFaqClick: this._onFaqClick
			})))
		}
	}
	var Lr = y().influx((e => ({
		showTosSummary: -1 === e.acknowledged.tosSummary
	})))(Ur);
	k(), S(), y(), v(), ke(), E();
	const Rr = "ig-buttons-mediator.back",
		Mr = "ig-buttons-mediator.refresh",
		Nr = "ig-buttons-mediator.fullscreen-enter",
		zr = "ig-buttons-mediator.fullscreen-exit",
		Vr = "ig-buttons-mediator.switch-to-day",
		Hr = "ig-buttons-mediator.switch-to-night",
		$r = "ig-buttons-mediator.zen",
		Wr = "ig-buttons-mediator.zen-on";
	k().default.SvgIcon.registerSvgIcons([`<symbol id="${Rr}" viewBox="0 0 40 40"><path fill="none" d="M0 0h40v40H0z"/><path d="M15.125 19.846l9.1-9.1-1.385-1.384-9.644 9.644a1.2 1.2 0 0 0 0 1.681l9.635 9.634 1.385-1.384z" fill="currentColor"/></symbol>`, `<symbol id="${Mr}" viewBox="0 0 40 40"><path fill="none" d="M0 0h40v40H0z"/><path d="M30.378 18.7a.82.82 0 1 0-1.618.258v.015a9.022 9.022 0 1 1-2.24-4.582L22.896 15.6l.518 1.555 4.917-1.639a.82.82 0 0 0 .561-.778V9.819h-1.64v2.994a10.546 10.546 0 1 0 3.126 5.887z" fill="currentColor"/></symbol>`, `<symbol id="${Nr}" viewBox="0 0 40 40"><path fill="none" d="M0 0h40v40H0z"/><path d="M28.163 26.952l-3.722-3.722-1.211 1.211 3.722 3.722H24.06v1.721h5.824V24.06h-1.721zm-3.722-10.179l3.722-3.725v2.892h1.721v-5.824H24.06v1.721h2.892l-3.722 3.721zm-8.883 6.457l-3.722 3.722V24.06h-1.72v5.824h5.824v-1.721h-2.892l3.725-3.722zm.382-11.393v-1.721h-5.824v5.824h1.721v-2.892l3.721 3.725 1.215-1.215-3.725-3.721z" fill="currentColor"/></symbol>`, `<symbol id="${zr}" viewBox="0 0 40 40"><path fill="none" d="M0 0h40v40H0z"/><path d="M24.951 26.162l3.722 3.722 1.211-1.211-3.722-3.722h2.892V23.23H23.23v5.824h1.721zm3.722-16.046l-3.722 3.725v-2.892H23.23v5.824h5.824v-1.721h-2.892l3.722-3.721zM11.332 29.884l3.722-3.722v2.892h1.72V23.23H10.95v1.721h2.892l-3.725 3.722zm-.382-14.832v1.721h5.824v-5.824h-1.721v2.892l-3.721-3.725-1.215 1.215 3.725 3.721z" fill="currentColor"/></symbol>`, `<symbol id="${Vr}" viewBox="0 0 40 40"><g transform="translate(3444 7623)"><rect width="40" height="40" transform="translate(-3444 -7623)" fill="none"/><path d="M11.166,20.406h3.118l-1.561,5.045ZM7.535,18.6l2.7,1.56L6.36,23.745Zm7.811,1.515,2.673-1.606,1.259,5.129Zm-9.2-7.347a6.622,6.622,0,1,1,6.621,6.529A6.576,6.576,0,0,1,6.14,12.766Zm-.8,2.579,1.607,2.673L1.816,19.277ZM18.6,17.917l1.56-2.7,3.588,3.875Zm1.81-6.75,5.045,1.561-5.045,1.558ZM0,12.723l5.045-1.557v3.118ZM1.707,6.36,6.855,7.534l-1.56,2.7Zm16.8,1.074,5.129-1.261-3.524,3.933ZM6.174,1.816l3.932,3.522L7.434,6.945Zm9.042,3.479,3.875-3.589L17.917,6.854Zm-4.049-.25L12.727,0l1.557,5.045Z" transform="translate(-3436.759 -7615.715)" fill="currentColor"/></g></symbol>`, `<symbol id="${Hr}" viewBox="0 0 40 40"><g transform="translate(3380 7623)"><rect width="40" height="40" transform="translate(-3380 -7623)" fill="none"/><g transform="translate(-3372.822 -7615.724)"><path d="M10.518,0a10.077,10.077,0,0,0-1.3.082A10.511,10.511,0,1,0,10.518,0ZM2.63,10.518A7.9,7.9,0,0,1,7.4,3.273a10.52,10.52,0,0,0,7.556,13.771A7.89,7.89,0,0,1,2.63,10.518Z" transform="translate(2.25 2.25)" fill="currentColor"/></g></g></symbol>`, `<symbol id="${$r}" viewBox="0 0 24.512 18.248"><path d="M24.3 14.632a8.813 8.813 0 00-2.357-1.69 9.42 9.42 0 002.271-4.292.719.719 0 00-.548-.7 9.941 9.941 0 00-3.588-.277c.044-.224.082-.449.112-.677a10.525 10.525 0 00-.27-4.118.718.718 0 00-.88-.508 10.6 10.6 0 00-3.729 1.848A10.866 10.866 0 0012.763.207a.719.719 0 00-1.016 0A10.866 10.866 0 009.2 4.222a10.6 10.6 0 00-3.729-1.848.718.718 0 00-.879.508 10.527 10.527 0 00-.158 4.798 9.941 9.941 0 00-3.586.276.719.719 0 00-.551.7 9.355 9.355 0 002.271 4.292A8.811 8.811 0 00.21 14.632a.718.718 0 000 1.016 8.877 8.877 0 0012.045.47 8.877 8.877 0 0012.031-.456.718.718 0 00.015-1.03zm-4.64-5.45a8.509 8.509 0 012.915 0 8.478 8.478 0 01-7.5 5.545 10.579 10.579 0 004.587-5.547zm-17.727 0a8.51 8.51 0 012.915 0 10.58 10.58 0 004.582 5.545 8.477 8.477 0 01-7.5-5.545zm4.552 7.629a7.383 7.383 0 01-4.7-1.672A7.322 7.322 0 013.7 14.007a9.9 9.9 0 005.826 2.159 7.42 7.42 0 01-3.043.646zm.4-6.624a9.15 9.15 0 01-1.077-6.2 9.171 9.171 0 012.96 1.746 10.921 10.921 0 001.249 7.677 9.122 9.122 0 01-3.125-3.222zm5.364 3.9a9.482 9.482 0 010-12.309 9.482 9.482 0 010 12.309zm3.705-6.155a10.931 10.931 0 00-.224-2.2A9.171 9.171 0 0118.7 3.98a9.143 9.143 0 01-4.21 9.423 10.9 10.9 0 001.471-5.474zm2.064 8.882a7.419 7.419 0 01-3.043-.646 9.9 9.9 0 005.826-2.159 7.326 7.326 0 011.917 1.133 7.383 7.383 0 01-4.693 1.67z" fill="currentColor"/></symbol>`]), k().default.Image.registerImages({
		[Wr]: "lotus.png:24.49:18.24"
	});
	class qr extends k().default.Component {
		constructor(...e) {
			var t;
			return t = super(...e), this._onBackClick = () => {
				v().gaController.sendEvent("user", "control:back-click"), E().iframeBus.send("ig.back")
			}, this._onRefreshClick = () => {
				v().gaController.sendEvent("user", "control:refresh-click"), E().iframeBus.send("ig.refresh")
			}, this._onFullscreenClick = () => {
				const e = this.props.isFullscreen ? "fullscreen:exit" : "fullscreen:enter";
				y().transaction((e => {
					e.igView.fullscreen = !e.igView.fullscreen
				})), v().gaController.sendEvent("user", e), E().iframeBus.send("ig.widescreen-toggled")
			}, this._onToggleThemeClick = () => {
				k().default.theme.toggleTheme()
			}, this._onToggleZenClick = () => {
				Ui.toggleZenMode()
			}, t
		}
		render() {
			return Glamor.createElement(S().default.IgButtons, {
				isHorizontal: this.props.isHorizontal,
				buttons: [{
					id: "back",
					icon: Rr,
					tooltip: {
						title: "GO BACK"
					},
					onClick: this._onBackClick
				}, {
					id: "refresh",
					icon: Mr,
					tooltip: {
						title: "REFRESH"
					},
					onClick: this._onRefreshClick,
					onLongPress: () => {
						ke().resetController.reset(), location.reload()
					}
				}, {
					id: "widescreen",
					icon: this.props.isFullscreen ? zr : Nr,
					tooltip: {
						title: "TOGGLE WIDESCREEN"
					},
					onClick: this._onFullscreenClick
				}, {
					id: "theme",
					icon: "night" === this.state.theme ? Vr : Hr,
					tooltip: {
						title: "night" === this.state.theme ? "GO LIGHT" : "GO DARK"
					},
					onClick: this._onToggleThemeClick
				}, {
					id: "zen",
					icon: this.props.zenModeEnabled ? null : $r,
					image: this.props.zenModeEnabled ? Wr : null,
					tooltip: {
						title: "ZEN MODE",
						text: Glamor.createElement(k().default.Fragment, null, "Switch your Instagram home feed", Glamor.createElement("br", null), "to a distraction free mode (PRO)")
					},
					onClick: this._onToggleZenClick
				}]
			})
		}
	}
	var jr = y().influx((e => ({
		isFullscreen: e.igView.fullscreen,
		zenModeEnabled: e.zen.enabled
	})))(k().default.theme.ThemeAware(qr));
	k(), S(), Q(), v(), E(), I(), y();
	const Yr = ({
		state: e
	}) => ({ ...e.disableHover && {
			pointerEvents: "none !important",
			"& *": {
				pointerEvents: "none !important"
			}
		}
	});
	class Kr extends k().default.Component {
		constructor(e) {
			super(e), Zr.call(this), this.state = {
				loggedInStatus: null,
				disableHover: !1
			}
		}
		componentDidMount() {
			I().chromeBus.on("auth.refreshed", this._dropLoggedInStatus), E().iframeBus.on("ig.ready", this._dropDisableHover)
		}
		componentWillUnmount() {
			I().chromeBus.off("auth.refreshed", this._dropLoggedInStatus), E().iframeBus.off("ig.ready", this._dropDisableHover)
		}
		render() {
			const e = this.props;
			return e.hidden ? null : Glamor.createElement(S().default.AccountSwitcher, {
				style: Yr(this),
				accounts: e.accounts.map((t => {
					const s = t.userId === e.selectedUserId && !e.addingNewAccount;
					let a = !0;
					return s && (a = null === this.state.loggedInStatus ? e.loggedIn : this.state.loggedInStatus), {
						id: t.userId,
						name: `@${t.username}`,
						avatarUrl: t.avatarUrl,
						selected: s,
						loggedIn: a
					}
				})),
				backgroundColor: this.props.backgroundColor,
				addingNewAccount: this.props.addingNewAccount,
				onSwitchAccountClick: this._onSwitchAccountClick,
				onAddAccountClick: this._onAddAccountClick,
				onDisconnectAccountClick: this._onDisconnectAccountClick
			})
		}
		async _logout() {
			this.setState({
				loggedInStatus: !1
			}), I().chromeBus.send("auth.logout")
		}
		async _login(e) {
			this.setState({
				loggedInStatus: !0
			}), I().chromeBus.send("auth.login", e)
		}
		_alertScheduleSaving() {
			alert("Inssist is saving changed to Scheduled Posts.Please wait for the save to complete before switching accounts.")
		}
	}
	var Zr = function () {
			this._onSwitchAccountClick = async e => {
				if (this.props.scheduleIsSaving) return void this._alertScheduleSaving();
				const t = this.props,
					s = t.accounts.find((t => t.userId === e));
				if (this.setState({
						disableHover: !0
					}), v().gaController.sendEvent("user", "multiaccount:switch"), y().transaction((t => {
						t.multiaccount.addingNewAccount = !1, t.multiaccount.selectedUserId = e
					})), t.loggedIn) {
					const e = await E().iframeBus.send("ig.get-url"),
						a = e.split("/")[1].split("?")[0];
					if (a === t.accounts.find((e => e.userId === t.selectedUserId)).username) {
						const t = e.replace(a, s.username);
						await I().chromeBus.send("auth.set-ig-initial-url", t)
					}
				}
				this._login(e)
			}, this._onAddAccountClick = async() => {
				this.props.scheduleIsSaving ? this._alertScheduleSaving() : (this.setState({
					disableHover: !0
				}), y().transaction((e => {
					e.multiaccount.addingNewAccount = !0
				})), this._logout())
			}, this._onDisconnectAccountClick = e => {
				if (this.props.scheduleIsSaving) return void this._alertScheduleSaving();
				const t = this.props.selectedUserId === e;
				if (t && this.props.loggedIn) return y().transaction((e => {
					e.authStatus.cookies.igSessionId = null
				})), this.setState({
					disableHover: !0
				}), void this._logout();
				let s;
				y().transaction((t => {
					const a = t.multiaccount.userIds;
					Q().default(a, e), s = a[a.length - 1] || null
				})), t && s && (y().transaction((e => {
					e.multiaccount.selectedUserId = s
				})), this.setState({
					loggedInStatus: !0
				}), this.setState({
					disableHover: !0
				}), this._login(s))
			}, this._dropLoggedInStatus = () => {
				this.setState({
					loggedInStatus: null
				})
			}, this._dropDisableHover = () => {
				this.setState({
					disableHover: !1
				})
			}
		},
		Jr = y().influx((e => ({
			hidden: e.welcome.shown,
			loggedIn: e.authStatus.isLoggedIn,
			selectedUserId: e.multiaccount.selectedUserId,
			addingNewAccount: e.multiaccount.addingNewAccount,
			scheduleIsSaving: Rn.isSaving(),
			accounts: e.multiaccount.userIds.map((t => {
				const s = e.userStates[t] || e;
				if (!s.authStatus.isLoggedIn) return null;
				if (s.authStatus.userId !== t) return null;
				const a = s.authStatus;
				return {
					userId: a.userId,
					username: a.username,
					avatarUrl: a.avatarUrl
				}
			})).filter(Boolean)
		})))(Kr);
	k(), S();
	class Xr extends k().default.Component {
		render() {
			return Glamor.createElement(S().default.Snackbar, null, Glamor.createElement(Ie, null), Glamor.createElement(Be, null), Glamor.createElement(Xe, null), Glamor.createElement(Do, null), Glamor.createElement(Mo, null), Glamor.createElement(qe, null), Glamor.createElement(An, null), Glamor.createElement(Gs, null))
		}
	}
	class Qr extends k().default.Component {
		render() {
			return Glamor.createElement(k().default.Fragment, null, Glamor.createElement(S().default.Layout, {
				Sidebar: Gr,
				IgView: hi,
				IgButtons: jr,
				Welcome: Dr,
				Tips: $a,
				Tos: Lr,
				IgtvUpload: Ke,
				AccountSwitcher: Jr,
				Snackbar: Xr,
				assistPanel: this._getAssistPanel(),
				isFullscreen: this.props.isFullscreen,
				isSidebarOpen: this.props.isSidebarOpen,
				isWelcomeShown: this.props.isWelcomeShown,
				isAssistPanelShown: this._isAssistPanelShown(),
				isExperimentsBarShown: this.props.isExperimentsBarShown,
				isIgtvUploadShown: this.props.isIgtvUploadShown,
				igViewFullscreenWidth: this.props.igViewFullscreenWidth,
				igViewWithBorder: this.props.igViewWithBorder,
				onBodyWheel: this._onBodyWheel
			}), Glamor.createElement(k().default.TooltipsContainer, null), Glamor.createElement(Uo, null))
		}
		_getAssistPanel() {
			return this.props.isTagAssistShown ? Glamor.createElement(yn, null) : this.props.isCoverAssistShown ? Glamor.createElement(Ji, null) : null
		}
		_isAssistPanelShown() {
			return this.props.isTagAssistShown || this.props.isCoverAssistShown
		}
		_onBodyWheel(e) {
			E().iframeBus.send("ig.broadcast-scroll", e.deltaY)
		}
	}
	var el = y().influx((e => ({
		isFullscreen: e.igView.fullscreen,
		isSidebarOpen: (e.authStatus.isLoggedIn || "logo" === e.sidebar.selectedTabId) && e.sidebar.isOpen,
		isWelcomeShown: e.welcome.shown,
		isTagAssistShown: e.tagAssist.shown,
		isCoverAssistShown: e.coverAssist.shown,
		isExperimentsBarShown: e.experiments.enabled,
		isIgtvUploadShown: e.igtvUpload.shown,
		igViewFullscreenWidth: e.igView.fullscreenWidth,
		igViewWithBorder: e.igView.withBorder
	})))(Qr);
	k().default.init({
		_: d().default,
		pathPrefix: "/img/",
		images: {
			"rate-us-heart": "ui-igswiss/rate-us-heart.png:40:40",
			"hex-bug": "ui-igswiss/hex-bug.png:53.47:60.01",
			"hex-zen": "ui-igswiss/hex-zen.png:53.47:60.01",
			"hex-story": "ui-igswiss/hex-story.png:53.47:60.01",
			"hex-carousel": "ui-igswiss/hex-carousel.png:53.47:60.01",
			"hex-multiaccount": "ui-igswiss/hex-multiaccount.png:53.47:60.01",
			"hex-analytics": "ui-igswiss/hex-analytics.png:53.47:60.01",
			"hex-monitor": "ui-igswiss/hex-monitor.png:53.47:60.01",
			"hex-tag": "ui-igswiss/hex-tag.png:53.47:60.01",
			"hex-mentions": "ui-igswiss/hex-mentions.png:53.47:60.01",
			"hex-marker": "ui-igswiss/hex-marker.png:53.47:60.01",
			"hex-video": "ui-igswiss/hex-video.png:53.47:60.01",
			"hex-igtv": "ui-igswiss/hex-igtv.png:53.47:60.01",
			"hex-insights": "ui-igswiss/hex-insights.png:53.47:60.01",
			"hex-igswiss": "ui-igswiss/hex-igswiss.png:53.47:60.01",
			"hex-quality": "ui-igswiss/hex-quality.png:53.47:60.01",
			"hex-update": "ui-igswiss/hex-update.png:53.47:60.01",
			"hex-schedule": "ui-igswiss/hex-schedule.png:53.47:60.01",
			"hex-moon": "ui-igswiss/hex-moon.png:53.47:60.01",
			"hex-dm": "ui-igswiss/hex-dm.png:53.47:60.01",
			"hex-engagement": "ui-igswiss/hex-engagement.png:53.47:60.01",
			"hex-ghost": "ui-igswiss/hex-ghost.png:53.47:60.01",
			"hex-ship": "ui-igswiss/hex-ship.png:53.47:60.01",
			"hex-youtube": "ui-igswiss/hex-youtube.png:53.47:60.01",
			"hex-caption": "ui-igswiss/hex-caption.png:53.47:60.01",
			"hex-swipe-up": "ui-igswiss/hex-swipe-up.png:53.47:60.01",
			"hex-repost": "ui-igswiss/hex-repost.png:53.47:60.01",
			"hex-lifetime": "ui-igswiss/hex-lifetime.png:53.47:60.01",
			"hex-mutual": "ui-igswiss/hex-mutual.png:53.47:60.01",
			"hex-xmas": "ui-igswiss/hex-xmas.png:53.47:60.01",
			"hex-pinterest": "ui-igswiss/hex-pinterest.png:53.47:60.01",
			"hex-tag-ladder": "ui-igswiss/hex-tag-ladder.png:53.47:60.01",
			"hex-reels": "ui-igswiss/hex-reels.png:53.47:60.01",
			"like-heart-empty": "ui-igswiss/like-heart-empty.svg:20:23",
			"like-heart-full": "ui-igswiss/like-heart-full.svg:20:23",
			"navigate.day": "ui-igswiss/navigate.day.svg:32:32",
			"navigate.night": "ui-igswiss/navigate.night.svg:32:32",
			"start-dm.day": "ui-igswiss/start-dm.day.svg:32:32",
			"start-dm.night": "ui-igswiss/start-dm.night.svg:32:32",
			"add-account-plus": "ui-igswiss/add-account-plus.png:30:30",
			"pro-art-1": "ui-igswiss/pro-art-1.png:471:598"
		},
		icons: []
	});
	const tl = function () {
		be().render(Glamor.createElement(ye().Provider, {
			store: y().model.store
		}, Glamor.createElement(el, null)), document.getElementById("app"))
	};
	var sl = {
		send: function (e, ...t) {
			const s = JSON.stringify({
				name: e,
				args: t
			});
			console.log(`electron-bus:${s}`)
		}
	};
	var al = {
		init: function () {
			document.body.addEventListener("dblclick", (e => {
				e.clientY > 40 || sl.send("maximize")
			}))
		}
	};
	!async function () {
		window._ = d().default, window.Glamor = u(), window.React = c, m().polyfillsController.init(), mr.init(), al.init(), await Cr.waitForBackground(), await vr.init(), p().logController.init(), g().i18nController.init(), f().abTestingController.init(), C().sentryController.init({
			dsn: "https://bea0900834f541bca8157710f7fd31fe@sentry.io/1547551"
		}), pi.init(), We.init(), await w().synchController.init("popup", !1), await Ai.cleanUpState(), wr.init(), v().gaController.init().sendPageview(), Fe.init(), Ue.init(), E().iframeBus.init(), Me.init(), Qe.init(), Ua.init(), In.init(), kr(), ci.init(), Rn.init(), Si.init(), No.init(), Pi.init(), Fi.init(), Ui.init(), Hi.init(), ji.init(), Xi.init(), it.init(), ar.init(), ir.init(), vt.init(), dr.init(), mi.init(), tl(), chrome.runtime.sendMessage({
			name: "update-user"
		}), I().chromeBus.send("popup.start"), I().chromeBus.on("popup.log", ((...e) => console.log(...e)))
	}()
}();